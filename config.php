<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'lmsdb1';
$CFG->dbuser    = 'root';
$CFG->dbpass    = 'admin';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '3306',
  'dbsocket' => '',
);

//$CFG->wwwroot   = 'http://10.1.64.76/E-Nipun';
//$CFG->wwwroot   = 'http://10.1.64.76/Investor-Education/E-Nipun';
$CFG->wwwroot   = 'https://'.$_SERVER['SERVER_NAME'].'/Investor-Education/E-Nipun';
$CFG->dataroot  = 'F:\Application\E-NipunData';
$CFG->admin     = 'admin';

//$CFG->debug = 32767; 
//$CFG->debugdisplay = true; 
$CFG->directorypermissions = 0777;

$CFG->passwordsaltmain = ')V6mW3QNs3Xx=d2R';
$CFG->disableupdateautodeploy = true;

$CFG->filetypeslist=array('.txt','.jpg','audio',
                          '.doc', '.docx','.rtf',
                          '.xls', '.xlsx','.csv',
                          '.pdf','.gif','.jpeg','.png','.svg','.tiff',
                          '.mp4', '.flv', '.mov', '.avi','.mp3', '.ogg', '.wav', '.aac', '.wma');

require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
