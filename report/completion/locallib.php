<?php

include_once('../../config.php');
include_once($CFG->dirroot . '/mod/scorm/locallib.php');
global $DB;
//$participants=get_enrolled_users(context_course::instance(7));
$courses = get_courses();
unset($courses[1]);
foreach ($courses as $k => $v) {
    $participants[$k] = get_enrolled_users(context_course::instance($v->id));
}

//echo"<pre>";print_r($participants);die;
foreach ($participants as $key => $value) {
    $course = $DB->get_record('course', array('id' => $key), 'shortname');
    $scorm = $DB->get_record('scorm', array('course' => $key));
    foreach ($value as $k => $v) {
        //   echo"<pre>";print_r($scorm);die;
        $scormGrades = scorm_grade_user($scorm, $v->id);

        //$userdetails['id']=$v->id; 
        $userdetails['courseid'] = $key;
        $userdetails['moduleid'] = $scorm->id;
        $userdetails['modulename'] = $scorm->name;
        $userdetails['userid'] = $v->id;
        $userdetails['username'] = $v->firstname . ' ' . $v->lastname;
        $userdetails['email'] = $v->email;
        $userdetails['phone'] = $v->phone1;
        $modulestarttime = get_scorm_start_time($key, $v->id);
        if ($modulestarttime) {
            $userdetails['stime'] = date('d-m-Y h:i', $modulestarttime);
            $moduleenddate = get_scorm_completed_last_accessed_date($v->id, $scorm->id);
            if ($moduleenddate) {
                $userdetails['etime'] = date('d-m-Y h:i', $moduleenddate);
            } else {
                $userdetails['etime'] = 'N/A';
            }
        } else {
            $userdetails['stime'] = '-';
            $userdetails['etime'] = '-';
        }

        $userdetails['modstatus'] = get_scorm_status($key, $v->id);
        $userdetails['modgrade'] = $scormGrades;

        $userInfo['data'][] = $userdetails;
        unset($userdetails);
        //echo"<pre>";print_r($userInfo);die;
    }
}
//echo"<pre>";print_r($userInfo);die;
echo json_encode($userInfo);

function get_scorm_start_time($courseid, $userid) {
    global $DB;
    $sql = "SELECT sst.id,s.course,s.name,
sst.userid,sst.scormid,sst.scoid,sst.element,sst.value FROM mdl_scorm_scoes_track AS sst
JOIN mdl_scorm AS s ON s.id=sst.scormid AND s.course=$courseid AND element='x.start.time' AND sst.userid=$userid";
    $res = $DB->get_record_sql($sql);
    if ($res->value) {
        $starttime = $res->value;
    }
    return $starttime;
}

function get_scorm_status($courseid, $userid) {
    global $DB;
    $sql = "SELECT sst.id,s.course,s.name,
sst.userid,sst.scormid,sst.scoid,sst.element,sst.value FROM mdl_scorm_scoes_track AS sst
JOIN mdl_scorm AS s ON s.id=sst.scormid AND s.course=$courseid AND element='cmi.core.lesson_status' AND sst.userid=$userid";
    $res = $DB->get_record_sql($sql);
    if ($res->value) {
        $modstatus = $res->value;
    } else {
        $modstatus = 'notattempted';
    }
    return $modstatus;
}

function get_scorm_completed_last_accessed_date($userid, $scormid) {
    global $DB;
    $sql = "SELECT gi.id,gi.courseid,gi.itemname,gi.itemtype,gi.iteminstance,gi.itemmodule,gg.userid,gg.rawgrade,gg.timemodified FROM mdl_grade_items AS gi  
       JOIN mdl_grade_grades AS gg ON gi.id=gg.itemid AND gg.userid=$userid AND gi.iteminstance=$scormid AND gi.itemtype='mod'";
    $res = $DB->get_record_sql($sql);
    if ($res->timemodified) {
        $timemodified = $res->timemodified;
    } else {
        // echo $sql; echo"<br/>";
        $timemodified = 0;
    }
    return $timemodified;
}

?>