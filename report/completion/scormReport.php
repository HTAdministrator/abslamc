<?php
require_once(__DIR__ . '/../../config.php');

$context = context_system::instance();
$url = new moodle_url('/report/completion/scormReport.php');
global $CFG;
/*$PAGE->set_url($url);
$PAGE->set_pagelayout('admin');
$str = 'Progress Report';
$PAGE->set_title($strcompletion);
$PAGE->set_heading($str);*/


$PAGE->set_pagelayout('standard');
$url = new moodle_url('/report/completion/scormReport.php');
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->navbar->ignore_active();
$PAGE->navbar->add('User Scorm Report', $url, navigation_node::TYPE_CONTAINER);
$PAGE->set_title('User Scorm Report');
require_login();
echo $OUTPUT->header();
echo $OUTPUT->heading('User Scorm Report');
 $dashboardurl = $CFG->wwwroot;
if(isset($_GET['courseid'])){
  $courseid=$_GET['courseid'];  
}else{
  $courseid=0;    
}
//echo "<div><a href='" . $dashboardurl . "' class='backbutton pull-right'>" . get_string('backtodashboard') . "</a></div>";
$courses = get_courses('all');

$course_arr = array();
foreach($courses as $course){
    $course_arr[$course->id] = $course->fullname;
}

//echo "<pre>";
//print_r($cohort_arr);
//echo "</pre>";
//echo $OUTPUT->heading('User Progress Reports');

//echo $OUTPUT->box_start('User Progress Reports');
?>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/jquery-3.3.1.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/ui-1.12.1/jquery-ui.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/jquery.dataTables.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.autoFill.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.buttons.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/buttons.colVis.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/buttons.flash.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/buttons.html5.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/buttons.print.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.colReorder.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.fixedColumns.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.fixedHeader.min.js"></script>

<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.responsive.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.rowGroup.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.scroller.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/ie/dataTables.select.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/jszip.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/pdfmake.min.js"></script>
<script src="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/js/vfs_fonts.js"></script>
<link rel="stylesheet" href="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo $CFG->wwwroot ?>/lib/jquery/datatables/css/dataTables.bootstrap.min.css">

<style>

#example th{
    min-width:120px;
}


</style>
<script>


var $j = jQuery.noConflict();
   $j(window).on('load', function(){
        $j('#example').DataTable({
            "ajax": {
                'url': "<?php echo $CFG->wwwroot ?>/report/completion/locallib.php?action=getscormdata&courseid=<?php echo $courseid; ?>",
                'type': 'GET'
            },
            "columns": [
                {"data": "username"},
                {"data": "email"},
                {"data": "phone"},
                {"data": "modulename"},
                {"data": "stime"},
                 {"data": "etime"},
                {"data": "modstatus"},
                {"data": "modgrade"},
  
            ],
            dom: 'Bfrtip',
            "bSort": true,
            "scrollX": true,
          
            buttons: [
            
            'excelHtml5',
            'print',
          
        ]
        });

        
       
    });

</script>


<table id="example" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>User Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Module Name</th>
            <th>Module Start date</th>
             <th>Module Completed/Last Access date</th>
            <th>Module status</th>
            <th>Score</th>
            
          
        </tr>
    </thead>

</table>
<?php
//echo $OUTPUT->box_end();
echo $OUTPUT->footer();
?>