<?php

/* Core */
$string['configtitle'] = 'Herald';
$string['pluginname'] = 'Herald';
$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Herald</h2>
<p><img src="herald/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About Herald</h3>
<p>Herald is a responsive Moodle theme designed for online learning.</p>
<h3>Theme Parents</h3>
<p>This theme is based on the <a href="https://github.com/bmbrands/theme_bootstrap">Bootstrap theme</a>.</p>
<h3>Theme Credits</h3>
<p>Author: 3rd Wave Media<br>
Contact: elearning@3rdwavemedia.com<br>
Website: <a href="http://elearning.3rdwavemedia.com/">elearning.3rdwavemedia.com</a>
</p>
<h3>Theme License</h3>
<p><a href="http://elearning.3rdwavemedia.com/licenses">License details</a></p>
</div></div>';

/* General */
$string['login'] = 'Log in';
$string['geneticsettings'] = 'General Settings';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Use this to add any CSS code you want to override the default theme CSS.';
$string['footerwidget'] = 'Footer Widget Area';
$string['footerwidgetdesc'] = 'This allows you to add extra content which will be displayed in the site footer';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Upload your own logo here if you want to replace the default one in the theme folder.';
$string['bootstrapcdn'] = 'Load FontAwesome from CDN';
$string['bootstrapcdndesc'] = 'Check this box if you\'d like to load FontAwesome from the online Bootstrap CDN source. Enable this if you are having issues getting the FontAwesome icons to display on your site.';
$string['copyright'] = 'Copyright';
$string['copyrightdesc'] = 'Enter the name of your site\'s copyright owner. This will be displayed in the site footer.';
$string['layout'] = 'Left-hand side sidebar';
$string['layoutdesc'] = 'By default the sidebar is displayed on the right-hand side. Check this box if you prefer a left-hand sidebar';

$string['backtotop'] = 'Back to top';
$string['nextsection'] = 'Next Section';
$string['previoussection'] = 'Previous Section';

$string['alwaysdisplay'] = 'Always Show';
$string['displaybeforelogin'] = 'Show before login only';
$string['displayafterlogin'] = 'Show after login only';
$string['dontdisplay'] = 'Never Show';

/* CustomMenu */
$string['mydashboard'] = 'My Dashboard';
$string['myhome'] = 'My Home';
$string['custommenuheading'] = 'Custom Menus';
$string['custommenuheadingsub'] = 'Add additional functionality to your custommenu.';
$string['custommenudesc'] = 'Settings here allow you to add new dynamic functionality to the custommenu (also refered to as Dropdown menu)';

$string['mydashboardinfo'] = 'My Dashboard';
$string['mydashboardinfodesc'] = 'Gives access to a list of the user\'s most visited pages.';
$string['displaymydashboard'] = 'Enable My Dashboard';
$string['displaymydashboarddesc'] = 'Show the "My Dashboard" option in the main menu.';

$string['mycoursesinfo'] = 'My Courses';
$string['mycoursesinfodesc'] = 'Gives access to a list of the user\'s enrolled courses.';
$string['displaymycourses'] = 'Display enrolled courses';
$string['displaymycoursesdesc'] = 'Show the "My Courses" option in the main menu.';

$string['mycoursetitle'] = 'Wording';
$string['mycoursetitledesc'] = 'Change the wording for the "My Courses" link text in the dropdown menu.';
$string['mycourses'] = 'My Courses';
$string['myunits'] = 'My Units';
$string['mylessons'] = 'My Lessons';
$string['mymodules'] = 'My Modules';
$string['myclasses'] = 'My Classes';
$string['allcourses'] = 'All Courses';
$string['allunits'] = 'All Units';
$string['allmodules'] = 'All Modules';
$string['allclasses'] = 'All Classes';
$string['noenrolments'] = 'You have no current enrolments';

/* Navbar Seperator */
$string['navbarsep'] = 'Breadcrumb Separator';
$string['navbarsepdesc'] = 'Use this to change the type of separator displayed in the breadcrumb';
$string['nav_thinbracket'] = 'Thin bracket';
$string['nav_doublebracket'] = 'Double thin bracket';
$string['nav_thickbracket'] = 'Thick Bracket';
$string['nav_slash'] = 'Forward slash';
$string['nav_pipe'] = 'Vertical line';

/* Color Switcher */
$string['colorswitcher'] = 'Color Switcher';
$string['colorswitcherdesc'] = 'Select the site color scheme';

/* Regions */
//$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Sidebar';
$string['region-hidden-dock'] = 'Hidden from users';

/* Homepage Featured Content Blocks Settings */
$string['homeblocksheading'] = 'Frontpage Featured Section';
$string['homeblocksheadingsub'] = 'Display featured courses on the frontpage';
$string['homeblocksdesc'] = 'This allows you to add up to 8 featured content blocks on your site\'s frontpage.';
$string['usehomeblocks'] = 'Enable featured section';
$string['usehomeblocksdesc'] = 'Check this box to show the featured content blocks section on the frontpage';
$string['featuredtitle'] = 'Featured Section Title';
$string['featuredtitledesc'] = 'Enter section title';

$string['featuredleadtext'] = 'Section Title Link Text';
$string['featuredleadtextdesc'] = 'Enter link text';

$string['featuredleadurl'] = 'Section Title Link URL';
$string['featuredleadurldesc'] = 'Enter the target destination of the link';

$string['homeblock1info'] = 'Block 1';
$string['homeblock2info'] = 'Block 2';
$string['homeblock3info'] = 'Block 3';
$string['homeblock4info'] = 'Block 4';
$string['homeblock5info'] = 'Block 5';
$string['homeblock6info'] = 'Block 6';
$string['homeblock7info'] = 'Block 7';
$string['homeblock8info'] = 'Block 8';

$string['homeblock1desc'] = 'Enter the settings for block 1';
$string['homeblock2desc'] = 'Enter the settings for block 2';
$string['homeblock3desc'] = 'Enter the settings for block 3';
$string['homeblock4desc'] = 'Enter the settings for block 4';
$string['homeblock5desc'] = 'Enter the settings for block 5';
$string['homeblock6desc'] = 'Enter the settings for block 6';
$string['homeblock7desc'] = 'Enter the settings for block 7';
$string['homeblock8desc'] = 'Enter the settings for block 8';

$string['homeblocktitle'] = 'Title';
$string['homeblocktitledesc'] = 'Enter the title for this block.';
$string['homeblockimage'] = 'Image';
$string['homeblockimagedesc'] = 'Set an image for this block. Recommended image size: 400px by 180px';
$string['homeblockcontent'] = 'Content';
$string['homeblockcontentdesc'] = 'Add text content for this block.';
$string['homeblockbuttontext'] = 'Call-to-action Button Text';
$string['homeblock2buttontext'] = 'Call-to-action Button Text';
$string['homeblock3buttontext'] = 'Call-to-action Button Text';
$string['homeblockurl'] = 'Target URL';
$string['homeblockbuttonurldesc'] = 'Enter the target destination of the call-to-action button and the block title (i.e http://www.yourwebsite.com/yourpage).';

/* Homepage Benefits */
$string['benefitsheading'] = 'Frontpage Benefits Section';
$string['benefitssub'] = 'Display benefits section on the frontpage';
$string['benefitsdesc'] = 'This allows you to add up to 6 benefits blocks on your site\'s frontpage.';
$string['usebenefits'] = 'Enable benefits section';
$string['usebenefitsdesc'] = 'Check this box to show the benefits section on the frontpage.';
$string['benefitsectiontitle'] = 'Benefits Section Title';
$string['benefitsectiontitledesc'] = 'Enter section title';

$string['benefit1icon'] = 'Icon';
$string['benefit2icon'] = 'Icon';
$string['benefit3icon'] = 'Icon';
$string['benefit4icon'] = 'Icon';
$string['benefit5icon'] = 'Icon';
$string['benefit6icon'] = 'Icon';

$string['benefit1icondesc'] = 'Enter FontAwesome code (<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">reference</a>)';
$string['benefit2icondesc'] = 'Enter FontAwesome code (<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">reference</a>)';
$string['benefit3icondesc'] = 'Enter FontAwesome code (<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">reference</a>)';
$string['benefit4icondesc'] = 'Enter FontAwesome code (<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">reference</a>)';
$string['benefit5icondesc'] = 'Enter FontAwesome code (<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">reference</a>)';
$string['benefit6icondesc'] = 'Enter FontAwesome code (<a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">reference</a>)';

$string['benefit1title'] = 'Title';
$string['benefit2title'] = 'Title';
$string['benefit3title'] = 'Title';
$string['benefit4title'] = 'Title';
$string['benefit5title'] = 'Title';
$string['benefit6title'] = 'Title';

$string['benefit1titledesc'] = 'Enter title';
$string['benefit2titledesc'] = 'Enter title';
$string['benefit3titledesc'] = 'Enter title';
$string['benefit4titledesc'] = 'Enter title';
$string['benefit5titledesc'] = 'Enter title';
$string['benefit6titledesc'] = 'Enter title';

$string['benefit1copy'] = 'Content';
$string['benefit2copy'] = 'Content';
$string['benefit3copy'] = 'Content';
$string['benefit4copy'] = 'Content';
$string['benefit5copy'] = 'Content';
$string['benefit6copy'] = 'Content';

$string['benefit1contentdesc'] = 'Enter content';
$string['benefit2contentdesc'] = 'Enter content';
$string['benefit3contentdesc'] = 'Enter content';
$string['benefit4contentdesc'] = 'Enter content';
$string['benefit5contentdesc'] = 'Enter content';
$string['benefit6contentdesc'] = 'Enter content';

$string['benefit1info'] = 'Benefit 1';
$string['benefit2info'] = 'Benefit 2';
$string['benefit3info'] = 'Benefit 3';
$string['benefit4info'] = 'Benefit 4';
$string['benefit5info'] = 'Benefit 5';
$string['benefit6info'] = 'Benefit 6';

$string['benefit1desc'] = 'Enter the settings for benefit 1';
$string['benefit2desc'] = 'Enter the settings for benefit 2';
$string['benefit3desc'] = 'Enter the settings for benefit 3';
$string['benefit4desc'] = 'Enter the settings for benefit 4';
$string['benefit5desc'] = 'Enter the settings for benefit 5';
$string['benefit6desc'] = 'Enter the settings for benefit 6';

/* Home Video Section */
$string['videoheading'] = 'Frontpage Video Section';
$string['videosub'] = 'Display Youtube video section on the frontpage';
$string['videodesc'] = 'Enter the video settings';
$string['usevideosection'] = 'Enable video section';
$string['usevideosectiondesc'] = 'Check this box to show the video section on the frontpage.';

$string['videosectiontitle'] = 'Video Section Title';
$string['videosectiontitledesc'] = 'Enter section title';

$string['videocontent'] = 'Video Section Content';
$string['videocontentdesc'] = 'Enter section content';

$string['videoid'] = 'Youtube Video ID';
$string['videoid2'] = 'Vimeo Video ID';
$string['videoiddesc'] = 'Enter Youtube video ID (eg. KnZfe1qpaKM)';
$string['videoid2desc'] = 'Enter Vimeo video ID (eg. 21600601)';

/* Slideshow */
$string['slideshowheading'] = 'Frontpage Slideshow';
$string['slideshowheadingsub'] = 'Display a slideshow on the frontpage';
$string['slideshowdesc'] = 'This allows you to add up to 4 slides to promote important areas of your site.';

$string['useslideshow'] = 'Enable slideshow';
$string['useslideshowdesc'] = 'Check this box to show the slideshow section on the frontpage.';

$string['slideshowTitle'] = 'Slideshow';
$string['slideinfodesc'] = 'Enter the settings for your slide.';

$string['slide1'] = 'Slide 1';
$string['slide2'] = 'Slide 2';
$string['slide3'] = 'Slide 3';
$string['slide4'] = 'Slide 4';

$string['slidetitle'] = 'Slide Title';
$string['slidetitledesc'] = 'Enter a title for this slide.';
$string['slidesubtitle'] = 'Slide Subtitle';
$string['slidesubtitledesc'] = 'Enter a subtitle for this slide.';
$string['slideimage'] = 'Slide Image';
$string['slideimagedesc'] = 'Enter the URL to an image to use as the slide of the slideshow. Recommended image size: 1280px by 760px';
$string['slidecaption'] = 'Slide Caption';
$string['slidecaptiondesc'] = 'Enter the caption text for this slide (appears under the slide subtitle).';
$string['slidecta'] = 'Call-to-action Button Text';
$string['slidectadesc'] = 'Enter the text for the call-to-action button';

$string['slideurl'] = 'CTA Button Link';
$string['slideurldesc'] = 'Enter the target destination of this CTA button\'s link (i.e http://www.yourwebsite.com/yourpage).';

/* Social Networks */
$string['enablesocial'] = 'Enable social media section';
$string['enablesocialdesc'] = 'Check to show social media section on the frontpage.';

$string['socialsectiontitle'] = 'Social Media Section Title';
$string['socialsectiontitledesc'] = 'Enter section title';

$string['socialheading'] = 'Frontpage Social Media';
$string['socialheadingsub'] = 'Display social media section on your frontpage.';
$string['socialdesc'] = 'This allows you to add popular social media links.';
$string['socialnetworks'] = 'Social Media';
$string['facebook'] = 'Facebook URL';
$string['facebookdesc'] = 'Enter the URL of your Facebook page. (i.e http://www.facebook.com/yourname)';

$string['twitter'] = 'Twitter URL';
$string['twitterdesc'] = 'Enter the URL of your Twitter homepage. (i.e http://www.twitter.com/yourname)';

$string['googleplus'] = 'Google+ URL';
$string['googleplusdesc'] = 'Enter the URL of your Google+ profile. (i.e http://plus.google.com/107817105228930159735)';

$string['linkedin'] = 'LinkedIn URL';
$string['linkedindesc'] = 'Enter the URL of your LinkedIn profile. (i.e http://www.linkedin.com/company/yourname)';

$string['youtube'] = 'YouTube URL';
$string['youtubedesc'] = 'Enter the URL of your YouTube channel. (i.e http://www.youtube.com/yourname)';

$string['vimeo'] = 'Vimeo URL';
$string['vimeodesc'] = 'Enter the URL of your Vimeo channel. (i.e https://www.vimeo.com/channel/name)';

$string['flickr'] = 'Flickr URL';
$string['flickrdesc'] = 'Enter the URL of your Flickr page. (i.e http://www.flickr.com/yourname)';

$string['skype'] = 'Skype Account';
$string['skypedesc'] = 'Enter your Skype username';

$string['pinterest'] = 'Pinterest URL';
$string['pinterestdesc'] = 'Enter the URL of your Pinterest page. (i.e http://pinterest.com/yourname)';

$string['instagram'] = 'Instagram URL';
$string['instagramdesc'] = 'Enter the URL of your Instagram page. (i.e http://instagram.com/yourname)';

$string['rss'] = 'RSS Feed URL';
$string['rssdesc'] = 'Enter the URL of your RSS Feed. (i.e http://www.yourwebsite.com/feed)';

/* iOS Icons */
$string['iosicon'] = 'iOS Homescreen Icons';
$string['iosicondesc'] = 'Upload your icons for iOS and android homescreens.';

$string['iphoneicon'] = 'iPhone/iPod Touch Icon (Non Retina)';
$string['iphoneicondesc'] = 'Upload the image to be used on non-retina display iPhone/iPod Touch devices. Should be a PNG file sized 57px by 57px';

$string['iphoneretinaicon'] = 'iPhone/iPod Touch Icon (Retina)';
$string['iphoneretinaicondesc'] = 'Upload the image to be used on retina display iPhone/iPod Touch devices. Should be a PNG file sized 114px by 114px';

$string['ipadicon'] = 'iPad Icon (Non Retina)';
$string['ipadicondesc'] = 'Upload the image to be used on non-retina display iPad devices. should be a PNG file sized 72px by 72px';

$string['ipadretinaicon'] = 'iPad Icon (Retina)';
$string['ipadretinaicondesc'] = 'Upload the image to be used on retina display iPad devices. Should be a PNG file sized 144px by 144px';

/* Google Analytics */
$string['analyticsheading'] = 'Google Analytics';
$string['analyticsheadingsub'] = 'Utilise analytics from Google';
$string['analyticsdesc'] = 'Here you can enable Google Analytics for your site. If you do not have one, you will need to sign up for a free account at the Google Analytics site (<a href="http://analytics.google.com" target="_blank">http://analytics.google.com</a>)';

$string['useanalytics'] = 'Enable Google Analytics';
$string['useanalyticsdesc'] = 'Enable or disable Google analytics.';

$string['analyticsid'] = 'Your Tracking ID';
$string['analyticsiddesc'] = 'Enter your Tracking ID for this site from Google Analytics. Typically formatted like UA-XXXXXXXX-X';


/* Alerts */
$string['alertsheading'] = 'Frontpage Alerts';
$string['alertsheadingsub'] = 'Display alerts on the frontpage';
$string['alertsdesc'] = 'This allows you to add up to 3 alerts on the site\'s frontpage.';

$string['enablealert'] = 'Enable this alert';
$string['enablealertdesc'] = 'Enable or disable this alert';

$string['alert1'] = 'Alert 1';
$string['alert2'] = 'Alert 2';
$string['alert3'] = 'Alert 3';
$string['alert1desc'] = 'Add content for this alert';
$string['alert2desc'] = 'Add content for this alert';
$string['alert3desc'] = 'Add content for this alert';

$string['alerttitle'] = 'Title';
$string['alerttitledesc'] = 'Main title/heading for your alert';

$string['alerttype'] = 'Type';
$string['alerttypedesc'] = 'Set the appropriate alert level/type to best inform your users';

$string['alerttext'] = 'Alert Text';
$string['alerttextdesc'] = 'Enter the text for this alert.';

$string['alert_info'] = 'Information';
$string['alert_warning'] = 'Warning';
$string['alert_general'] = 'Announcement';


/* Frontpage Testimonials */
$string['testimonialsheading'] = 'Frontpage Testimonials';
$string['testimonialssub'] = 'Display testimonials on the frontpage';
$string['testimonialsdesc'] = 'This allows you to add up to 6 testimonials on the frontpage';
$string['usetestimonials'] = 'Enable testimonials';
$string['usetestimonialsdesc'] = 'Check this box to show the testimonials section on the frontpage.';
$string['testimonialsectiontitle'] = 'Testimonials Section Title';
$string['testimonialsectiontitledesc'] = 'Enter section title';
$string['testimonial1'] = 'Testimonial 1';
$string['testimonial2'] = 'Testimonial 2';
$string['testimonial3'] = 'Testimonial 3';
$string['testimonial4'] = 'Testimonial 4';
$string['testimonial5'] = 'Testimonial 5';
$string['testimonial6'] = 'Testimonial 6';
$string['testimonial1desc'] = 'Add content for this testimonial';
$string['testimonial2desc'] = 'Add content for this testimonial';
$string['testimonial3desc'] = 'Add content for this testimonial';
$string['testimonial4desc'] = 'Add content for this testimonial';
$string['testimonial5desc'] = 'Add content for this testimonial';
$string['testimonial6desc'] = 'Add content for this testimonial';
$string['testimonialimage'] = 'Image';
$string['testimonialimagedesc'] = 'Upload the image you want to use for this testimonial. Recommended image size: 100px by 100px';
$string['testimonialname'] = 'Source Name';
$string['testimonialnamedesc'] = 'Testimonial source name';
$string['testimonialtitle'] = 'Source Title';
$string['testimonialtitledesc'] = 'Testimonial source title';
$string['testimonialcontent'] = 'Testimonial Quote';
$string['testimonialcontentdesc'] = 'Enter the testimonial quote';


/* Frontpage Logos */
$string['logosheading'] = 'Frontpage logos Section';
$string['logossub'] = 'Display partner/press logos on the frontpage';
$string['logosdesc'] = 'This allows you to add up to 8 logo images on the frontpage.';
$string['uselogos'] = 'Enable logos section';
$string['uselogosdesc'] = 'Check this box to show the logos section on the frontpage.';
$string['logosectiontitle'] = 'Logos Section Title';
$string['logosectiontitledesc'] = 'Enter section title';
$string['logo1'] = 'logo 1';
$string['logo2'] = 'logo 2';
$string['logo3'] = 'logo 3';
$string['logo4'] = 'logo 4';
$string['logo5'] = 'logo 5';
$string['logo6'] = 'logo 6';
$string['logo7'] = 'logo 7';
$string['logo8'] = 'logo 8';
$string['logo1desc'] = 'Add content for this logo';
$string['logo2desc'] = 'Add content for this logo';
$string['logo3desc'] = 'Add content for this logo';
$string['logo4desc'] = 'Add content for this logo';
$string['logo5desc'] = 'Add content for this logo';
$string['logo6desc'] = 'Add content for this logo';
$string['logo7desc'] = 'Add content for this logo';
$string['logo8desc'] = 'Add content for this logo';
$string['logoimage'] = 'Image';
$string['logoimagedesc'] = 'Upload the image you want to use for this logo. Recommended image size: 270px by 88px';
$string['logoalttext'] = 'Alternative Text';
$string['logoalttextdesc'] = 'Alternative text for the logo image';
$string['logourl'] = 'Logo Link';
$string['logourldesc'] = 'Enter the target destination of the logo image (i.e http://www.logowebsite.com/).';


/* Header Settings */
$string['headerheading'] = 'Site Header Settings';
$string['headerheadingsub'] = 'This allows you to add a header widget area next to the logo across the site';
$string['headerdesc'] = 'Enter settings for the site header.';
$string['headerwidget'] = 'Header Widget Area';
$string['headerwidgetdesc'] = 'Ideally this should be used to display social media buttons (eg. Twitter and Facebook like) <a href="http://elearning.3rdwavemedia.com/blog/premium-moodle-themes-ace-and-brizzle/852/" target="_blank">View Tutorial</a>';



/* Footer Settings */
$string['footerblocksheading'] = 'Footer Content Blocks';
$string['footerblocksheadingsub'] = 'Display 3 content blocks in the footer area across the site.';
$string['footerblocksdesc'] = 'This allows you to add 3 content blocks in the site footer area.';
$string['enablefooterblocks'] = 'Enable footer content blocks';
$string['enablefooterblocksdesc'] = 'Check this box to show footer content blocks';
$string['footerblock1'] = 'Footer Block 1';
$string['footerblock2'] = 'Footer Block 2';
$string['footerblock3'] = 'Footer Block 3';
$string['footerblock1desc'] = 'Add content for this block';
$string['footerblock2desc'] = 'Add content for this block';
$string['footerblock3desc'] = 'Add content for this block';

