<?php

$hasvideosectiontitle = (!empty($PAGE->theme->settings->videosectiontitle));
$hasvideocontent = (!empty($PAGE->theme->settings->videocontent ));
$hasvideoid = (!empty($PAGE->theme->settings->videoid ));
$hasvideoid2 = (!empty($PAGE->theme->settings->videoid2 ));

$videosectiontitle = $PAGE->theme->settings->videosectiontitle;
$videocontent = $PAGE->theme->settings->videocontent;
$videoid = $PAGE->theme->settings->videoid;
if ($hasvideoid2) {
   $videoid2 = $PAGE->theme->settings->videoid2; 
}


?>

<?php if($PAGE->theme->settings->usevideosection ==1) { ?>        

    <!-- ******Video Section****** -->
    <section class="section video has-texture">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-8 col-sm-10 col-xs-12 col-md-offset-2 col-sm-offset-1 col-xs-offset-0">
                    <?php if ($hasvideosectiontitle) { ?>
                    <h2 class="title"><?php echo $videosectiontitle ?></h2>
                    <?php } ?>
                    <?php if ($hasvideocontent) { ?>
                    <p class="intro"><?php echo $videocontent ?></p>
                    <?php } ?>
                    <?php if ($hasvideoid) { ?>
                    <div class="video-container video-container-youtube">
                        <iframe width="560" height="315" src="//www.youtube.com/embed/<?php echo $videoid  ?>" frameborder="0" allowfullscreen></iframe>
                        
                    </div><!--//video-container-->
                    <?php } ?>
                    <?php if ($hasvideoid2) { ?>
                    <div class="video-container video-container-viemo">
                        <iframe width="560" height="315" src="https://player.vimeo.com/video/<?php echo $videoid2  ?>?color=ffffff&portrait=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        
                    </div><!--//video-container-->
                    <?php } ?>
                </div>
            </div><!--//row-->       
        </div><!--//container-->
    </section><!--//press-->  
    
<?php }?>