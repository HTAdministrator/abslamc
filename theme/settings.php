<?php
$settings = null;

defined('MOODLE_INTERNAL') || die;
    
	$ADMIN->add('themes', new admin_category('theme_birlasunlife', 'Birla Sun Life'));

	// "genericsettings" settingpage
	$temp = new admin_settingpage('theme_birlasunlife_generic',  get_string('geneticsettings', 'theme_birlasunlife'));
	
    // Logo file setting.   
    $name = 'theme_birlasunlife/logo';
    $title = get_string('logo','theme_birlasunlife');
    $description = get_string('logodesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);  

    // Layout - sidebar position.
    $name = 'theme_birlasunlife/layout';
    $title = get_string('layout', 'theme_birlasunlife');
    $description = get_string('layoutdesc', 'theme_birlasunlife');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Include Awesome Font from Bootstrapcdn
    $name = 'theme_birlasunlife/bootstrapcdn';
    $title = get_string('bootstrapcdn', 'theme_birlasunlife');
    $description = get_string('bootstrapcdndesc', 'theme_birlasunlife');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
	
	// Include Awesome Font from Bootstrapcdn
    $name = 'theme_birlasunlife/frontpagenews';
    $title = get_string('frontpagenewstitle', 'theme_birlasunlife');
    $description = get_string('frontpagenewsdesc', 'theme_birlasunlife');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // Navbar Seperator.
    $name = 'theme_birlasunlife/navbarsep';
    $title = get_string('navbarsep' , 'theme_birlasunlife');
    $description = get_string('navbarsepdesc', 'theme_birlasunlife');
    $nav_thinbracket = get_string('nav_thinbracket', 'theme_birlasunlife');
    $nav_doublebracket = get_string('nav_doublebracket', 'theme_birlasunlife');
    $nav_thickbracket = get_string('nav_thickbracket', 'theme_birlasunlife');
    $nav_slash = get_string('nav_slash', 'theme_birlasunlife');
    $nav_pipe = get_string('nav_pipe', 'theme_birlasunlife');
    $dontdisplay = get_string('dontdisplay', 'theme_birlasunlife');
    $default = '/';
    $choices = array('\f105'=>$nav_thinbracket, '/'=>$nav_slash, '\f101'=>$nav_doublebracket, '\f054'=>$nav_thickbracket, '|'=>$nav_pipe);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // Theme Color Switcher
    $name = 'theme_birlasunlife/birlasunlife';
    $title = get_string('birlasunlife', 'theme_birlasunlife');
    $description = get_string('birlasunlifedesc', 'theme_birlasunlife');
    $default = '1';
    $choices = array(
        '1' => 'Default',
        '2' => 'Blue',
        '3' => 'Green',
        '4' => 'Purple'
    );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Theme Color scheme static content.
    
    $themecolors = '<ul class="settings-theme-colors list-unstyled list-inline">
	 <li class="item">
	    <span class="item-inner">
    	    <span class="color-block" style="background:#28A5A8"></span>
    	    <br>
    	    <span class="color-name">Default</span>
	    </span>
	 </li>
	 <li class="item">
	    <span class="item-inner">
    	   <span class="color-block" style="background:#3375B1"></span>
    	   <br>
    	   <span class="color-name">Blue</span>
        </span>
	 </li>
	 <li class="item">
	    <span class="item-inner">
    	   <span class="color-block" style="background: #47B861"></span>
    	   <br>
    	   <span class="color-name">Green</span>
        </span>
	 </li>
	 <li class="item">
	   <span class="item-inner">
    	   <span class="color-block" style="background: #a06081"></span>
    	   <br>
    	   <span class="color-name">Purple</span>
        </span>
	 </li>	 
</ul>';
    $temp->add(new admin_setting_heading('theme_birlasunlife_switcherheading', '', $themecolors));
        
    
    // Copyright setting.
    $name = 'theme_birlasunlife/copyright';
    $title = get_string('copyright', 'theme_birlasunlife');
    $description = get_string('copyrightdesc', 'theme_birlasunlife');
    $default = '3rd Wave Media';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Footerwidget setting.
    $name = 'theme_birlasunlife/footerwidget';
    $title = get_string('footerwidget', 'theme_birlasunlife');
    $description = get_string('footerwidgetdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Custom CSS file.
    $name = 'theme_birlasunlife/customcss';
    $title = get_string('customcss', 'theme_birlasunlife');
    $description = get_string('customcssdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_birlasunlife', $temp);
    
    /* Header Settings */
    $temp = new admin_settingpage('theme_birlasunlife_header', get_string('headerheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_header', get_string('headerheadingsub', 'theme_birlasunlife'),
            format_text(get_string('headerdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
    
    // Header widget setting
    $name = 'theme_birlasunlife/headerwidget';
    $title = get_string('headerwidget','theme_birlasunlife');
    $description = get_string('headerwidgetdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
        
    $ADMIN->add('theme_birlasunlife', $temp);
    
    /* Custom Menu Settings */
    $temp = new admin_settingpage('theme_birlasunlife_custommenu', get_string('custommenuheading', 'theme_birlasunlife'));
	            
    // Description for mydashboard
    $name = 'theme_birlasunlife/mydashboardinfo';
    $heading = get_string('mydashboardinfo', 'theme_birlasunlife');
    $information = get_string('mydashboardinfodesc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable dashboard display in custommenu.
    $name = 'theme_birlasunlife/displaymydashboard';
    $title = get_string('displaymydashboard', 'theme_birlasunlife');
    $description = get_string('displaymydashboarddesc', 'theme_birlasunlife');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Description for mycourse
    $name = 'theme_birlasunlife/mycoursesinfo';
    $heading = get_string('mycoursesinfo', 'theme_birlasunlife');
    $information = get_string('mycoursesinfodesc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Toggle courses display in custommenu.
    $name = 'theme_birlasunlife/displaymycourses';
    $title = get_string('displaymycourses', 'theme_birlasunlife');
    $description = get_string('displaymycoursesdesc', 'theme_birlasunlife');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Set wording for dropdown course list
	$name = 'theme_birlasunlife/mycoursetitle';
	$title = get_string('mycoursetitle','theme_birlasunlife');
	$description = get_string('mycoursetitledesc', 'theme_birlasunlife');
	$default = 'course';
	$choices = array(
		'course' => get_string('mycourses', 'theme_birlasunlife'),
		'lesson' => get_string('mylessons', 'theme_birlasunlife'),
		'class' => get_string('myclasses', 'theme_birlasunlife'),
		'module' => get_string('mymodules', 'theme_birlasunlife'),
		'Unit' => get_string('myunits', 'theme_birlasunlife'),
	);
	$setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
	$setting->set_updatedcallback('theme_reset_all_caches');
	$temp->add($setting);
    
    $ADMIN->add('theme_birlasunlife', $temp);
    
    
    /* Footer Blocks Settings */
	$temp = new admin_settingpage('theme_birlasunlife_footerblocks', get_string('footerblocksheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_footerblocks', get_string('footerblocksheadingsub', 'theme_birlasunlife'),
            format_text(get_string('footerblocksdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));           
	
	//Enable Footer Blocks.
    $name = 'theme_birlasunlife/enablefooterblocks';
    $title = get_string('enablefooterblocks', 'theme_birlasunlife');
    $description = get_string('enablefooterblocksdesc', 'theme_birlasunlife');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
	
	//Footer Block One.
	$name = 'theme_birlasunlife/footerblock1';
    $title = get_string('footerblock1', 'theme_birlasunlife');
    $description = get_string('footerblock1desc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Footer Block Two.
	$name = 'theme_birlasunlife/footerblock2';
    $title = get_string('footerblock2', 'theme_birlasunlife');
    $description = get_string('footerblock2desc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //Footer Block Three.
	$name = 'theme_birlasunlife/footerblock3';
    $title = get_string('footerblock3', 'theme_birlasunlife');
    $description = get_string('footerblock3desc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
        
    $ADMIN->add('theme_birlasunlife', $temp);
    
    
    /* Frontpage Alerts */
    $temp = new admin_settingpage('theme_birlasunlife_alerts', get_string('alertsheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_alerts', get_string('alertsheadingsub', 'theme_birlasunlife'),
            format_text(get_string('alertsdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
    
    /* Alert 1 */
    
    // Description for Alert One
    $name = 'theme_birlasunlife/alert1info';
    $heading = get_string('alert1', 'theme_birlasunlife');
    $information = get_string('alert1desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert One
    $name = 'theme_birlasunlife/enable1alert';
    $title = get_string('enablealert', 'theme_birlasunlife');
    $description = get_string('enablealertdesc', 'theme_birlasunlife');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Type.
    $name = 'theme_birlasunlife/alert1type';
    $title = get_string('alerttype' , 'theme_birlasunlife');
    $description = get_string('alerttypedesc', 'theme_birlasunlife');
    $alert_info = get_string('alert_info', 'theme_birlasunlife');
    $alert_warning = get_string('alert_warning', 'theme_birlasunlife');
    $alert_general = get_string('alert_general', 'theme_birlasunlife');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Title.
    $name = 'theme_birlasunlife/alert1title';
    $title = get_string('alerttitle', 'theme_birlasunlife');
    $description = get_string('alerttitledesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert One Text.
    $name = 'theme_birlasunlife/alert1text';
    $title = get_string('alerttext', 'theme_birlasunlife');
    $description = get_string('alerttextdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Alert 2 */
    
    // Description for Alert Two
    $name = 'theme_birlasunlife/alert2info';
    $heading = get_string('alert2', 'theme_birlasunlife');
    $information = get_string('alert2desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert Two
    $name = 'theme_birlasunlife/enable2alert';
    $title = get_string('enablealert', 'theme_birlasunlife');
    $description = get_string('enablealertdesc', 'theme_birlasunlife');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Type.
    $name = 'theme_birlasunlife/alert2type';
    $title = get_string('alerttype' , 'theme_birlasunlife');
    $description = get_string('alerttypedesc', 'theme_birlasunlife');
    $alert_info = get_string('alert_info', 'theme_birlasunlife');
    $alert_warning = get_string('alert_warning', 'theme_birlasunlife');
    $alert_general = get_string('alert_general', 'theme_birlasunlife');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Title.
    $name = 'theme_birlasunlife/alert2title';
    $title = get_string('alerttitle', 'theme_birlasunlife');
    $description = get_string('alerttitledesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Two Text.
    $name = 'theme_birlasunlife/alert2text';
    $title = get_string('alerttext', 'theme_birlasunlife');
    $description = get_string('alerttextdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Alert 3 */
    
    // Description for Alert Three.
    $name = 'theme_birlasunlife/alert3info';
    $heading = get_string('alert3', 'theme_birlasunlife');
    $information = get_string('alert3desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Enable Alert Three.
    $name = 'theme_birlasunlife/enable3alert';
    $title = get_string('enablealert', 'theme_birlasunlife');
    $description = get_string('enablealertdesc', 'theme_birlasunlife');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Type.
    $name = 'theme_birlasunlife/alert3type';
    $title = get_string('alerttype' , 'theme_birlasunlife');
    $description = get_string('alerttypedesc', 'theme_birlasunlife');
    $alert_info = get_string('alert_info', 'theme_birlasunlife');
    $alert_warning = get_string('alert_warning', 'theme_birlasunlife');
    $alert_general = get_string('alert_general', 'theme_birlasunlife');
    $default = 'info';
    $choices = array('info'=>$alert_info, 'error'=>$alert_warning, 'success'=>$alert_general);
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Title.
    $name = 'theme_birlasunlife/alert3title';
    $title = get_string('alerttitle', 'theme_birlasunlife');
    $description = get_string('alerttitledesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Alert Three Text.
    $name = 'theme_birlasunlife/alert3text';
    $title = get_string('alerttext', 'theme_birlasunlife');
    $description = get_string('alerttextdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
            
    
    $ADMIN->add('theme_birlasunlife', $temp);
 
 
    /* Slideshow Widget Settings */
    $temp = new admin_settingpage('theme_birlasunlife_slideshow', get_string('slideshowheading', 'theme_birlasunlife'));
    $temp->add(new admin_setting_heading('theme_birlasunlife_slideshow', get_string('slideshowheadingsub', 'theme_birlasunlife'),
            format_text(get_string('slideshowdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
    
    // Enable Slideshow.    
    $name = 'theme_birlasunlife/useslideshow';
    $title = get_string('useslideshow', 'theme_birlasunlife');
    $description = get_string('useslideshowdesc', 'theme_birlasunlife');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    

    /* Slide 1 */
     
    // Description for Slide One
    $name = 'theme_birlasunlife/slide1info';
    $heading = get_string('slide1', 'theme_birlasunlife');
    $information = get_string('slideinfodesc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_birlasunlife/slide1';
    $title = get_string('slidetitle', 'theme_birlasunlife');
    $description = get_string('slidetitledesc', 'theme_birlasunlife');
    $default = 'Slide One Headline';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // ImageURL.
    $name = 'theme_birlasunlife/slide1image';
    $title = get_string('slideimage', 'theme_birlasunlife');
    $description = get_string('slideimagedesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');    
    $temp->add($setting);
    
    // SubTitle.
    $name = 'theme_birlasunlife/slide1subtitle';
    $title = get_string('slidesubtitle', 'theme_birlasunlife');
    $description = get_string('slidesubtitledesc', 'theme_birlasunlife');
    $default = 'Slide One Caption Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_birlasunlife/slide1caption';
    $title = get_string('slidecaption', 'theme_birlasunlife');
    $description = get_string('slidecaptiondesc', 'theme_birlasunlife');
    $default = 'Slide 1 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam malesuada, erat bibendum gravida varius, lectus massa vehicula nibh, et dapibus nisl dui ac neque.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // CTA
    $name = 'theme_birlasunlife/slide1cta';
    $title = get_string('slidecta', 'theme_birlasunlife');
    $description = get_string('slidectadesc', 'theme_birlasunlife');
    $default = 'Button 1 Text';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // CTA URL.
    $name = 'theme_birlasunlife/slide1url';
    $title = get_string('slideurl', 'theme_birlasunlife');
    $description = get_string('slideurldesc', 'theme_birlasunlife');
    $default = '#link1';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    /* Slide 2 */
     
    // Description for Slide One
    $name = 'theme_birlasunlife/slide2info';
    $heading = get_string('slide2', 'theme_birlasunlife');
    $information = get_string('slideinfodesc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_birlasunlife/slide2';
    $title = get_string('slidetitle', 'theme_birlasunlife');
    $description = get_string('slidetitledesc', 'theme_birlasunlife');
    $default = 'Slide Two Headline';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // ImageURL.
    $name = 'theme_birlasunlife/slide2image';
    $title = get_string('slideimage', 'theme_birlasunlife');
    $description = get_string('slideimagedesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');    
    $temp->add($setting);
    
    // SubTitle.
    $name = 'theme_birlasunlife/slide2subtitle';
    $title = get_string('slidesubtitle', 'theme_birlasunlife');
    $description = get_string('slidesubtitledesc', 'theme_birlasunlife');
    $default = 'Slide 2 Caption Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_birlasunlife/slide2caption';
    $title = get_string('slidecaption', 'theme_birlasunlife');
    $description = get_string('slidecaptiondesc', 'theme_birlasunlife');
    $default = 'Slide 2 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam malesuada, erat bibendum gravida varius, lectus massa vehicula nibh, et dapibus nisl dui ac neque.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // CTA
    $name = 'theme_birlasunlife/slide2cta';
    $title = get_string('slidecta', 'theme_birlasunlife');
    $description = get_string('slidectadesc', 'theme_birlasunlife');
    $default = 'Button 2 Text';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // CTA URL.
    $name = 'theme_birlasunlife/slide2url';
    $title = get_string('slideurl', 'theme_birlasunlife');
    $description = get_string('slideurldesc', 'theme_birlasunlife');
    $default = '#link2';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Slide 3 */
     
    // Description for Slide One
    $name = 'theme_birlasunlife/slide3info';
    $heading = get_string('slide3', 'theme_birlasunlife');
    $information = get_string('slideinfodesc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_birlasunlife/slide3';
    $title = get_string('slidetitle', 'theme_birlasunlife');
    $description = get_string('slidetitledesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // ImageURL.
    $name = 'theme_birlasunlife/slide3image';
    $title = get_string('slideimage', 'theme_birlasunlife');
    $description = get_string('slideimagedesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');    
    $temp->add($setting);
    
    // SubTitle.
    $name = 'theme_birlasunlife/slide3subtitle';
    $title = get_string('slidesubtitle', 'theme_birlasunlife');
    $description = get_string('slidesubtitledesc', 'theme_birlasunlife');
    $default = 'Slide 3 Caption Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_birlasunlife/slide3caption';
    $title = get_string('slidecaption', 'theme_birlasunlife');
    $description = get_string('slidecaptiondesc', 'theme_birlasunlife');
    $default = 'Slide 3 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam malesuada, erat bibendum gravida varius, lectus massa vehicula nibh, et dapibus nisl dui ac neque.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // CTA
    $name = 'theme_birlasunlife/slide3cta';
    $title = get_string('slidecta', 'theme_birlasunlife');
    $description = get_string('slidectadesc', 'theme_birlasunlife');
    $default = 'Button 3 Text';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // CTA URL.
    $name = 'theme_birlasunlife/slide3url';
    $title = get_string('slideurl', 'theme_birlasunlife');
    $description = get_string('slideurldesc', 'theme_birlasunlife');
    $default = '#link3';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Slide 4 */
     
    // Description for Slide One
    $name = 'theme_birlasunlife/slide4info';
    $heading = get_string('slide4', 'theme_birlasunlife');
    $information = get_string('slideinfodesc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);

    // Title.
    $name = 'theme_birlasunlife/slide4';
    $title = get_string('slidetitle', 'theme_birlasunlife');
    $description = get_string('slidetitledesc', 'theme_birlasunlife');
    $default = 'Slide Four Headline';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // ImageURL.
    $name = 'theme_birlasunlife/slide4image';
    $title = get_string('slideimage', 'theme_birlasunlife');
    $description = get_string('slideimagedesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');    
    $temp->add($setting);
    
    // SubTitle.
    $name = 'theme_birlasunlife/slide4subtitle';
    $title = get_string('slidesubtitle', 'theme_birlasunlife');
    $description = get_string('slidesubtitledesc', 'theme_birlasunlife');
    $default = 'Slide 4 Caption Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Caption.
    $name = 'theme_birlasunlife/slide4caption';
    $title = get_string('slidecaption', 'theme_birlasunlife');
    $description = get_string('slidecaptiondesc', 'theme_birlasunlife');
    $default = 'Slide 4 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam malesuada, erat bibendum gravida varius, lectus massa vehicula nibh, et dapibus nisl dui ac neque.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // 
    $name = 'theme_birlasunlife/slide4cta';
    $title = get_string('slidecta', 'theme_birlasunlife');
    $description = get_string('slidectadesc', 'theme_birlasunlife');
    $default = 'Button 4 Text';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // CTA URL.
    $name = 'theme_birlasunlife/slide4url';
    $title = get_string('slideurl', 'theme_birlasunlife');
    $description = get_string('slideurldesc', 'theme_birlasunlife');
    $default = '#link4';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add('theme_birlasunlife', $temp);
    
    
    /* Frontpage featured blocks */	
	$temp = new admin_settingpage('theme_birlasunlife_homeblocks', get_string('homeblocksheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_homeblocks', get_string('homeblocksheadingsub', 'theme_birlasunlife'),
            format_text(get_string('homeblocksdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
            
	
	//Enable Home Blocks.
    $name = 'theme_birlasunlife/usehomeblocks';
    $title = get_string('usehomeblocks', 'theme_birlasunlife');
    $description = get_string('usehomeblocksdesc', 'theme_birlasunlife');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Featured Section Title
    $name = 'theme_birlasunlife/featuredtitle';
    $title = get_string('featuredtitle', 'theme_birlasunlife');
    $description = get_string('featuredtitledesc', 'theme_birlasunlife');
    $default = 'Featured Courses';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Section Lead Text
    $name = 'theme_birlasunlife/featuredleadtext';
    $title = get_string('featuredleadtext', 'theme_birlasunlife');
    $description = get_string('featuredleadtextdesc', 'theme_birlasunlife');
    $default = 'View all courses';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Section Lead URL
    $name = 'theme_birlasunlife/featuredleadurl';
    $title = get_string('featuredleadurl', 'theme_birlasunlife');
    $description = get_string('featuredleadurldesc', 'theme_birlasunlife');
    $default = 'course';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
	
	/* Home Block 1 */	
	
    $name = 'theme_birlasunlife/homeblock1info';
    $heading = get_string('homeblock1info', 'theme_birlasunlife');
    $information = get_string('homeblock1desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
	
	$name = 'theme_birlasunlife/homeblock1';
    $title = get_string('homeblocktitle', 'theme_birlasunlife');
    $description = get_string('homeblocktitledesc', 'theme_birlasunlife');
    $default = 'Course One Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock1image';
    $title = get_string('homeblockimage', 'theme_birlasunlife');
    $description = get_string('homeblockimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock1content';
    $title = get_string('homeblockcontent', 'theme_birlasunlife');
    $description = get_string('homeblockcontentdesc', 'theme_birlasunlife');
    $default = 'Course 1 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    
    $name = 'theme_birlasunlife/homeblock1url';
    $title = get_string('homeblockurl', 'theme_birlasunlife');
    $description = get_string('homeblockbuttonurldesc', 'theme_birlasunlife');
    $default = '#link1';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Home Block 2 */    
    $name = 'theme_birlasunlife/homeblock2info';
    $heading = get_string('homeblock2info', 'theme_birlasunlife');
    $information = get_string('homeblock2desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);   
    
	$name = 'theme_birlasunlife/homeblock2';
    $title = get_string('homeblocktitle', 'theme_birlasunlife');
    $description = get_string('homeblocktitledesc', 'theme_birlasunlife');
    $default = 'Course Two Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock2image';
    $title = get_string('homeblockimage', 'theme_birlasunlife');
    $description = get_string('homeblockimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock2content';
    $title = get_string('homeblockcontent', 'theme_birlasunlife');
    $description = get_string('homeblockcontentdesc', 'theme_birlasunlife');
    $default = 'Course 2 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock2url';
    $title = get_string('homeblockurl', 'theme_birlasunlife');
    $description = get_string('homeblockbuttonurldesc', 'theme_birlasunlife');
    $default = '#link2';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Home Block 3 */        
    $name = 'theme_birlasunlife/homeblock3info';
    $heading = get_string('homeblock3info', 'theme_birlasunlife');
    $information = get_string('homeblock3desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
	$name = 'theme_birlasunlife/homeblock3';
    $title = get_string('homeblocktitle', 'theme_birlasunlife');
    $description = get_string('homeblocktitledesc', 'theme_birlasunlife');
    $default = 'Course Three Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock3image';
    $title = get_string('homeblockimage', 'theme_birlasunlife');
    $description = get_string('homeblockimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock3content';
    $title = get_string('homeblockcontent', 'theme_birlasunlife');
    $description = get_string('homeblockcontentdesc', 'theme_birlasunlife');
    $default = 'Course 3 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock3url';
    $title = get_string('homeblockurl', 'theme_birlasunlife');
    $description = get_string('homeblockbuttonurldesc', 'theme_birlasunlife');
    $default = '#link3';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Home Block 4 */    
    $name = 'theme_birlasunlife/homeblock4info';
    $heading = get_string('homeblock4info', 'theme_birlasunlife');
    $information = get_string('homeblock4desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
	$name = 'theme_birlasunlife/homeblock4';
    $title = get_string('homeblocktitle', 'theme_birlasunlife');
    $description = get_string('homeblocktitledesc', 'theme_birlasunlife');
    $default = 'Course Four Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock4image';
    $title = get_string('homeblockimage', 'theme_birlasunlife');
    $description = get_string('homeblockimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock4content';
    $title = get_string('homeblockcontent', 'theme_birlasunlife');
    $description = get_string('homeblockcontentdesc', 'theme_birlasunlife');
    $default = 'Course 4 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock4url';
    $title = get_string('homeblockurl', 'theme_birlasunlife');
    $description = get_string('homeblockbuttonurldesc', 'theme_birlasunlife');
    $default = '#link4';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    /* Home Block 5 */    
    $name = 'theme_birlasunlife/homeblock5info';
    $heading = get_string('homeblock5info', 'theme_birlasunlife');
    $information = get_string('homeblock5desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
	$name = 'theme_birlasunlife/homeblock5';
    $title = get_string('homeblocktitle', 'theme_birlasunlife');
    $description = get_string('homeblocktitledesc', 'theme_birlasunlife');
    $default = 'Course Five Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock5image';
    $title = get_string('homeblockimage', 'theme_birlasunlife');
    $description = get_string('homeblockimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock5image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock5content';
    $title = get_string('homeblockcontent', 'theme_birlasunlife');
    $description = get_string('homeblockcontentdesc', 'theme_birlasunlife');
    $default = 'Course 5 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock5url';
    $title = get_string('homeblockurl', 'theme_birlasunlife');
    $description = get_string('homeblockbuttonurldesc', 'theme_birlasunlife');
    $default = '#link5';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);    
    
    /* Home Block 6 */   
    $name = 'theme_birlasunlife/homeblock6info';
    $heading = get_string('homeblock6info', 'theme_birlasunlife');
    $information = get_string('homeblock6desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
     
	$name = 'theme_birlasunlife/homeblock6';
    $title = get_string('homeblocktitle', 'theme_birlasunlife');
    $description = get_string('homeblocktitledesc', 'theme_birlasunlife');
    $default = 'Course Six Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock6image';
    $title = get_string('homeblockimage', 'theme_birlasunlife');
    $description = get_string('homeblockimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock6image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock6content';
    $title = get_string('homeblockcontent', 'theme_birlasunlife');
    $description = get_string('homeblockcontentdesc', 'theme_birlasunlife');
    $default = 'Course 6 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock6url';
    $title = get_string('homeblockurl', 'theme_birlasunlife');
    $description = get_string('homeblockbuttonurldesc', 'theme_birlasunlife');
    $default = '#link6';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);    
    
    /* Home Block 7 */   
    $name = 'theme_birlasunlife/homeblock7info';
    $heading = get_string('homeblock7info', 'theme_birlasunlife');
    $information = get_string('homeblock7desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
     
	$name = 'theme_birlasunlife/homeblock7';
    $title = get_string('homeblocktitle', 'theme_birlasunlife');
    $description = get_string('homeblocktitledesc', 'theme_birlasunlife');
    $default = 'Course Seven Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock7image';
    $title = get_string('homeblockimage', 'theme_birlasunlife');
    $description = get_string('homeblockimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock7image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock7content';
    $title = get_string('homeblockcontent', 'theme_birlasunlife');
    $description = get_string('homeblockcontentdesc', 'theme_birlasunlife');
    $default = 'Course 7 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock7url';
    $title = get_string('homeblockurl', 'theme_birlasunlife');
    $description = get_string('homeblockbuttonurldesc', 'theme_birlasunlife');
    $default = '#link7';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);    
    
    /* Home Block 8 */ 
    $name = 'theme_birlasunlife/homeblock8info';
    $heading = get_string('homeblock8info', 'theme_birlasunlife');
    $information = get_string('homeblock8desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
       
	$name = 'theme_birlasunlife/homeblock8';
    $title = get_string('homeblocktitle', 'theme_birlasunlife');
    $description = get_string('homeblocktitledesc', 'theme_birlasunlife');
    $default = 'Course Eight Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock8image';
    $title = get_string('homeblockimage', 'theme_birlasunlife');
    $description = get_string('homeblockimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'homeblock8image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock8content';
    $title = get_string('homeblockcontent', 'theme_birlasunlife');
    $description = get_string('homeblockcontentdesc', 'theme_birlasunlife');
    $default = 'Course 8 content goes here Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $name = 'theme_birlasunlife/homeblock8url';
    $title = get_string('homeblockurl', 'theme_birlasunlife');
    $description = get_string('homeblockbuttonurldesc', 'theme_birlasunlife');
    $default = '#link8';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);    
    
    $ADMIN->add('theme_birlasunlife', $temp);
    
    /* Frontpage Benefits */
    $temp = new admin_settingpage('theme_birlasunlife_benefits', get_string('benefitsheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_benefits', get_string('benefitssub', 'theme_birlasunlife'),
            format_text(get_string('benefitsdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
    
    // Enable Benefits Section
    $name = 'theme_birlasunlife/usebenefits';
    $title = get_string('usebenefits', 'theme_birlasunlife');
    $description = get_string('usebenefitsdesc', 'theme_birlasunlife');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Benefits Section Title
    $name = 'theme_birlasunlife/benefitsectiontitle';
    $title = get_string('benefitsectiontitle', 'theme_birlasunlife');
    $description = get_string('benefitsectiontitledesc', 'theme_birlasunlife');
    $default = 'Why Join Us';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // Benefit 1
    
    // Description
    $name = 'theme_birlasunlife/benefit1info';
    $heading = get_string('benefit1info', 'theme_birlasunlife');
    $information = get_string('benefit1desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Icon
    $name = 'theme_birlasunlife/benefit1icon';
    $title = get_string('benefit1icon', 'theme_birlasunlife');
    $description = get_string('benefit1icondesc', 'theme_birlasunlife');    
    $default = 'fa-laptop';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Title
    $name = 'theme_birlasunlife/benefit1title';
    $title = get_string('benefit1title', 'theme_birlasunlife');
    $description = get_string('benefit1titledesc', 'theme_birlasunlife');
    $default = 'Benefit One';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Content
    $name = 'theme_birlasunlife/benefit1content';
    $title = get_string('benefit1copy', 'theme_birlasunlife');
    $description = get_string('benefit1contentdesc', 'theme_birlasunlife');
    $default = 'Benefit 1 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla feugiat libero placerat magna venenatis, eu iaculis neque molestie.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Benefit 2
    
    // Description
    $name = 'theme_birlasunlife/benefit2info';
    $heading = get_string('benefit2info', 'theme_birlasunlife');
    $information = get_string('benefit2desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Icon
    $name = 'theme_birlasunlife/benefit2icon';
    $title = get_string('benefit2icon', 'theme_birlasunlife');
    $description = get_string('benefit2icondesc', 'theme_birlasunlife');
    $default = 'fa-lightbulb-o';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Title
    $name = 'theme_birlasunlife/benefit2title';
    $title = get_string('benefit2title', 'theme_birlasunlife');
    $description = get_string('benefit2titledesc', 'theme_birlasunlife');
    $default = 'Benefit Two';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Content
    $name = 'theme_birlasunlife/benefit2content';
    $title = get_string('benefit2copy', 'theme_birlasunlife');
    $description = get_string('benefit2contentdesc', 'theme_birlasunlife');
    $default = 'Benefit 2 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla feugiat libero placerat magna venenatis, eu iaculis neque molestie.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Benefit 3
    
    // Description
    $name = 'theme_birlasunlife/benefit3info';
    $heading = get_string('benefit3info', 'theme_birlasunlife');
    $information = get_string('benefit3desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Icon
    $name = 'theme_birlasunlife/benefit3icon';
    $title = get_string('benefit3icon', 'theme_birlasunlife');
    $description = get_string('benefit3icondesc', 'theme_birlasunlife');
    $default = 'fa-unlock';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Title
    $name = 'theme_birlasunlife/benefit3title';
    $title = get_string('benefit3title', 'theme_birlasunlife');
    $description = get_string('benefit3titledesc', 'theme_birlasunlife');
    $default = 'Benefit Three';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Content
    $name = 'theme_birlasunlife/benefit3content';
    $title = get_string('benefit3copy', 'theme_birlasunlife');
    $description = get_string('benefit3contentdesc', 'theme_birlasunlife');
    $default = 'Benefit 3 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla feugiat libero placerat magna venenatis, eu iaculis neque molestie.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Benefit 4
    
    // Description
    $name = 'theme_birlasunlife/benefit4info';
    $heading = get_string('benefit4info', 'theme_birlasunlife');
    $information = get_string('benefit4desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Icon
    $name = 'theme_birlasunlife/benefit4icon';
    $title = get_string('benefit4icon', 'theme_birlasunlife');
    $description = get_string('benefit4icondesc', 'theme_birlasunlife');
    $default = 'fa-support';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Title
    $name = 'theme_birlasunlife/benefit4title';
    $title = get_string('benefit4title', 'theme_birlasunlife');
    $description = get_string('benefit4titledesc', 'theme_birlasunlife');
    $default = 'Benefit Four';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Content
    $name = 'theme_birlasunlife/benefit4content';
    $title = get_string('benefit4copy', 'theme_birlasunlife');
    $description = get_string('benefit4contentdesc', 'theme_birlasunlife');
    $default = 'Benefit 4 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla feugiat libero placerat magna venenatis, eu iaculis neque molestie.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Benefit 5
    
    // Description
    $name = 'theme_birlasunlife/benefit5info';
    $heading = get_string('benefit5info', 'theme_birlasunlife');
    $information = get_string('benefit5desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Icon
    $name = 'theme_birlasunlife/benefit5icon';
    $title = get_string('benefit5icon', 'theme_birlasunlife');
    $description = get_string('benefit5icondesc', 'theme_birlasunlife');
    $default = 'fa-group';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Title
    $name = 'theme_birlasunlife/benefit5title';
    $title = get_string('benefit5title', 'theme_birlasunlife');
    $description = get_string('benefit5titledesc', 'theme_birlasunlife');
    $default = 'Benefit Five';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Content
    $name = 'theme_birlasunlife/benefit5content';
    $title = get_string('benefit5copy', 'theme_birlasunlife');
    $description = get_string('benefit5contentdesc', 'theme_birlasunlife');
    $default = 'Benefit 5 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla feugiat libero placerat magna venenatis, eu iaculis neque molestie.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);


    // Benefit 6
    
    // Description
    $name = 'theme_birlasunlife/benefit6info';
    $heading = get_string('benefit6info', 'theme_birlasunlife');
    $information = get_string('benefit6desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Icon
    $name = 'theme_birlasunlife/benefit6icon';
    $title = get_string('benefit6icon', 'theme_birlasunlife');
    $description = get_string('benefit6icondesc', 'theme_birlasunlife');
    $default = 'fa-mobile';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Title
    $name = 'theme_birlasunlife/benefit6title';
    $title = get_string('benefit6title', 'theme_birlasunlife');
    $description = get_string('benefit6titledesc', 'theme_birlasunlife');
    $default = 'Benefit Six';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Content
    $name = 'theme_birlasunlife/benefit6content';
    $title = get_string('benefit6copy', 'theme_birlasunlife');
    $description = get_string('benefit6contentdesc', 'theme_birlasunlife');
    $default = 'Benefit 6 description goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla feugiat libero placerat magna venenatis, eu iaculis neque molestie.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);   
    
    $ADMIN->add('theme_birlasunlife', $temp);
    
    /* Frontpage Testimonials */
    
    $temp = new admin_settingpage('theme_birlasunlife_testimonials', get_string('testimonialsheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_testimonials', get_string('testimonialssub', 'theme_birlasunlife'),
            format_text(get_string('testimonialsdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
    
    // Enable Testimonails section
    $name = 'theme_birlasunlife/usetestimonials';
    $title = get_string('usetestimonials', 'theme_birlasunlife');
    $description = get_string('usetestimonialsdesc', 'theme_birlasunlife');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Section Title
    $name = 'theme_birlasunlife/testimonialsectiontitle';
    $title = get_string('testimonialsectiontitle', 'theme_birlasunlife');
    $description = get_string('testimonialsectiontitledesc', 'theme_birlasunlife');
    $default = 'What are people saying';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    /* Testimonial 1 */
    
    // Description for Testimonial One
    $name = 'theme_birlasunlife/testimonial1info';
    $heading = get_string('testimonial1', 'theme_birlasunlife');
    $information = get_string('testimonial1desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_birlasunlife/testimonial1image';
    $title = get_string('testimonialimage', 'theme_birlasunlife');
    $description = get_string('testimonialimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_birlasunlife/testimonial1name';
    $title = get_string('testimonialname', 'theme_birlasunlife');
    $description = get_string('testimonialnamedesc', 'theme_birlasunlife');
    $default = 'User One Name';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_birlasunlife/testimonial1title';
    $title = get_string('testimonialtitle', 'theme_birlasunlife');
    $description = get_string('testimonialtitledesc', 'theme_birlasunlife');
    $default = 'User One Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_birlasunlife/testimonial1content';
    $title = get_string('testimonialcontent', 'theme_birlasunlife');
    $description = get_string('testimonialcontentdesc', 'theme_birlasunlife');
    $default = 'User 1 testimonial goes here. Pellentesque porttitor magna quis elementum lobortis. Nunc vitae massa vitae elementum lobortis.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 2 */
    
    // Description for Testimonial Two
    $name = 'theme_birlasunlife/testimonial2info';
    $heading = get_string('testimonial2', 'theme_birlasunlife');
    $information = get_string('testimonial2desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_birlasunlife/testimonial2image';
    $title = get_string('testimonialimage', 'theme_birlasunlife');
    $description = get_string('testimonialimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_birlasunlife/testimonial2name';
    $title = get_string('testimonialname', 'theme_birlasunlife');
    $description = get_string('testimonialnamedesc', 'theme_birlasunlife');
    $default = 'User Two Name';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_birlasunlife/testimonial2title';
    $title = get_string('testimonialtitle', 'theme_birlasunlife');
    $description = get_string('testimonialtitledesc', 'theme_birlasunlife');
    $default = 'User Two Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_birlasunlife/testimonial2content';
    $title = get_string('testimonialcontent', 'theme_birlasunlife');
    $description = get_string('testimonialcontentdesc', 'theme_birlasunlife');
    $default = 'User 2 testimonial goes here. Pellentesque porttitor magna quis elementum lobortis. Nunc vitae massa vitae elementum lobortis.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 3 */
    
    // Description for Testimonial Three
    $name = 'theme_birlasunlife/testimonial3info';
    $heading = get_string('testimonial3', 'theme_birlasunlife');
    $information = get_string('testimonial3desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_birlasunlife/testimonial3image';
    $title = get_string('testimonialimage', 'theme_birlasunlife');
    $description = get_string('testimonialimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_birlasunlife/testimonial3name';
    $title = get_string('testimonialname', 'theme_birlasunlife');
    $description = get_string('testimonialnamedesc', 'theme_birlasunlife');
    $default = 'User Three Name';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_birlasunlife/testimonial3title';
    $title = get_string('testimonialtitle', 'theme_birlasunlife');
    $description = get_string('testimonialtitledesc', 'theme_birlasunlife');
    $default = 'User Three Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_birlasunlife/testimonial3content';
    $title = get_string('testimonialcontent', 'theme_birlasunlife');
    $description = get_string('testimonialcontentdesc', 'theme_birlasunlife');
    $default = 'User 3 testimonial goes here. Pellentesque porttitor magna quis elementum lobortis. Nunc vitae massa vitae elementum lobortis.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 4 */
    
    // Description for Testimonial Four
    $name = 'theme_birlasunlife/testimonial4info';
    $heading = get_string('testimonial4', 'theme_birlasunlife');
    $information = get_string('testimonial4desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_birlasunlife/testimonial4image';
    $title = get_string('testimonialimage', 'theme_birlasunlife');
    $description = get_string('testimonialimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_birlasunlife/testimonial4name';
    $title = get_string('testimonialname', 'theme_birlasunlife');
    $description = get_string('testimonialnamedesc', 'theme_birlasunlife');
    $default = 'User Four Name';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_birlasunlife/testimonial4title';
    $title = get_string('testimonialtitle', 'theme_birlasunlife');
    $description = get_string('testimonialtitledesc', 'theme_birlasunlife');
    $default = 'User Four Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_birlasunlife/testimonial4content';
    $title = get_string('testimonialcontent', 'theme_birlasunlife');
    $description = get_string('testimonialcontentdesc', 'theme_birlasunlife');
    $default = 'User 4 testimonial goes here. Pellentesque porttitor magna quis elementum lobortis. Nunc vitae massa vitae elementum lobortis.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    /* Testimonial 5 */
    
    // Description for Testimonial Four
    $name = 'theme_birlasunlife/testimonial5info';
    $heading = get_string('testimonial5', 'theme_birlasunlife');
    $information = get_string('testimonial5desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_birlasunlife/testimonial5image';
    $title = get_string('testimonialimage', 'theme_birlasunlife');
    $description = get_string('testimonialimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial5image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_birlasunlife/testimonial5name';
    $title = get_string('testimonialname', 'theme_birlasunlife');
    $description = get_string('testimonialnamedesc', 'theme_birlasunlife');
    $default = 'User Five Name';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_birlasunlife/testimonial5title';
    $title = get_string('testimonialtitle', 'theme_birlasunlife');
    $description = get_string('testimonialtitledesc', 'theme_birlasunlife');
    $default = 'User Five Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_birlasunlife/testimonial5content';
    $title = get_string('testimonialcontent', 'theme_birlasunlife');
    $description = get_string('testimonialcontentdesc', 'theme_birlasunlife');
    $default = 'User 5 testimonial goes here. Pellentesque porttitor magna quis elementum lobortis. Nunc vitae massa vitae elementum lobortis.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Testimonial 6 */
    
    // Description for Testimonial Four
    $name = 'theme_birlasunlife/testimonial6info';
    $heading = get_string('testimonial6', 'theme_birlasunlife');
    $information = get_string('testimonial6desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Testimonial Image 
    $name = 'theme_birlasunlife/testimonial6image';
    $title = get_string('testimonialimage', 'theme_birlasunlife');
    $description = get_string('testimonialimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'testimonial6image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Name
    $name = 'theme_birlasunlife/testimonial6name';
    $title = get_string('testimonialname', 'theme_birlasunlife');
    $description = get_string('testimonialnamedesc', 'theme_birlasunlife');
    $default = 'User Six Name';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Profile Title
    $name = 'theme_birlasunlife/testimonial6title';
    $title = get_string('testimonialtitle', 'theme_birlasunlife');
    $description = get_string('testimonialtitledesc', 'theme_birlasunlife');
    $default = 'User Six Title';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Testimonial Content 
    $name = 'theme_birlasunlife/testimonial6content';
    $title = get_string('testimonialcontent', 'theme_birlasunlife');
    $description = get_string('testimonialcontentdesc', 'theme_birlasunlife');
    $default = 'User 6 testimonial goes here. Pellentesque porttitor magna quis elementum lobortis. Nunc vitae massa vitae elementum lobortis.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_birlasunlife', $temp);
    
    
    /* Frontpage Video Section */
    
    $temp = new admin_settingpage('theme_birlasunlife_video', get_string('videoheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_video', get_string('videosub', 'theme_birlasunlife'),
            format_text(get_string('videodesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
    
    // Enable video Section
    $name = 'theme_birlasunlife/usevideosection';
    $title = get_string('usevideosection', 'theme_birlasunlife');
    $description = get_string('usevideosectiondesc', 'theme_birlasunlife');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // Frontpage Video Section Title
    $name = 'theme_birlasunlife/homeBannerVideo';
    $title = get_string('homeBannerVideoTitle', 'theme_birlasunlife');
    $description =get_string('homeBannerVideoDesc', 'theme_birlasunlife');
    $default = 'http://download.openbricks.org/sample/H264/big_buck_bunny_720p_H264_AAC_25fps_3400K.MP4';
    $setting = new admin_setting_configtext($name, $title, $description, $default,PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Enable video Section
    $name = 'theme_birlasunlife/usevideosection1';
    $title = get_string('usevideosection', 'theme_birlasunlife');
    $description = get_string('usevideosectiondesc', 'theme_birlasunlife');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Video Section Title
    $name = 'theme_birlasunlife/videosectiontitle';
    $title = get_string('videosectiontitle', 'theme_birlasunlife');
    $description = get_string('videosectiontitledesc', 'theme_birlasunlife');
    $default = 'Our Story';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Video content
    $name = 'theme_birlasunlife/videocontent';
    $title = get_string('videocontent', 'theme_birlasunlife');
    $description = get_string('videocontentdesc', 'theme_birlasunlife');
    $default = 'Section description goes here. Vestibulum lectus lectus, interdum faucibus ultricies non, hendrerit in nunc. Vestibulum ultricies sem vitae aliquam aliquam. Mauris congue mattis diam, ac iaculis ante efficitur eget. Suspendisse vehicula enim id lorem convallis, ac facilisis diam tincidunt. Morbi eget tincidunt purus. Praesent ornare arcu diam, nec imperdiet justo blandit non. Cras mi odio, rutrum non ultrices viverra, tincidunt nec augue.';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    // Frontpage Youtube Video ID
    $name = 'theme_birlasunlife/videoid';
    $title = get_string('videoid', 'theme_birlasunlife');
    $description = get_string('videoiddesc', 'theme_birlasunlife');
    $default = 'KnZfe1qpaKM';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Frontpage Vimeo Video ID
    $name = 'theme_birlasunlife/videoid2';
    $title = get_string('videoid2', 'theme_birlasunlife');
    $description = get_string('videoid2desc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);    
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
        
    $ADMIN->add('theme_birlasunlife', $temp);
    
    
    /* Frontpage logos */
    $temp = new admin_settingpage('theme_birlasunlife_logos', get_string('logosheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_logos', get_string('logossub', 'theme_birlasunlife'),
            format_text(get_string('logosdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
    
    // Enable logos Section
    $name = 'theme_birlasunlife/uselogos';
    $title = get_string('uselogos', 'theme_birlasunlife');
    $description = get_string('uselogosdesc', 'theme_birlasunlife');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Section Title
    $name = 'theme_birlasunlife/logosectiontitle';
    $title = get_string('logosectiontitle', 'theme_birlasunlife');
    $description = get_string('logosectiontitledesc', 'theme_birlasunlife');
    $default = 'Our Partners';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Logo 1 */
    
    // Description for logo One
    $name = 'theme_birlasunlife/logo1info';
    $heading = get_string('logo1', 'theme_birlasunlife');
    $information = get_string('logo1desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    
    // Logo Image 
    $name = 'theme_birlasunlife/logo1image';
    $title = get_string('logoimage', 'theme_birlasunlife');
    $description = get_string('logoimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo1image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Image Alt Text
    $name = 'theme_birlasunlife/logo1alttext';
    $title = get_string('logoalttext', 'theme_birlasunlife');
    $description = get_string('logoalttextdesc', 'theme_birlasunlife');
    $default = 'logo';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    

    // Logo URL
    $name = 'theme_birlasunlife/logo1url';
    $title = get_string('logourl', 'theme_birlasunlife');
    $description = get_string('logourldesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    
    /* Logo 2 */
    
    // Description for logo Two
    $name = 'theme_birlasunlife/logo2info';
    $heading = get_string('logo2', 'theme_birlasunlife');
    $information = get_string('logo2desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Logo Image 
    $name = 'theme_birlasunlife/logo2image';
    $title = get_string('logoimage', 'theme_birlasunlife');
    $description = get_string('logoimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo2image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Image Alt Text
    $name = 'theme_birlasunlife/logo2alttext';
    $title = get_string('logoalttext', 'theme_birlasunlife');
    $description = get_string('logoalttextdesc', 'theme_birlasunlife');
    $default = 'logo';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Logo URL
    $name = 'theme_birlasunlife/logo2url';
    $title = get_string('logourl', 'theme_birlasunlife');
    $description = get_string('logourldesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    /* Logo 3 */
    
    // Description for logo Three
    $name = 'theme_birlasunlife/logo3info';
    $heading = get_string('logo3', 'theme_birlasunlife');
    $information = get_string('logo3desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Logo Image 
    $name = 'theme_birlasunlife/logo3image';
    $title = get_string('logoimage', 'theme_birlasunlife');
    $description = get_string('logoimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo3image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Image Alt Text
    $name = 'theme_birlasunlife/logo3alttext';
    $title = get_string('logoalttext', 'theme_birlasunlife');
    $description = get_string('logoalttextdesc', 'theme_birlasunlife');
    $default = 'logo';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Logo URL
    $name = 'theme_birlasunlife/logo3url';
    $title = get_string('logourl', 'theme_birlasunlife');
    $description = get_string('logourldesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Logo 4 */
    
    // Description for logo Four
    $name = 'theme_birlasunlife/logo4info';
    $heading = get_string('logo4', 'theme_birlasunlife');
    $information = get_string('logo4desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Logo Image 
    $name = 'theme_birlasunlife/logo4image';
    $title = get_string('logoimage', 'theme_birlasunlife');
    $description = get_string('logoimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo4image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Image Alt Text
    $name = 'theme_birlasunlife/logo4alttext';
    $title = get_string('logoalttext', 'theme_birlasunlife');
    $description = get_string('logoalttextdesc', 'theme_birlasunlife');
    $default = 'logo';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Logo URL
    $name = 'theme_birlasunlife/logo4url';
    $title = get_string('logourl', 'theme_birlasunlife');
    $description = get_string('logourldesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Logo 5 */
    
    // Description for logo Five
    $name = 'theme_birlasunlife/logo5info';
    $heading = get_string('logo5', 'theme_birlasunlife');
    $information = get_string('logo5desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Logo Image 
    $name = 'theme_birlasunlife/logo5image';
    $title = get_string('logoimage', 'theme_birlasunlife');
    $description = get_string('logoimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo5image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Image Alt Text
    $name = 'theme_birlasunlife/logo5alttext';
    $title = get_string('logoalttext', 'theme_birlasunlife');
    $description = get_string('logoalttextdesc', 'theme_birlasunlife');
    $default = 'logo';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Logo URL
    $name = 'theme_birlasunlife/logo5url';
    $title = get_string('logourl', 'theme_birlasunlife');
    $description = get_string('logourldesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Logo 6 */
    
    // Description for logo Six
    $name = 'theme_birlasunlife/logo6info';
    $heading = get_string('logo6', 'theme_birlasunlife');
    $information = get_string('logo6desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Logo Image 
    $name = 'theme_birlasunlife/logo6image';
    $title = get_string('logoimage', 'theme_birlasunlife');
    $description = get_string('logoimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo6image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Image Alt Text
    $name = 'theme_birlasunlife/logo6alttext';
    $title = get_string('logoalttext', 'theme_birlasunlife');
    $description = get_string('logoalttextdesc', 'theme_birlasunlife');
    $default = 'logo';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Logo URL
    $name = 'theme_birlasunlife/logo6url';
    $title = get_string('logourl', 'theme_birlasunlife');
    $description = get_string('logourldesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Logo 7 */
    
    // Description for logo Six
    $name = 'theme_birlasunlife/logo7info';
    $heading = get_string('logo7', 'theme_birlasunlife');
    $information = get_string('logo7desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Logo Image 
    $name = 'theme_birlasunlife/logo7image';
    $title = get_string('logoimage', 'theme_birlasunlife');
    $description = get_string('logoimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo7image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Image Alt Text
    $name = 'theme_birlasunlife/logo7alttext';
    $title = get_string('logoalttext', 'theme_birlasunlife');
    $description = get_string('logoalttextdesc', 'theme_birlasunlife');
    $default = 'logo';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Logo URL
    $name = 'theme_birlasunlife/logo7url';
    $title = get_string('logourl', 'theme_birlasunlife');
    $description = get_string('logourldesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    /* Logo 8 */
    
    // Description for logo Six
    $name = 'theme_birlasunlife/logo8info';
    $heading = get_string('logo8', 'theme_birlasunlife');
    $information = get_string('logo8desc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // Logo Image 
    $name = 'theme_birlasunlife/logo8image';
    $title = get_string('logoimage', 'theme_birlasunlife');
    $description = get_string('logoimagedesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo8image');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    //logo Image Alt Text
    $name = 'theme_birlasunlife/logo8alttext';
    $title = get_string('logoalttext', 'theme_birlasunlife');
    $description = get_string('logoalttextdesc', 'theme_birlasunlife');
    $default = 'logo';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Logo URL
    $name = 'theme_birlasunlife/logo8url';
    $title = get_string('logourl', 'theme_birlasunlife');
    $description = get_string('logourldesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
       
    $ADMIN->add('theme_birlasunlife', $temp);

	/* Social Media Settings */
	$temp = new admin_settingpage('theme_birlasunlife_social', get_string('socialheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_social', get_string('socialheadingsub', 'theme_birlasunlife'),
            format_text(get_string('socialdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
            
    // Enable social media 
    $name = 'theme_birlasunlife/enablesocial';
    $title = get_string('enablesocial', 'theme_birlasunlife');
    $description = get_string('enablesocialdesc', 'theme_birlasunlife');
    $default = true;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Social Media Section Title
    $name = 'theme_birlasunlife/socialsectiontitle';
    $title = get_string('socialsectiontitle', 'theme_birlasunlife');
    $description = get_string('socialsectiontitledesc', 'theme_birlasunlife');
    $default = 'Get Connected';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
            
    // Twitter url setting.
    $name = 'theme_birlasunlife/twitter';
    $title = get_string('twitter', 'theme_birlasunlife');
    $description = get_string('twitterdesc', 'theme_birlasunlife');
    $default = '#twitter-link';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Facebook url setting.
    $name = 'theme_birlasunlife/facebook';
    $title = get_string('facebook', 'theme_birlasunlife');
    $description = get_string('facebookdesc', 'theme_birlasunlife');
    $default = '#facebook-link';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // YouTube url setting.
    $name = 'theme_birlasunlife/youtube';
    $title = get_string('youtube', 'theme_birlasunlife');
    $description = get_string('youtubedesc', 'theme_birlasunlife');
    $default = '#youtube-link';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Vimeo url setting.
    $name = 'theme_birlasunlife/vimeo';
    $title = get_string('vimeo', 'theme_birlasunlife');
    $description = get_string('vimeodesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // LinkedIn url setting.
    $name = 'theme_birlasunlife/linkedin';
    $title = get_string('linkedin', 'theme_birlasunlife');
    $description = get_string('linkedindesc', 'theme_birlasunlife');
    $default = '#';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Flickr url setting.
    $name = 'theme_birlasunlife/flickr';
    $title = get_string('flickr', 'theme_birlasunlife');
    $description = get_string('flickrdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Google+ url setting.
    $name = 'theme_birlasunlife/googleplus';
    $title = get_string('googleplus', 'theme_birlasunlife');
    $description = get_string('googleplusdesc', 'theme_birlasunlife');
    $default = '#google-link';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Pinterest url setting.
    $name = 'theme_birlasunlife/pinterest';
    $title = get_string('pinterest', 'theme_birlasunlife');
    $description = get_string('pinterestdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Instagram url setting.
    $name = 'theme_birlasunlife/instagram';
    $title = get_string('instagram', 'theme_birlasunlife');
    $description = get_string('instagramdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Skype account setting.
    $name = 'theme_birlasunlife/skype';
    $title = get_string('skype', 'theme_birlasunlife');
    $description = get_string('skypedesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // RSS url setting.
    $name = 'theme_birlasunlife/rss';
    $title = get_string('rss', 'theme_birlasunlife');
    $description = get_string('rssdesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_URL);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    
    //Description for iOS Icons
    $name = 'theme_birlasunlife/iosiconinfo';
    $heading = get_string('iosicon', 'theme_birlasunlife');
    $information = get_string('iosicondesc', 'theme_birlasunlife');
    $setting = new admin_setting_heading($name, $heading, $information);
    $temp->add($setting);
    
    // iPhone Icon.
    $name = 'theme_birlasunlife/iphoneicon';
    $title = get_string('iphoneicon', 'theme_birlasunlife');
    $description = get_string('iphoneicondesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'iphoneicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPhone Retina Icon.
    $name = 'theme_birlasunlife/iphoneretinaicon';
    $title = get_string('iphoneretinaicon', 'theme_birlasunlife');
    $description = get_string('iphoneretinaicondesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'iphoneretinaicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPad Icon.
    $name = 'theme_birlasunlife/ipadicon';
    $title = get_string('ipadicon', 'theme_birlasunlife');
    $description = get_string('ipadicondesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'ipadicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // iPad Retina Icon.
    $name = 'theme_birlasunlife/ipadretinaicon';
    $title = get_string('ipadretinaicon', 'theme_birlasunlife');
    $description = get_string('ipadretinaicondesc', 'theme_birlasunlife');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'ipadretinaicon');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_birlasunlife', $temp);
       
    
    /* Analytics Settings */
    $temp = new admin_settingpage('theme_birlasunlife_analytics', get_string('analyticsheading', 'theme_birlasunlife'));
	$temp->add(new admin_setting_heading('theme_birlasunlife_analytics', get_string('analyticsheadingsub', 'theme_birlasunlife'),
            format_text(get_string('analyticsdesc' , 'theme_birlasunlife'), FORMAT_MARKDOWN)));
    
    // Enable Analytics
    $name = 'theme_birlasunlife/useanalytics';
    $title = get_string('useanalytics', 'theme_birlasunlife');
    $description = get_string('useanalyticsdesc', 'theme_birlasunlife');
    $default = false;
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    // Google Analytics ID
    $name = 'theme_birlasunlife/analyticsid';
    $title = get_string('analyticsid', 'theme_birlasunlife');
    $description = get_string('analyticsiddesc', 'theme_birlasunlife');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
        
    $ADMIN->add('theme_birlasunlife', $temp);
