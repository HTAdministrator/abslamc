<?php

$usevideosection = (!empty($PAGE->theme->settings->usevideosection));
$homeBannerVideo = $PAGE->theme->settings->homeBannerVideo;
if($usevideosection){
?>
<section class="banner-section">
	<span class="fa fa-times"></span>
	<video class="banner-video-player" poster="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/video_static_image.png" autoplay>
		<!--theme/birlasunlife/pix/video/video.mp4--->
	  <source src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/video/Building for the Future.mp4" type="video/mp4">
	  <source src="movie.ogg" type="video/ogg">
	  Your browser does not support the video tag.
	</video>

	<div class="title-heading">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="title-heading-content">
						<span><i>jaanoge</i> tabhi</span>
						<span>toh <i>maanoge</i></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="playpause"><span class="fa fa-play"></span><span class="fa fa-pause"></span></div>

</section>
<?php } ?>

