<?php
$hascopyright = (empty($PAGE->theme->settings->copyright)) ? false : $PAGE->theme->settings->copyright;
$hasfooterwidget = (empty($PAGE->theme->settings->footerwidget)) ? false : $PAGE->theme->settings->footerwidget;

/* Footer blocks settings */
$hasfooterblocks = (empty($PAGE->theme->settings->enablefooterblocks)) ? false : $PAGE->theme->settings->enablefooterblocks;
$hasfooterblock1 = (empty($PAGE->theme->settings->footerblock1)) ? false : $PAGE->theme->settings->footerblock1;
$hasfooterblock2 = (empty($PAGE->theme->settings->footerblock2)) ? false : $PAGE->theme->settings->footerblock2;
$hasfooterblock3 = (empty($PAGE->theme->settings->footerblock3)) ? false : $PAGE->theme->settings->footerblock3;

$footerblock1 = $PAGE->theme->settings->footerblock1;
$footerblock2 = $PAGE->theme->settings->footerblock2;
$footerblock3 = $PAGE->theme->settings->footerblock3;

$footerwidget = $PAGE->theme->settings->footerwidget;

// OLD ICONS
// $twittericonurl = $OUTPUT->pix_url('birla_images/twitter', 'theme_birlasunlife');
// $fbiconurl = $OUTPUT->pix_url('birla_images/fb', 'theme_birlasunlife');
// $linkediniconurl = $OUTPUT->pix_url('birla_images/linkedin', 'theme_birlasunlife');

// NEW ICONS
$twittericonurl = 'https://mutualfund.adityabirlacapital.com/-/media/bsl/images/footer/footersocialicons/twitter.png?h=25&w=25&la=en&hash=87A1848993F129B046617ABBAC3411C1298A0A25';
$fbiconurl = 'https://mutualfund.adityabirlacapital.com/-/media/bsl/images/footer/footersocialicons/fb.png?h=25&w=25&la=en&hash=8F32C9A6C09CF3EB37DBDB50438957D53DBA7781';
$linkediniconurl = 'https://mutualfund.adityabirlacapital.com/-/media/bsl/images/footer/footersocialicons/linkedin.png?h=25&w=25&la=en&hash=3BE6B032D9DC412DD6483C7F2DE39628AC3BB1C5';
?>
<!-- Start .page-footer-->
<div>
    <div class="row">
    </div>

    <div class="row footerCols">
        <div class="columWrapper clearfix">
            <div class="col-md-10 col-sm-12 col-xs-12">
                <ul class="clearfix footerMenus">
                    <li>
                        <h3>About Us</h3>
                        <ul class="links">
                            <li>
                                <a href="/aboutus">Overview</a>
                            </li>
                            <li>
                                <a href="/aboutus/our-people">Our People</a>
                            </li>
                            <li>
                                <a href="/financials">Financials</a>
                            </li>
                            <li>
                                <a href="https://adityabirla.taleo.net/careersection/fin_serv/jobsearch.ftl?lang=en&amp;radiusType=K&amp;searchExpanded=false&amp;organization=7031060141771&amp;radius=1&amp;portal=8260483274" target="_blank">Careers</a>
                            </li>
                            <li>
                                <a href="https://adityabirlacapital.com/Press-and-Media" target="_blank">Press &amp; Media</a>
                            </li>
                            <li>
                                <a href="/resources">Resources</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h3>Funds</h3>
                        <ul class="links">
                            <li>
                                <a href="/debt-funds">Savings Solution</a>
                            </li>
                            <li>
                                <a href="/income-funds">Income Solution</a>
                            </li>
                            <li>
                                <a href="/equity-funds">Wealth Creation</a>
                            </li>
                            <li>
                                <a href="/elss-funds">Tax Solution</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h3>Goal Planning</h3>
                        <ul class="links">
                            <li>
                                <a href="https://sipnow.birlasunlife.com/sipnow.aspx?golki=GOAL001" target="_blank">Buy a Car</a>
                            </li>
                            <li>
                                <a href="https://sipnow.birlasunlife.com/sipnow.aspx?golki=GOAL003" target="_blank">Child Education</a>
                            </li>
                            <li>
                                <a href="https://sipnow.birlasunlife.com/sipnow.aspx?golki=GOAL0011" target="_blank"> Children's Marriage</a>
                            </li>
                            <li>
                                <a href="https://sipnow.birlasunlife.com/sipnow.aspx?golki=GOAL002" target="_blank">Dream House</a>
                            </li>
                            <li>
                                <a href="https://sipnow.birlasunlife.com/sipnow.aspx?golki=GOAL004" target="_blank"> Retirement</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h3>Investor Solutions</h3>
                        <ul class="links">
                            <li>
                                <a href="/investor-solution-nri">NRI</a>
                            </li>
                            <li>
                                <a href="/investor-solution-corporate">Corporate</a>
                            </li>
                            <li>
                                <a href="/investor-solution-advisor">Advisor</a>
                            </li>
                            <li>
                                <a href="https://mutualfund.adityabirlacapital.com/investor-solution-portfolio-management-service">PMS</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h3>Quick Links</h3>
                        <ul class="links">
                            <li>
                                <a href="/your-fund-performance">Check Fund Performance</a>
                            </li>
                            <li>
                                <a href="http://mf.adityabirlacapital.com/Pages/Individual/Customer-Service/EKycLandingPage.aspx" target="_blank">Complete eKYC</a>
                            </li>
                            <li>
                                <a href="/resources">Download Application Forms</a>
                            </li>
                            <li>
                                <a href="/location">Find an Advisor</a>
                            </li>
                            <li>
                                <a href="http://janotohmano.birlasunlife.com/" target="_blank">Investor Education</a>
                            </li>
                            <li>
                                <a href="https://mutualfund.adityabirlacapital.com/blog" target="_blank">Market Insights</a>
                            </li>
                            <li>
                                <a href="https://sipnow.birlasunlife.com/" target="_blank">Start a SIP</a>
                            </li>
                            <li>
                                <a href="/-/media/bsl/files/pms-forms/disclosure-document-june-2018.pdf" target="_blank">PMS</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <h3>Help</h3>
                        <ul class="links">
                            <li>
                                <a href="/help-centre/faq">FAQs</a>
                            </li>
                            <li>
                                <a href="mailto:care.mutualfunds@adityabirlacapital.com?subject=Post%20your%20Query">Post a Query</a>
                            </li>
                            <li>
                                <a href="/help-centre/contact-us">Contact Us</a>
                            </li>
                            <li>
                                <a href="/glossary">Glossary</a>
                            </li>
                            <li>
                                <a href="/location">Locate Us</a>
                            </li>
                            <li>
                                <a href="/help-centre">Customer Service</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="col-md-2 col-sm-12 col-xs-12">
                <div class="col-wrapper">

                    <div class="col-subscribe">
                        <h3>Connect with us</h3>
                        <ul class="socialLinks">
                            <li>
                                <a href="https://www.facebook.com/BSLMF" target="_blank" title="Follow Us on Facebook"><img src="<?php echo $fbiconurl; ?>" alt=""></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/BSLMF" target="_blank" title="Tweet us on Twitter"><img src="<?php echo $twittericonurl; ?>" alt=""></a>        </li>

                            <li>
                                <a href="https://www.linkedin.com/company/bslmf" target="_blank" title="Connect with us on LinkedIn"><img src="<?php echo $linkediniconurl; ?>" alt=""></a>        </li>
                        </ul>
                    </div>


                    <div class="col-mobApp">
                        <h3>Looking for our mobile app</h3>
                        <div class="dvAppIcons">
                            <a href="https://itunes.apple.com/in/developer/birla-sun-life-mutual-fund/id1100179210" class="ap" target="_blank"></a>        <a href="https://play.google.com/store/apps/developer?id=Aditya+Birla+Sun+Life+AMC+Ltd." class="ag" target="_blank"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row dvPrivacyBar">
        <div class="columWrapper clearfix">
            <div class="col-sm-12">
                <div class="dvCopyright">
                    Aditya Birla Sun Life AMC Limited (formerly known as Birla Sun Life Asset Management Company Limited), the investment manager of Aditya Birla Sun Life Mutual Fund is a joint venture between the Aditya Birla Group and Sun Life Financial Inc. of Canada.
                </div>
            </div>
        </div>
    </div>
    <div class="row dvPrivacyBar">
        <div class="columWrapper clearfix">
            <div class="pull-left">
                <div class="dvCopyright">Copyright Aditya Birla Sunlife Mutual Funds 2017. All Rights Reserved.</div>
            </div>
            <div class="pull-right">
                <div class="col-subscribe">
                    <span class="s_connect">Connect with us</span>
                    <ul class="socialLinks">
                        <li><a title="Follow Us on Facebook" href="https://www.facebook.com/BSLMF" target="_blank"><img src="<?php echo $fbiconurl; ?>"></a></li>
                        <li><a title="Tweet us on Twitter" href="https://twitter.com/BSLMF" target="_blank"><img src="<?php echo $twittericonurl; ?>"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End .page-footer            -->

<div class="row brandfooter">
        <div class="columWrapper clearfix footer_container">
            <div class="upper-div">
                <div class="logo">

                                <a href="https://adityabirlacapital.com/en?_ga=2.41328994.1928849228.1517467881-46591046.1517467881" target="_blank">
                                <img src="<?php echo $CFG->wwwroot . '/pix/footer_logo.png'; ?>" alt="footer logo"></a></div>
                <div class="hidden-sm hidden-xs">
                        <div class="lcol">
                            <ul class="no-mar no-pad">
                                    <li><a href="https://adityabirlacapital.com/en/About-Us" target="_blank">About Us</a></li>
                                    <li><a href="https://adityabirlacapital.com/en/Investor-Relations" target="_blank">Investor Relations</a></li>
                                    <li><a href="https://adityabirlacapital.com/en/Press-and-Media" target="_blank">Press &amp; Media</a></li>
                            </ul>
                        </div>
                        <div class="lcol">
                            <ul class="no-mar no-pad">
                                    <li><a href="https://adityabirlacapital.com/en/About-Us/Careers" target="_blank">Careers</a></li>
                                    <li><a href="https://adityabirlacapital.com/en/Customer-Services" target="_blank">Customer Service</a></li>
                                    <li><a href="https://adityabirlacapital.com/en/Corporate-Social-Responsibility" target="_blank">CSR</a></li>
                            </ul>
                        </div>
                        <div class="lcol">
                            <ul class="no-mar no-pad">
                                    <li><a href="https://adityabirlacapital.com/en/Privacy-Policy" target="_blank">Privacy Policy</a> </li>
                                    <li><a href="https://adityabirlacapital.com/en/Terms-and-Conditions" target="_blank">Terms &amp; Conditions</a> </li>
                            </ul>
                        </div>
                </div>
                <div class="hidden-md hidden-xs hidden-lg smcopyright">
                    <p class="caption no-mar">© 2017, Aditya Birla Capital Inc. All Rights Reserved.</p>
                    <div class="contacttext">
                        <p class="caption">Call us directly:    </p><span class="abcicons icon-icon-phone iconspan"></span>
                        <p class="caption">1800 270 7000</p>
                    </div>
                </div>
            </div>
            <div class="lower-div">
                <div class="copyright hidden-sm">
                    <p class="caption no-mar">© 2017, Aditya Birla Capital Inc. All Rights Reserved.</p>
                </div>
                <div class="socialicons hidden-sm">
                    <div class="contacttext">
                        <p class="caption">Call us directly:  </p><span class="abcicons icon-icon-phone iconspan"></span>
                        <p class="caption">1800 270 7000</p>
                    </div>
                    <ul class="no-mar hidden-xs">
                            <li><a href="https://www.facebook.com/AdityaBirlaCapital/?ref=bookmarks" target="_blank"><img src="<?php echo $CFG->wwwroot . '/pix/fb-OPT.png'; ?>" alt=""></a>
                            </li>
                            <li><a href="https://twitter.com/abcapital" target="_blank"><img src="<?php echo $CFG->wwwroot . '/pix/twitter-OPT.png'; ?>" alt=""></a>
                            </li>
                            <li> <a href="https://scstg.adityabirlacapital.com/en" target="_blank"><img src="<?php echo $CFG->wwwroot . '/pix/you_tube-OPT.png'; ?>" alt=""></a>
                            </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
