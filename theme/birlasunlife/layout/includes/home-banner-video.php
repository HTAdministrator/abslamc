<?php
$bannerImage='banner_image.png';


$usevideosection = (!empty($PAGE->theme->settings->usevideosection));
$homeBannerVideo = $PAGE->theme->settings->homeBannerVideo;
if($usevideosection){
?>



<section class="section_wrapper pb_none">
			<div class="banner_wrapper">
				<div class="container">
					<div class="row">	
						<div class="banner_wrap slideInLeft animated">
							<div class="banner_up_text">
								<?php echo get_string('banner_up_text','theme_birlasunlife');?>
							</div>
							<div class="banner_big_text">
								<span class="mf"><?php echo get_string('banner_big_text','theme_birlasunlife');?></span> <?php echo get_string('banner_big_text1','theme_birlasunlife');?>
							</div>
							<div class="banner_sub_text">
								<?php echo get_string('banner_sub_text','theme_birlasunlife');?>
							</div>
                                                    <a href="<?php echo $CFG->wwwroot ?>/my/"><input type="button" value="<?php echo get_string('banner_btn','theme_birlasunlife');?>" class="banner_btn"/></a>
						</div>
                                            <img class="banner_img slideInRight animated animation-delay-100" src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/<?php echo $bannerImage;?>"></img>
						
					</div><!--row-->
				</div><!--container-->
			</div><!--banner_wrapper-->
		</section><!--section_wrapper-->

<?php } ?>

