<?php
$hascopyright = (empty($PAGE->theme->settings->copyright)) ? false : $PAGE->theme->settings->copyright;
$hasfooterwidget = (empty($PAGE->theme->settings->footerwidget)) ? false : $PAGE->theme->settings->footerwidget;

/* Footer blocks settings */
$hasfooterblocks = (empty($PAGE->theme->settings->enablefooterblocks)) ? false : $PAGE->theme->settings->enablefooterblocks;
$hasfooterblock1 = (empty($PAGE->theme->settings->footerblock1)) ? false : $PAGE->theme->settings->footerblock1;
$hasfooterblock2 = (empty($PAGE->theme->settings->footerblock2)) ? false : $PAGE->theme->settings->footerblock2;
$hasfooterblock3 = (empty($PAGE->theme->settings->footerblock3)) ? false : $PAGE->theme->settings->footerblock3;

$footerblock1 = $PAGE->theme->settings->footerblock1;
$footerblock2 = $PAGE->theme->settings->footerblock2;
$footerblock3 = $PAGE->theme->settings->footerblock3;

$footerwidget = $PAGE->theme->settings->footerwidget;

$twittericonurl = $OUTPUT->pix_url('birla_images/twitter', 'theme_birlasunlife');
$fbiconurl = $OUTPUT->pix_url('birla_images/fb', 'theme_birlasunlife');
?>
<!-- Start .page-footer-->
<div>
    <div class="row">
    </div>

    <div class="row dvPrivacyBar">
        <div class="columWrapper clearfix">
            <div class="col-sm-12">
                <div class="dvCopyright">
                    Aditya Birla Sun Life AMC Limited (formerly known as Birla Sun Life Asset Management Company Limited), the investment manager of Aditya Birla Sun Life Mutual Fund is a joint venture between the Aditya Birla Group and Sun Life Financial Inc. of Canada.
                </div>
            </div>
        </div>
    </div>
    <div class="row dvPrivacyBar">
        <div class="columWrapper clearfix">
            <div class="col-sm-6 col-md-6">
                <div class="dvCopyright">Copyright Aditya Birla Sunlife Mutual Funds 2017. All Rights Reserved.</div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="col-subscribe">
                    <span class="s_connect">Connect with us</span>
                    <ul class="socialLinks">
                        <li><a title="Follow Us on Facebook" href="https://www.facebook.com/BSLMF" target="_blank"><img src="<?php echo $fbiconurl; ?>"></a></li>
                        <li><a title="Tweet us on Twitter" href="https://twitter.com/BSLMF" target="_blank"><img src="<?php echo $twittericonurl; ?>"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End .page-footer            -->
<!--
<div class="row brandfooter">
        <div class="columWrapper clearfix footer_container">
            <div class="upper-div">
                <div class="logo">
                
                                <a href="https://adityabirlacapital.com/en?_ga=2.41328994.1928849228.1517467881-46591046.1517467881" target="_blank">
                                <img src="<?php echo $CFG->wwwroot . '/pix/footer_logo.png'; ?>" alt="footer logo"></a></div>
                <div class="hidden-sm hidden-xs">
                        <div class="lcol">
                            <ul class="no-mar no-pad">
                                    <li><a href="https://adityabirlacapital.com/en/About-Us" target="_blank">About Us</a></li>
                                    <li><a href="https://adityabirlacapital.com/en/Investor-Relations" target="_blank">Investor Relations</a></li>
                                    <li><a href="https://adityabirlacapital.com/en/Press-and-Media" target="_blank">Press &amp; Media</a></li>
                            </ul>
                        </div>
                        <div class="lcol">
                            <ul class="no-mar no-pad">
                                    <li><a href="https://adityabirlacapital.com/en/About-Us/Careers" target="_blank">Careers</a></li>
                                    <li><a href="https://adityabirlacapital.com/en/Customer-Services" target="_blank">Customer Service</a></li>
                                    <li><a href="https://adityabirlacapital.com/en/Corporate-Social-Responsibility" target="_blank">CSR</a></li>
                            </ul>
                        </div>
                        <div class="lcol">
                            <ul class="no-mar no-pad">
                                    <li><a href="https://adityabirlacapital.com/en/Privacy-Policy" target="_blank">Privacy Policy</a> </li>
                                    <li><a href="https://adityabirlacapital.com/en/Terms-and-Conditions" target="_blank">Terms &amp; Conditions</a> </li>
                            </ul>
                        </div>
                </div>
                <div class="hidden-md hidden-xs hidden-lg smcopyright">
                    <p class="caption no-mar">© 2017, Aditya Birla Capital Inc. All Rights Reserved.</p>
                    <div class="contacttext">
                        <p class="caption">Call us directly:    </p><span class="abcicons icon-icon-phone iconspan"></span>
                        <p class="caption">1800 270 7000</p>
                    </div>
                </div>
            </div>
            <div class="lower-div">
                <div class="copyright hidden-sm">
                    <p class="caption no-mar">© 2017, Aditya Birla Capital Inc. All Rights Reserved.</p>
                </div>
                <div class="socialicons hidden-sm">
                    <div class="contacttext">
                        <p class="caption">Call us directly:  </p><span class="abcicons icon-icon-phone iconspan"></span>
                        <p class="caption">1800 270 7000</p>
                    </div>
                    <ul class="no-mar hidden-xs">
                            <li><a href="https://www.facebook.com/AdityaBirlaCapital/?ref=bookmarks" target="_blank"><img src="<?php echo $CFG->wwwroot . '/pix/fb-OPT.png'; ?>" alt=""></a>                                
                            </li>
                            <li><a href="https://twitter.com/abcapital" target="_blank"><img src="<?php echo $CFG->wwwroot . '/pix/twitter-OPT.png'; ?>" alt=""></a>                            
                            </li>
                            <li> <a href="https://scstg.adityabirlacapital.com/en" target="_blank"><img src="<?php echo $CFG->wwwroot . '/pix/you_tube-OPT.png'; ?>" alt=""></a>                           
                            </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

     </div><!--//bottom-bar
-->