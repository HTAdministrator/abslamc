<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* LOGO Image */
$haslogo = (!empty($PAGE->theme->settings->logo));

/* Logo settings */
if ($haslogo) {
$logourl = $PAGE->theme->setting_file_url('logo', 'logo');
} else {
$logourl = $OUTPUT->pix_url('logo', 'theme');
}


/* Header widget settings */
$hasheaderwidget = (!empty($PAGE->theme->settings->headerwidget));
$headerwidget = $PAGE->theme->settings->headerwidget;
if(!isloggedin()){
  $frontpageheadspan='frontpageheadspan';  
}else{
    $frontpageheadspan='';
}
?>
<link href="<?php echo $CFG->httpswwwroot ?>/theme/birlasunlife/style/animate.css" rel="stylesheet" />
<link href="<?php echo $CFG->httpswwwroot ?>/theme/birlasunlife/style/style_nw.css" rel="stylesheet" />

<script src="<?php echo $CFG->httpswwwroot ?>/theme/birlasunlife/jquery/jquery.viewportchecker.js"></script>
<script>
$(document).ready(function() {
   /* 
    $('.banner-section .container > .row').addClass("hidden-1").viewportChecker({
        classToAdd: 'visible animated slideInUp',
        offset: 100
       });

    $('.container > .row.slick-animations-slider').addClass("hidden-1").viewportChecker({
        classToAdd: ' visible animated slideInLeft',
        offset: 100
       });	   

    $('.course-section .container > .row').addClass("hidden-1").viewportChecker({
        classToAdd: ' visible animated slideInUp',
        offset: 100
       });	   

    $('.container > .row.slick-videos-slider').addClass("hidden-1").viewportChecker({
        classToAdd: ' visible animated slideInRight',
        offset: 100
       });	   

    $('#page-site-index #page-footer .row > .footer_container').addClass("hidden-1").viewportChecker({
        classToAdd: 'visible animated slideInUp',
        offset: 100
       });    
     $('#page-site-index #page-footer .lower-div').addClass("hidden-1 visible animated slideInUp").viewportChecker({
        classToAdd: 'visible animated slideInUp',
        offset: 100
       });*/

	 
	$(".header .main-nav .nav li a").css("transition", "0s"); 
	var currentLang = "<?php echo $_SESSION[SESSION]->lang;?>";
	//alert(currentLang);
	if((getUrlParameter("lang") && getUrlParameter("lang") == "ta") || (currentLang && currentLang == "ta")){
		$(".header .main-nav .nav li a").css("padding", "15px 15px");
	} 	
	
	//$(".header .main-nav .nav li a").css("transition", " all .4s ease-in-out"); 	
	  
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
</script>

<header id="page-header" class="header <?php //echo $frontpageheadspan; ?> <?php if (isloggedin()) { echo "loggedInUser";} ?>">
	<!--<div class="row">
			<div class="columWrapper clearfix">
				<div class="topbar_active topbar_transparent">
					  <div class="topcontent clearfix">
						<div class="top-link box_socials pull-right"> 
						<a href="https://adityabirlacapital.com/en/Corporate?_ga=2.75345106.1928849228.1517467881-46591046.1517467881" target="_blank"> Corporates</a> 
						<a href="https://adityabirlacapital.com/en/Advisors?_ga=2.75345106.1928849228.1517467881-46591046.1517467881" target="_blank"> Advisors</a> 
						<a href="https://adityabirlacapital.com/en/Customer-Services?_ga=2.37200480.1928849228.1517467881-46591046.1517467881" target="_blank"> Customer Service</a>
						<a href="https://adityabirlacapital.com/en/About-Us/Careers?_ga=2.37200480.1928849228.1517467881-46591046.1517467881" target="_blank">Careers</a>
						<a href="https://adityabirlacapital.com/en/About-Us?_ga=2.238324928.1928849228.1517467881-46591046.1517467881" target="_blank"> About Us</a> 
						 </div>
					  </div>
				</div>
			</div>
	</div>-->
    <div class="row corporate">  
		<div class="columWrapper clearfix">
		
		<div class="main_bar_active">
			<div class="navbar navbar-default">
			<div class="navbar-header">
				<a href="https://mutualfund.adityabirlacapital.com/Investor-Education"><img id="logo" src="<?php echo $logourl; ?>" alt="<?php echo $SITE->shortname; ?>" /></a>
			</div>
				<!--<div class="minimainlink_menu">
					<ul class="nav navbar-nav navbar-left">
					  <li><a href="https://adityabirlacapital.com/en/Protecting?_ga=2.218366041.1963910145.1517812733-1380603538.1517222623" target="_blank">PROTECTING </a> </li>
					  <li><a href="https://adityabirlacapital.com/en/Investing?_ga=2.59295725.1963910145.1517812733-1380603538.1517222623" target="_blank">INVESTING</a></li>
					  <li><a href="https://adityabirlacapital.com/en/Financing?_ga=2.59295725.1963910145.1517812733-1380603538.1517222623" target="_blank">FINANCING</a></li>
					  <li><a href="https://adityabirlacapital.com/en/Advising?_ga=2.59295725.1963910145.1517812733-1380603538.1517222623" target="_blank">ADVISING</a></li>
					</ul>
                                </div>-->
			  
			<div class="customerservice_menu">		
				<ul class="nav navbar-nav navbar-right">
						   <?php if (!isloggedin()) { ?>
						   <li class="lg-img">
							<a class="btn btn-cta btn-cta-secondary loginButton " href="<?php echo $CFG->httpswwwroot ?>/login/" ><?php echo get_string('login') ?></a>
							</li>
							<?php } ?>                    
							
							<?php if (isloggedin()) { ?>
								<?php echo $OUTPUT->user_menu(); ?> 
							<?php } ?>	
				</ul>	

				<nav role="navigation" class="main-nav float-right-div">
					<div class="navbar-header">
						<button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target="#nav-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button><!--//nav-toggle-->
					</div><!--//navbar-header-->
				</nav>	
			
			</div>
		</div> <!-- navbar navbar-default -->
			</div> 
		</div>
		</div>
		
				
		<div class="row headerDesktop">
        <div class="primaryNavWrapper">
            <div class="columWrapper clearfix">
				<div class="primaryNav">
					<div class="brandTitle">
							<!--<a href="<?php echo $CFG->httpswwwroot ?>">Mutual funds</a>-->
                                                        <a href="https://mutualfund.adityabirlacapital.com/Investor-Education"><?php echo get_string('themelearingmodules', 'theme_birlasunlife');?></a>
					</div>
				</div>
				<div class="navbar megamenu" role="navigation">
							<div class="smallmenu">
								<div class="navbar-offcanvas navbar-offcanvas-touch  navbar-offcanvas-right navbar-inverse" id="sidenavbar">
									<div class="navbar-header1">
									<button type="button" class="navbar-toggle offcanvas-toggle pull-right" data-toggle="offcanvas" data-target="#sidenavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
									</div>
									<div id="nav-collapse" class="nav-collapse collapse">
										<?php  echo $OUTPUT->custom_menu(); ?>              
									</div>
								</div>
							</div>
				</div>
                
                                <div class="rightCol">
                                        <div class="primaryNav">
                                          <ul>
                                            <li><a href="#" class="ssomfpostClick pms_log"><span class="pms_log_text"><u>Go to</u> Mutual Fund Website</span></a>

                                            </li>
                                          </ul>
                                        </div>
                              </div>
            </div>
        </div>
    </div>
		
		
		<div class="row adityaFix">
		<div class="columWrapper clearfix"> 
		<div class="bottombar_dark">
			  <div class="abname">
				<p>Aditya Birla Sun Life Mutual Fund</p>
			  </div>
		</div>
		</div>
		</div>
		<!-- container - close -->
    </div><!--//topbar-->    
<div class="row"></div>
    <!--<div class=" branding container-fluid">     
        <div class="row" >
            <h1 class="logo col-md-5 col-sm-12 col-xs-12">
                <a href="<?php echo $CFG->httpswwwroot ?>"><img id="logo" src="<?php echo $logourl ?>" alt="<?php echo $SITE->shortname; ?>" /></a>
            </h1><!--//logo--
            
            <div class="info-container col-md-7 col-sm-12 col-xs-12">
                
                <div class="logininfo-container">
                    <?php //echo $PAGE->headingmenu ?>
                    
                    <?php if (!isloggedin()) { ?>
                    <a class="btn btn-cta btn-cta-secondary" href="<?php echo $CFG->httpswwwroot ?>/login/" ><?php echo get_string('login') ?></a>
                    <?php } ?>                    
                    
                    <?php if (isloggedin()) { ?>
                        <?php echo $OUTPUT->user_menu(); ?> 
                    <?php } ?>
                    
                </div><!--//logininfo-container--
                
                <?php /* Moodle 3.1 global search feature - solr needs to be configured */
                if (method_exists($OUTPUT, 'search_box')) {
                  echo $OUTPUT->search_box(); // global search, for Moodle 3.1+
                }
                ?>
            
                <?php if($hasheaderwidget) {?>
                <div class="header-widget hidden-xs hidden-sm">
                    <?php echo $headerwidget ?>
                </div><!--//header-widget--
                <?php }?>
            </div>
        </div><!--//row--           
    </div><!--//branding-->
</header>