<?php
$catid = optional_param('catid', 1, PARAM_INT);
global $DB,$CFG,$USER;
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
$courserenderer = $PAGE->get_renderer('core', 'course');
if (!is_siteadmin()) {
    require_once($CFG->dirroot.'/my/enrolcourseslib.php');
    if (isset($USER->id) && !empty($USER->id)) {
        local_enrol_user($USER->id);
    }
}
?>
 <!--Videos-->
	<section class="section_wrapper sectionline">
			<div class="container">
				<div class="row">	
					<div class="sec_header_wrap">
						<div class="sec_head_bod"></div>
						<div class="sec_head" id="sec_head1">
							<img src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/introducing_mutual_fund_icon.png" class="sec_head_icon"/>
							<?php echo get_string('video','theme_birlasunlife');?>
                                                        
						</div>
					</div><!--sec_header_wrap-->
                                        <div class="img_slider_wrap" id="img_slider_wrap1">
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
                                            	<!-- Indicators -->
                                                <ol class="carousel-indicators">
                                                 <?php
                                                $videos = get_coursemodules_in_course('url', 21);
                                                $cnt_videos = count($videos);

                                                for ($i = 0; $i < $cnt_videos; $i++) {
                                                    if ($i == 0) {
                                                        echo '<li data-target="#myCarousel" data-slide-to="' . $i . '" class="active">' . ($i + 1) . '</li>';
                                                    } else {
                                                        echo '<li data-target="#myCarousel" data-slide-to="' . $i . '">' . ($i + 1) . '</li>';
                                                    }
                                                }
                                                ?>
						</ol>
                                               
                                                <div class="carousel-inner">
                                                    <!--aDD FOR LOOP-->
                                                     <?php
                                                    
                                                    $i = 1;
                                                    foreach ($videos as $k => $v) {
                                                    $desc = $DB->get_record('url', array('id' => $v->instance));
                                                    //externalurl
                                                    if($i==1){
                                                        $add_active = 'active';
                                                        $add_id_l="cs6_l";
                                                        $add_id_r="cs6_r";
                                                    }
                                                    else{
                                                        $add_active = '';
                                                         $add_id_l='';
                                                        $add_id_r='';
                                                    }
                                                    $i++;
                                                    ?>
                                                    <div class="item <?php echo $add_active;?>">
                                                       	<div class="col-sm-6" id="<?php echo $add_id_l;?>">
									<div class="img_wrapper">
										
                                                                           <!-- <a href="<?php// echo $CFG->wwwroot; ?>/mod/<?php //echo $v->modname; ?>/view.php?id=<?php //echo $v->id; ?>">-->
										<video controls="true" width="100%" height="253" preload="metadata" title="<?php echo $desc->headertext?>">
                                                                                    <source src="<?php echo $desc->externalurl?>" type="video/mp4">
											
											
										</video>
									</div>
							</div> 
                                                   
                                                    <div class="col-sm-6" id="<?php echo $add_id_r;?>">
									<div class="slider_right_text">
										<?php echo $desc->headertext?>
									</div>
									<div class="slider_right_sub_text">
										<?php echo $desc->bodytext?>
									</div>
								</div>
                                                        </div>
                                                    <?php }?>
                                                 <!--END FOR LOOP-->
                                                </div><!--carousel-inner-->
                                                	<!-- Left and right controls -->
							<a class="left carousel-control" href="#myCarousel" data-slide="prev">
							  <img src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/birla_images/slide_left_aro.png"/>
							</a>
							<a class="right carousel-control" href="#myCarousel" data-slide="next">
                                                            <img src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/birla_images/slide_right_aro.png"/>
							</a>
                                         </div>
					</div><!--img_slider_wrap-->   
                                </div><!--row-->
			</div><!--container-->
        </section><!--section_wrapper-->
        <!--Animation-->  
        <section class="section_wrapper sectionline back_one">
			<div class="container">
				<div class="row">	
					<div class="sec_header_wrap shw1">
						<div class="sec_head_bod"></div>
						<div class="sec_head sh1" id="sec_head2">
							<img src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/about_mutual_fund_icon.png" class="sec_head_icon"/>
							<?php echo get_string('animation','theme_birlasunlife');?>
						</div>
					</div><!--sec_header_wrap-->
                                        <div class="img_slider_wrap" id="img_slider_wrap2">
                                            <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                                                <?php
                                               
                                                 ?>
                                                <!-- Indicators -->
                                                <ol class="carousel-indicators">
                                                 <?php
                                                 $animations = get_coursemodules_in_course('url', 22);
                                                $cnt_animate = count($animations);

                                                for ($i = 0; $i < $cnt_animate; $i++) {
                                                    if ($i == 0) {
                                                        echo '<li data-target="#myCarousel1" data-slide-to="' . $i . '" class="active">' . ($i + 1) . '</li>';
                                                    } else {
                                                        echo '<li data-target="#myCarousel1" data-slide-to="' . $i . '">' . ($i + 1) . '</li>';
                                                    }
                                                }
                                                ?>
						</ol>
                                                 <div class="carousel-inner">
                                                    <!--aDD FOR LOOP-->
                                                     <?php
                                                    
                                                    $i = 1;
                                                    foreach ($animations as $k => $v) {
                                                    $desc_animation = $DB->get_record('url', array('id' => $v->instance));
                                                    //externalurl
                                                    if($i==1){
                                                        $add_active = 'active';
                                                        $add_id_l='cs6_1_l';
                                                        $add_id_r='cs6_1_r';
                                                    }
                                                    else{
                                                        $add_active = '';
                                                         $add_id_l='';
                                                        $add_id_r='';
                                                    }
                                                    $i++;
                                                    ?>
                                                    <div class="item <?php echo $add_active;?>">
                                                       	<div class="col-sm-6" id="<?php echo $add_id_l;?>">
									<div class="img_wrapper">
                                                                            <!--<a href="<?php //echo $CFG->wwwroot; ?>/mod/<?php //echo $v->modname; ?>/view.php?id=<?php //echo $v->id; ?>">-->
										<!--<img src="./images/poster_1.png" class="img-responsive">-->
										<video controls="true" width="100%" height="253" preload="metadata" title="Tuition">
                                                                                    <source src="<?php echo $desc_animation->externalurl?>" type="video/mp4">
											
											
										</video>
									</div>
							</div> 
                                                   
                                                    <div class="col-sm-6" id="<?php echo $add_id_r;?>">
									<div class="slider_right_text">
										<?php echo $desc_animation->headertext?>
									</div>
									<div class="slider_right_sub_text">
										<?php echo $desc_animation->bodytext?>
									</div>
								</div>
                                                        </div>
                                                    <?php }?>
                                                 <!--END FOR LOOP-->
                                                </div><!--carousel-inner-->
                                                <!-- Left and right controls -->
                                                <a class="left carousel-control" href="#myCarousel1" data-slide="prev">
                                                  <img src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/birla_images/slide_left_aro.png"/>
                                                </a>
                                                <a class="right carousel-control" href="#myCarousel1" data-slide="next">
                                                    <img src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/birla_images/slide_right_aro.png"/>
                                                </a>
                                            </div>
					</div><!--img_slider_wrap-->
                                 </div><!--row-->
			</div><!--container-->
		</section><!--section_wrapper-->
		
 <!--Modules-->
  
<section class="section_wrapper sectionline">
			<div class="container">
				<div class="row">	
					<div class="sec_header_wrap shw2">
						<div class="sec_head_bod"></div>
						<div class="sec_head sh2" id="sec_head3">
							<img src="<?php echo $CFG->wwwroot; ?>/theme/birlasunlife/pix/elearning_icon.png" class="sec_head_icon"/>
							<?php echo get_string('themelearingmodules', 'theme_birlasunlife'); ?>
						</div>
					</div><!--sec_header_wrap-->
					<div class="elearning_wrap">
 <?php
 $catid = optional_param('catid', '', PARAM_INT);
$_SESSION[SESSION]->lang;
$categories = $OUTPUT->get_list_categories();
if ($catid != "") {
    $catid = $catid;
} else {
    if ($_SESSION[SESSION]->lang != "") {
        $catid = $OUTPUT->get_course_id_session_lang($_SESSION[SESSION]->lang);
    } else {
        if ($_SESSION[SESSION]->lang == "") {
            $lang1 = optional_param('lang', '', PARAM_RAW);
            $catid = $OUTPUT->get_course_id_session_lang($lang1);
        } else {
            $catid = 1;
        }
    }
}
$courses = $OUTPUT->get_homePage_courses($catid, 6);
//echo"<pre>";
//print_r($courses);
if (count($courses)) {
    $counse_count = 0;
    foreach ($courses as $k => $v) {

        if ($counse_count == 3) {
            continue;
        }
        $result2 = $DB->get_field("course_modules", "instance", array("course" => $v['id']));
        $course = $DB->get_record('course', array('id' => $v['id']), '*', MUST_EXIST);
        $scorm = $DB->get_record('scorm', array('id' => $result2), '*', MUST_EXIST);
        if (!$cm = get_coursemodule_from_instance("scorm", $scorm->id, $course->id, true)) {
            print_error('invalidcoursemodule');
        }

        if (empty($organization)) {
            $organization = $scorm->launch;
        }
        $orgidentifier = '';
        $sco = scorm_get_sco($organization, SCO_ONLY);
        if ($sco->organization == '' && $sco->launch == '') {
            $orgidentifier = $sco->identifier;
        } else {
            $orgidentifier = $sco->organization;
        }
        $buttonname = get_string('themelaunch', 'theme_birlasunlife').' >';
        $string_form = "<form id='scormviewform_" . $cm->id . "' method='post' action='" . $CFG->wwwroot . "/mod/scorm/player.php'>
                         <input type='hidden' name='mode' value='normal'>
                         <input type='hidden' name='scoid' value='" . $scorm->launch . "'>
                         <input type='hidden' name='cm' value='" . $cm->id . "'>
                         <input type='hidden' name='currentorg' value='" . $orgidentifier . "'>
                    
                         <input type='submit' value='" . $buttonname . "'  class='module_btn'></form>";
    $m_name = get_string('module', 'theme_birlasunlife'). ' '.($counse_count+1);
    ?>

            <div class="col-md-4" id="module_<?php echo ($counse_count+1);?>">
                    <div class="elearning_head">
                            <?php echo $m_name;?>
                    </div>
                    <div class="clear"></div>
                    <div class="elearning_sub_head">
                             <?php 
                             $arr_cname = explode(":", $v['coursename']);
                            echo $course_name = mb_substr($arr_cname[1], 0, 40, 'UTF-8') . '...';
                           ?>
                    </div>
                    <div class="module_wrapper">
                            <img src="<?php echo $v['courseimage']; ?>"/>
                            <div class="module_body">
                                    <div class="module_text">
                                     <?php
                                    $summary = $v['summary'];

                                    if (strlen($summary) > 100) {
                                        $summarystr = mb_substr($summary, 0, 97,'UTF-8') . '...';
                                    } else {
                                        $summarystr = $summary;
                                    }

                                    echo $summarystr;
                                    ?>
                                    </div>
                                    <div class="module_btn_wrap">
                                            <a href="#">
                                                    <div>
                                                            <?php echo $string_form;?>
                                                    </div>
                                            </a>
                                    </div>
                            </div>
                    </div>
            </div>
                    <?php
                    $counse_count++;
                }
                 }else {
                echo "<div class='col-sm-12 no-course-available'>" . get_string('nomodules', 'theme_birlasunlife') . "</div>";
            }
                ?>			
						<div class="clear"></div>
						<div class="more_wrap">
							   <a class="know-more" href="<?php echo $CFG->wwwroot; ?>/my/index.php"><div class="more"><?php echo get_string('moremodules','theme_birlasunlife');?>  >></div></a>
						</div>
					</div><!--elearning_wrap-->
				</div><!--row-->
			</div><!--container-->
		</section><!--section_wrapper-->





<?php $hasfrontpagenews = (!empty($PAGE->theme->settings->frontpagenews)); ?>
<?php if ($hasfrontpagenews) { ?>
    <section class="news-section">
        <h1 class="h1-heading"><span>News</span></h1>
        <div class="container class">
            <div class="row">
                <div class="col-sm-6">
                    <div class="news-vertical-arrows"></div>
                    <div id="news-vertical" class="carousel vertical slide">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>
                                </div>
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>					  
                            </div>

                            <div class="item">
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>
                            </div>

                        </div>

                        <!-- Controls -->
                        <a class="up carousel-control" href="#news-vertical" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="down carousel-control" href="#news-vertical" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>


                <div class="col-sm-6">
                    <div class="video-container video-container-viemo">
                        <iframe src="//www.youtube.com/embed/KnZfe1qpaKM" frameborder="0" allowfullscreen="" ></iframe>

                    </div><!--//video-container-->
                    <div class="video-details-container">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <div>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </div>	
                    </div>
                </div>

            </div>
        </div>
    </section>
<?php } ?>
<script>
$(document).ready(function(){
    
			$(".ip_btn").click(function(){
				$(".f_error").show();
			});
			$(".error_close").click(function(){
				$(".f_error").hide();
			});
			
			$(".investDmm").click(function(e){
				e.preventDefault();
				$(this).parent(".investWrapper").find("ul").toggle();
			});
			$(".brandMenu").click(function(e){
				e.preventDefault();
				$(".topNavContent").toggle();
			});
			
			$(".arrow").click(function(e){
				e.preventDefault();
				$(this).parents("li").find(".subMenuWrapper").toggle();
			});
			
			$(".navbar-toggle").click(function(){
				$("#mainsidebar").css("right","0px");
			});
			$(".menu-close").click(function(){
				$("#mainsidebar").css("right","-500px");
			});
			
			$(".pms_log").hover(function(){
				$(this).find(".pms_log_img").attr("src", "./images/login_icon2.png");
				}, function(){
				$(this).find(".pms_log_img").attr("src", "./images/login_icon.png");
			});
			$(".pms_log_sub").hover(function(){
				$(".pms_log").find(".pms_log_img").attr("src", "./images/login_icon2.png");
				}, function(){
				$(".pms_log").find(".pms_log_img").attr("src", "./images/login_icon.png");
			});
			
			$(".module_wrapper").hover(function(){
				$(this).find(".module_body").css("top", "0px");
				}, function(){
				$(this).find(".module_body").css("top", "345px");
			});
			
			
			$(window).scroll(function(){
				// This is then function used to detect if the element is scrolled into view
				function elementScrolled(elem)
				{
					var docViewTop = $(window).scrollTop();
					var docViewBottom = docViewTop + $(window).height();
					var elemTop = $(elem).offset().top;
					return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
				}
				 
				// This is where we use the function to detect if ".box2" is scrolled into view, and when it is add the class ".animated" to the <p> child element
				if(elementScrolled('#sec_head1')) {
					var els = $('#sec_head1'),
						i = 0,
						f = function () {
							$(els).addClass('fadeInDown');
							$(els).addClass('animated');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				if(elementScrolled('#sec_head2')) {
					var els = $('#sec_head2'),
						i = 0,
						f = function () {
							$(els).addClass('fadeInDown');
							$(els).addClass('animated');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				if(elementScrolled('#sec_head3')) {
					var els = $('#sec_head3'),
						i = 0,
						f = function () {
							$(els).addClass('fadeInDown');
							$(els).addClass('animated');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				
				if(elementScrolled('#img_slider_wrap1')) {
					var els = $('#img_slider_wrap1'),
						i = 0,
						f = function () {
							$(els).addClass('fadeInLeft');
							$(els).addClass('animated');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				
				if(elementScrolled('#img_slider_wrap2')) {
					var els = $('#img_slider_wrap2'),
						i = 0,
						f = function () {
							$(els).addClass('fadeInLeft');
							$(els).addClass('animated');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				
				if(elementScrolled('#module_1')) {
					var els = $('#module_1'),
						i = 0,
						f = function () {
							$(els).addClass('slideInUp');
							$(els).addClass('animated');
							$(els).addClass('animation-delay-100');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				if(elementScrolled('#module_2')) {
					var els = $('#module_2'),
						i = 0,
						f = function () {
							$(els).addClass('slideInUp');
							$(els).addClass('animated');
							$(els).addClass('animation-delay-200');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				if(elementScrolled('#module_3')) {
					var els = $('#module_3'),
						i = 0,
						f = function () {
							$(els).addClass('slideInUp');
							$(els).addClass('animated');
							$(els).addClass('animation-delay-300');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				
				if(elementScrolled('#cs6_l')) {
					var els = $('#cs6_l'),
						i = 0,
						f = function () {
							$(els).addClass('slideInLeft');
							$(els).addClass('animated');
							$(els).addClass('animation-delay-200');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				
				if(elementScrolled('#cs6_r')) {
					var els = $('#cs6_r'),
						i = 0,
						f = function () {
							$(els).addClass('slideInRight');
							$(els).addClass('animated');
							$(els).addClass('animation-delay-200');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				
				if(elementScrolled('#cs6_1_l')) {
					var els = $('#cs6_1_l'),
						i = 0,
						f = function () {
							$(els).addClass('slideInLeft');
							$(els).addClass('animated');
							$(els).addClass('animation-delay-200');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				
				if(elementScrolled('#cs6_1_r')) {
					var els = $('#cs6_1_r'),
						i = 0,
						f = function () {
							$(els).addClass('slideInRight');
							$(els).addClass('animated');
							$(els).addClass('animation-delay-200');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}
				
				
				
			});
			
		});
		

</script>
