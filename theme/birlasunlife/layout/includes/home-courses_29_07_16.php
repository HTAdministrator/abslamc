<?php
$catid = optional_param('catid', 1, PARAM_INT);
global $DB,$CFG,$USER;
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot.'/mod/scorm/locallib.php');
$courserenderer = $PAGE->get_renderer('core', 'course');
?>

<section class="animation-section" id="animation-section">
    <h1 class="h1-heading"><span><?php echo get_string('animation','theme_birlasunlife');?></span></h1>
    <div class="container">

        <div class="row slick-animations-slider">
            <?php
            $animations = get_coursemodules_in_course('url', 22);
            /*foreach ($animations as $k => $v) {
                $desc = $DB->get_record('url', array('id' => $v->instance));
                echo "<div class='animation-blocks'> ";?>
                <a href="<?php echo $CFG->wwwroot; ?>/mod/<?php echo $v->modname; ?>/view.php?id=<?php echo $v->id; ?>"><div class='animation-image-block'>";
                <?php if ($desc->intro) { ?>
                    <?php echo $desc->intro; ?>	
                    </div>
                    <div class="animation-heading-block"><?php echo $desc->headertext; ?></div>
                    <div class="animation-details-block"><?php  echo $desc->bodytext; ?></div>
                    <?php } ?>
                    </a> </div> 
                    <?php } ?>/*
             * 
             * 
             */
            foreach ($animations as $k => $v) {
             $desc = $DB->get_record('url', array('id' => $v->instance));
             echo "<div class='col-sm-4'><div class='animation-blocks'>";?>
             <a href="<?php echo $CFG->wwwroot; ?>/mod/<?php echo $v->modname; ?>/view.php?id=<?php echo $v->id; ?>">
            <?php echo    "<div class='animation-image-block'> ";
            if ($desc->intro) { echo $desc->intro; }
             echo "</div></a><div class='animation-heading-block'><br>";
             echo $desc->headertext;
             echo "</div><div class='animation-details-block'>";
             echo $desc->bodytext;  
             echo "</div></div></div>";
            }
            ?>
            
        </div>
    </div>
</section>

<section class="course-section">
    <h1 class="h1-heading"><span><?php echo get_string('themelearingmodules', 'theme_birlasunlife'); ?></span></h1>


    <div class="container class">
        <div class="row class">
            <div class="col-sm-12 language-sorting">
                <?php $categories = $OUTPUT->get_list_categories(); ?>
<!--<select id="categories" name="categories" id="categories">
    <option value="">All Module</option>
<?php foreach ($categories as $k => $v) { ?>
        <option value="<?php echo $v->id; ?>" <?php if ($v->id == $catid) {
        echo"selected";
    } ?>><?php echo $v->name; ?></option>
            <?php } ?>
    
</select>-->
            </div>
            <?php
             $catid = optional_param('catid','', PARAM_INT);
            
             $_SESSION[SESSION]->lang;
             $categories=$OUTPUT->get_list_categories();
                if($catid!="")
                {
                  $catid=$catid;
                }
                else
                {
                if($_SESSION[SESSION]->lang !="" )
                 {
                 $catid = $OUTPUT->get_course_id_session_lang($_SESSION[SESSION]->lang);
                 }
                  else
                 {
                    if($_SESSION[SESSION]->lang=="")
                    {
                       $lang1= optional_param('lang','', PARAM_RAW);
                       $catid = $OUTPUT->get_course_id_session_lang($lang1);
                     }
                    else
                    {
                      $catid=1;
                    }
                 }
                }
            $courses = $OUTPUT->get_homePage_courses($catid);
            // echo"<pre>";print_r($courses);
            if (count($courses)) {
                ?>
    <?php foreach ($courses as $k => $v) {
        
        $result2 = $DB->get_field("course_modules", "instance", array("course"=>$v['id']));
        $course = $DB->get_record('course', array('id' =>$v['id']), '*', MUST_EXIST);
        $scorm = $DB->get_record('scorm', array('id' => $result2), '*', MUST_EXIST);
         if(! $cm = get_coursemodule_from_instance("scorm", $scorm->id, $course->id, true)) {
                 print_error('invalidcoursemodule');
         }

        if(empty($organization)){
          $organization = $scorm->launch;
         } 
         $orgidentifier = '';
         $sco = scorm_get_sco($organization, SCO_ONLY);
         if($sco->organization == '' && $sco->launch == '')
         {
             $orgidentifier = $sco->identifier;
         } else {
             $orgidentifier = $sco->organization;
         }
         $buttonname= get_string('themelaunch','theme_birlasunlife');
         $string_form="<form id='scormviewform_".$cm->id."' method='post' action='".$CFG->wwwroot."/mod/scorm/player.php'>
                         <input type='hidden' name='mode' value='normal'>
                         <input type='hidden' name='scoid' value='".$scorm->launch."'>
                         <input type='hidden' name='cm' value='".$cm->id."'>
                         <input type='hidden' name='currentorg' value='".$orgidentifier."'>
                         <input type='submit' value='".$buttonname."'></form>";
        
        ?>
                    <div class="col-sm-6">
                        <div class="course-content">
                            <div class="row nopadding">
                                <div class="col-sm-5 nopadding">
                                    <div class="course-image">
                                        <img src="<?php echo $v['courseimage']; ?>" />
                                    </div>
                                </div>	
                                <div class="col-sm-7 nopadding">
                                    <div class="course-details">

                                        <div class="course-details-heading">
                                            <?php echo $v['coursename']; ?>
                                        </div>
                                        <div class="course-details-heading-text">
                                            <?php
                                            $summary = $v['summary'];

                                            if (strlen($summary) > 100) {
                                                $summarystr = substr($summary, 0, 97) . '...';
                                            } else {
                                                $summarystr = $summary;
                                            }

                                            echo $summarystr;
                                            ?>
                                        </div>
                                        <div class="course-details-ratings float-left-div width50 box-sizing">
                                                <!--<span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>
                                                <span class="fa fa-star"></span>-->
                                        </div>
                                        <div class="course-details-launch float-right-div width50 box-sizing">
                                            <?php echo $string_form;?>
                                        </div>								
                                    </div>
                                </div>
                            </div>	
                        </div>
                    </div>
                <?php
                }
            }else {
                echo "<div class='col-sm-12 no-course-available'>" . get_string('nomodules', 'theme_birlasunlife') . "</div>";
            }
            ?>

        </div>
    </div>
</section>


<section class="animation-section video-section" id="videos-section">
    <h1 class="h1-heading"><span><?php echo get_string('video','theme_birlasunlife');?></span></h1>
    <div class="container">
        <div class="row slick-videos-slider">
            <?php
            $videos = get_coursemodules_in_course('url', 21);
            // echo"<pre>";print_r($videos); die;
            /*foreach ($videos as $k => $v) {
                ?>
    <?php $desc = $DB->get_record('url', array('id' => $v->instance), 'intro');
             if ($desc) {
                ?>
                    <a href="<?php echo $CFG->wwwroot; ?>/mod/<?php echo $v->modname; ?>/view.php?id=<?php echo $v->id; ?>"><div class="col-sm-4">
        <?php echo $desc->intro; ?>	
                        </div></a>
    <?php }
} */
            foreach ($videos as $k => $v) {
             $desc = $DB->get_record('url', array('id' => $v->instance));
             echo "<div class='col-sm-4'><div class='animation-blocks'>";?>
             <a href="<?php echo $CFG->wwwroot; ?>/mod/<?php echo $v->modname; ?>/view.php?id=<?php echo $v->id; ?>">
            <?php echo    "<div class='animation-image-block'> ";
            if ($desc->intro) { echo $desc->intro; }
             echo "</div></a><div class='animation-heading-block'><br>";
             echo $desc->headertext;
             echo "</div><div class='animation-details-block'>";
             echo $desc->bodytext;  
             echo "</div></div></div>";
            }
            ?>


        </div>
    </div>
</section>





<?php $hasfrontpagenews = (!empty($PAGE->theme->settings->frontpagenews)); ?>
<?php if ($hasfrontpagenews) { ?>
    <section class="news-section">
        <h1 class="h1-heading"><span>News</span></h1>
        <div class="container class">
            <div class="row">
                <div class="col-sm-6">
                    <div class="news-vertical-arrows"></div>
                    <div id="news-vertical" class="carousel vertical slide">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>
                                </div>
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>					  
                            </div>

                            <div class="item">
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>
                                <div class="news-content">
                                    <p class="news-heading">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    </p>
                                    <div class="news-details">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.	
                                    </div>
                                    <div class="news-redirection"><a href="#">Know More</a></div>						
                                </div>
                            </div>

                        </div>

                        <!-- Controls -->
                        <a class="up carousel-control" href="#news-vertical" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="down carousel-control" href="#news-vertical" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>


                <div class="col-sm-6">
                    <div class="video-container video-container-viemo">
                        <iframe src="//www.youtube.com/embed/KnZfe1qpaKM" frameborder="0" allowfullscreen="" ></iframe>

                    </div><!--//video-container-->
                    <div class="video-details-container">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <div>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </div>	
                    </div>
                </div>

            </div>
        </div>
    </section>
<?php } ?>
<script>

$('#carousel-example-vertical').carousel({
  interval: 3000
})

$(document).ready(function(){
	

	$('.slick-videos-slider').slick({
	 infinite: true,
	 slidesToShow: 3,
	 slidesToScroll: 1,
    	dots: false,	  
		responsive: [
			{
			 breakpoint: 1024,
			 settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,

			 }
			},
			{
			 breakpoint: 600,
			 settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			 }
			},
			{
			 breakpoint: 480,
			 settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			 }
			}
		]	
	});	


	$('.slick-animations-slider').slick({
	 infinite: true,
	 slidesToShow: 3,
	 slidesToScroll: 1,
    	dots: false,	  
		responsive: [
			{
			 breakpoint: 1024,
			 settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,

			 }
			},
			{
			 breakpoint: 600,
			 settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			 }
			},
			{
			 breakpoint: 480,
			 settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			 }
			}
		]	
	});


$("video").prop('muted', true);

$('.main-nav a').on('click', function(event) {
        
        if($(this).parent().parent().parent().hasClass("langmenu")){
        }else{
        
	event.preventDefault();
        
	if($(this).attr("title") == "Modules"){
		$('html, body').stop().animate({
			scrollTop: ($(".course-section").offset().top - 87)
		}, 1000);
	}else if($(this).attr("title") == "Animations"){
		$('html, body').stop().animate({
			scrollTop: ($(".animation-section").offset().top - 87)
		}, 1000);
	}else if($(this).attr("title") == "Videos"){
		$('html, body').stop().animate({
			scrollTop: ($(".video-section").offset().top - 87)
		}, 1000);
	}

        }

   // var target = $(this.getAttribute('href'));
   /* if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }*/
});

	$("#page-header").css({"position":"fixed","width" : "100%","top":"0","z-index" : "111"});


/*	$(".playpause .fa-play").click(function(){
			$(".playpause .fa-pause").css("display","table-cell");
			$(".playpause .fa-play").hide();
			$('.banner-video-player')[0].play();
	});

	$(".playpause .fa-pause").click(function(){
			$('.banner-video-player')[0].pause();
			$(".playpause .fa-play").css("display","table-cell");
			$(".playpause .fa-pause").hide();

	});*/

	$(".playpause").click(function(){
		$(".banner-section").addClass("banner-section-desktop");
		$('.banner-video-player')[0].play();
		$(this).hide();
		$("video").prop('muted', false);
	});

	$(".banner-section .fa.fa-times").click(function(){
		if($("video").prop('muted') == false){

			$(".banner-section").removeClass("banner-section-desktop");
			$('.banner-video-player')[0].play();
			$(".playpause").show();
			$("video").prop('muted', true)

		}
	});	

	$(".banner-video-player").click(function(){
		if($("video").prop('muted') == false){

			$(".banner-section").removeClass("banner-section-desktop");
			$('.banner-video-player')[0].play();
			$(".playpause").show();
			$("video").prop('muted', true)

		}
		/*$(this).removeClass("banner-section-desktop");
		$('.banner-video-player')[0].play();
		$(".playpause").show();
		$("video").prop('muted', true);*/
	});
	
	if($(window).width() < 475){
		$("#page-header").css({"position":"relative", "background": "rgb(73, 77, 85)"});	
	}

});

$(window).scroll(function (event) {
   // if($(window).width() > 768){
		var scroll = $(window).scrollTop();

		console.log(scroll);

		var tempscroll = $(".animation-section").offset().top - 120;
                
                if($("video").prop('muted') != false){
                    if(tempscroll < scroll){
                            $('.banner-video-player')[0].pause();
                                    //$(".playpause .fa-play").css("display","table-cell");
                                    //$(".playpause .fa-pause").hide();
                    }else{
                            //if($(".playpause .fa-pause").css("diplay") == "none"){
                                    $('.banner-video-player')[0].play();

                            //}
                    }
                }

		if(scroll > 100){
			console.log("in");

			$("#page-header").css({"position":"fixed","width" : "100%","top":"0","z-index" : "111", "background" : "#494d55"});

			$("#page-site-index .header .topbar").css({"background":"none"});
			

			if($(window).width() < 475){
				$(".sub-logo").hide();
				$("#logo").hide();
				
			}	
			
		/*}else{
			$("#page-header").css({"background" : "none"});
			$("#page-site-index .header .topbar").css("cssText","");
		}*/
		}else{
			
			$(".sub-logo").show();
			$("#logo").show();
			$("#page-header").css("background", "transparent");
			$("#page-site-index .header .topbar").css("cssText", "");
			
			if($(window).width() < 475){
				$("#page-header").css({"position":"relative", "background": "rgb(73, 77, 85)"});	
			}	
		}
});


</script>
