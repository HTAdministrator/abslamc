<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* LOGO Image */
$haslogo = (!empty($PAGE->theme->settings->logo));

/* Logo settings */
if ($haslogo) {
$logourl = $PAGE->theme->setting_file_url('logo', 'logo');
} else {
$logourl = $OUTPUT->pix_url('logo', 'theme');
}


/* Header widget settings */
$hasheaderwidget = (!empty($PAGE->theme->settings->headerwidget));
$headerwidget = $PAGE->theme->settings->headerwidget;
if(!isloggedin()){
  $frontpageheadspan='frontpageheadspan';  
}else{
    $frontpageheadspan='';
}
?>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo $CFG->httpswwwroot ?>/theme/birlasunlife/style/animate.css" rel="stylesheet" />
<link href="<?php echo $CFG->httpswwwroot ?>/theme/birlasunlife/style/style_nw.css" rel="stylesheet" />



<script src="<?php echo $CFG->httpswwwroot ?>/theme/birlasunlife/jquery/jquery.viewportchecker.js"></script>

<script>
$(document).ready(function() {
   /* 
    $('.banner-section .container > .row').addClass("hidden-1").viewportChecker({
        classToAdd: 'visible animated slideInUp',
        offset: 100
       });

    $('.container > .row.slick-animations-slider').addClass("hidden-1").viewportChecker({
        classToAdd: ' visible animated slideInLeft',
        offset: 100
       });	   

    $('.course-section .container > .row').addClass("hidden-1").viewportChecker({
        classToAdd: ' visible animated slideInUp',
        offset: 100
       });	   

    $('.container > .row.slick-videos-slider').addClass("hidden-1").viewportChecker({
        classToAdd: ' visible animated slideInRight',
        offset: 100
       });	   

    $('#page-site-index #page-footer .row > .footer_container').addClass("hidden-1").viewportChecker({
        classToAdd: 'visible animated slideInUp',
        offset: 100
       });    
     $('#page-site-index #page-footer .lower-div').addClass("hidden-1 visible animated slideInUp").viewportChecker({
        classToAdd: 'visible animated slideInUp',
        offset: 100
       });*/

	 
	$(".header .main-nav .nav li a").css("transition", "0s"); 
	var currentLang = "<?php echo $_SESSION[SESSION]->lang;?>";
	//alert(currentLang);
	if((getUrlParameter("lang") && getUrlParameter("lang") == "ta") || (currentLang && currentLang == "ta")){
		$(".header .main-nav .nav li a").css("padding", "15px 15px");
	} 	
	
	//$(".header .main-nav .nav li a").css("transition", " all .4s ease-in-out"); 	
	  
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
</script>


<!--<header id="page-header" class="header <?php //echo $frontpageheadspan; ?> <?php if (isloggedin()) { echo "loggedInUser";} ?>">
	<div class="row header-top">
			<div class="columWrapper clearfix">
				<div class="topbar_active topbar_transparent">
					  <div class="topcontent clearfix">
						<div class="top-link box_socials pull-right"> 
						<a href="https://adityabirlacapital.com/en/Corporate?_ga=2.75345106.1928849228.1517467881-46591046.1517467881" target="_blank"> Corporates</a> 
						<a href="https://adityabirlacapital.com/en/Advisors?_ga=2.75345106.1928849228.1517467881-46591046.1517467881" target="_blank"> Advisors</a> 
						<a href="https://adityabirlacapital.com/en/Customer-Services?_ga=2.37200480.1928849228.1517467881-46591046.1517467881" target="_blank"> Customer Service</a>
						<a href="https://adityabirlacapital.com/en/About-Us/Careers?_ga=2.37200480.1928849228.1517467881-46591046.1517467881" target="_blank">Careers</a>
						<a href="https://adityabirlacapital.com/en/About-Us?_ga=2.238324928.1928849228.1517467881-46591046.1517467881" target="_blank"> About Us</a> 
						 </div>
					  </div>
				</div>
			</div>
	</div>
 
    <div class="row corporate">  
		<div class="columWrapper clearfix">
		
		<div class="main_bar_active">
			<div class="navbar navbar-default">
                            
			<div class="navbar-header">
				<a href="<?php echo $CFG->httpswwwroot ?>"><img id="logo" src="<?php echo $logourl; ?>" alt="<?php echo $SITE->shortname; ?>" /></a>
			</div>
				<div class="minimainlink_menu">
					<ul class="nav navbar-nav navbar-left">
					  <li><a href="https://adityabirlacapital.com/en/Protecting?_ga=2.218366041.1963910145.1517812733-1380603538.1517222623" target="_blank">PROTECTING </a> </li>
					  <li><a href="https://adityabirlacapital.com/en/Investing?_ga=2.59295725.1963910145.1517812733-1380603538.1517222623" target="_blank">ADVISING </a></li>
					  <li><a href="https://adityabirlacapital.com/en/Financing?_ga=2.59295725.1963910145.1517812733-1380603538.1517222623" target="_blank">FINANCING</a></li>
					  <li><a href="https://adityabirlacapital.com/en/Advising?_ga=2.59295725.1963910145.1517812733-1380603538.1517222623" target="_blank">INVESTING</a></li>
					</ul>
                                </div>
			  
			<div class="customerservice_menu">		
				<ul class="nav navbar-nav navbar-right">
						   <?php if (!isloggedin()) { ?>
						   <li class="lg-img">
							<a class="btn btn-cta btn-cta-secondary loginButton " href="<?php echo $CFG->httpswwwroot ?>/login/" ><?php echo get_string('login') ?></a>
							</li>
							<?php } ?>                    
							
							<?php if (isloggedin()) { ?>
								<?php echo $OUTPUT->user_menu(); ?> 
							<?php } ?>	
				</ul>	

				<nav role="navigation" class="main-nav float-right-div">
					<div class="navbar-header">
						<button class="navbar-toggle btn-navbar" type="button" data-toggle="collapse" data-target="#nav-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div
				</nav>	
			
			</div>
		</div> 
			</div> 
		</div>
		</div>
		
				
		<div class="row headerDesktop">
        <div class="primaryNavWrapper">
            <div class="columWrapper clearfix">
                
				<div class="primaryNav">
					<div class="brandTitle">
							<a href="<?php echo $CFG->httpswwwroot ?>">Mutual funds</a>
                                                        <a href="<?php echo $CFG->httpswwwroot ?>"><?php echo get_string('themelearingmodules', 'theme_birlasunlife');?></a>
					</div>
				</div>
				<div class="navbar megamenu" role="navigation">
							<div class="smallmenu">
								<div class="navbar-offcanvas navbar-offcanvas-touch  navbar-offcanvas-right navbar-inverse" id="sidenavbar">
									<div class="navbar-header1">
									<button type="button" class="navbar-toggle offcanvas-toggle pull-right" data-toggle="offcanvas" data-target="#sidenavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
									</div>
									<div id="nav-collapse" class="nav-collapse collapse">
										<?php  echo $OUTPUT->custom_menu(); ?>              
									</div>
								</div>
							</div>
				</div>
                
                                <div class="rightCol">
                                        <div class="primaryNav">
                                          <ul>
                                            <li><a href="#" class="ssomfpostClick pms_log"><span class="pms_log_text"><u>Go to</u> Mutual Fund Website</span></a>

                                            </li>
                                          </ul>
                                        </div>
                              </div>
            </div>
        </div>
    </div>
		
		
		<div class="row adityaFix">
		<div class="columWrapper clearfix"> 
		<div class="bottombar_dark">
			  <div class="abname">
				<p>Aditya Birla Sun Life Mutual Fund</p>
			  </div>
		</div>
		</div>
		</div>-->
<?php //echo"<pre>";print_r($_SERVER['REQUEST_URI']);


    
?>
<div id="page-header" class="header <?php //echo $frontpageheadspan; ?> <?php if (isloggedin()) { echo "loggedInUser";} ?>">
<div class="mast_head ">
            <header class="header-static">

                <div class="header-top">
                    <div class="div_head">

                        <div class="col-md-5 hidden-xs hidden-sm">
                        </div>
                        <div class="col-md-7 hidden-xs hidden-sm">
                            <div class="pull-right">
                                <nav>
                                    <ul class="nav nav-service navbar-nav nav-pills">
                                        <li class="">
                                            <a href="https://adityabirlacapital.com/corporate" target="_blank" title="Corporates" class="">
                                                Corporates
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="https://www.adityabirlacapital.com/advisors" target="_blank" title="Advisors" class="">
                                                Advisors
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="https://www.adityabirlacapital.com/customer-services" target="_blank" title="Customer Services" class="">
                                                Customer Services
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="https://www.adityabirlacapital.com/about-us/careers" target="_blank" title="Career" class="">
                                                Career
                                            </a>
                                        </li>
                                        <li class="divider-left">
                                            <a href="https://www.adityabirlacapital.com/About-Us?_ga=2.23695480.339100320.1557747752-1004736259.1557300437" target="_blank" title="About Us" class="">
                                                About Us
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
          <nav class="navbar navbar-default navbar-static custom-nav" id="mainNavbar">
                    <div class="navigation_div">

                        <div class="navbar-left">
                            <!--<span class="icon-icon-login abcicons cust-log-ico-mob hidden-md hidden-lg "></span>-->
<button type="button" class="navbar-toggle pull-right collapsed menu-icon" data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="logo" itemscope itemprop="organization" itemtype="http://schema.org/Organization">
                                <a itemprop="url" href="<?php echo $CFG->httpswwwroot ?>">
                                    <span class="hidden-md hidden-lg">
                                    <img src="<?php echo $OUTPUT->pix_url('ABC-Logo-sm-xs','theme'); ?>" class="" alt="logo" title="" />
                                </span>
                                 <span class="hidden-sm hidden-xs">
                                   <a href="<?php echo $CFG->httpswwwroot ?>"><img id="logo" src="<?php echo $logourl; ?>" alt="<?php echo $SITE->shortname; ?>" /></a>
                                </span>
                                </a>
                            </div>
                        </div>
                        <div class="navbar-center pos-nav hidden-xs hidden-sm">
                            <div class="collapse navbar-collapse ">
                                <ul class="nav navbar-nav center-list">
                                    <li class="">
                                        <a class="text-uppercase active" href="https://www.adityabirlacapital.com/protecting" target="_blank">
                                            Protecting
                                        </a>
                                    </li>
                                    <li class="">
                                        <a class="text-uppercase" href="https://www.adityabirlacapital.com/advising" target="_blank">
                                            Advising
                                        </a>
                                    </li>
                                    <li class="">
                                        <a class="text-uppercase" href="https://www.adityabirlacapital.com/financing" target="_blank">
                                            Financing
                                        </a>
                                    </li>
                                    <li class="">
                                        <a class="text-uppercase" href="https://www.adityabirlacapital.com/investing" target="_blank"">
                                            Investing
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <ul class="navbar-right">
                                                   <?php if($PAGE->pagetype=='login-index'){
                                                       
                                                   }else if (!isloggedin()) { ?>
						   <li class="lg-img">
                                                       
                                                       <a class="btn btn-cta btn-cta-secondary loginButton " href="<?php echo $CFG->httpswwwroot ?>/login/" ><span class="weblogindiv"><?php echo get_string('login') ?></span></a>
							</li>
							<?php } ?>                    
							
							<?php if (isloggedin()) { ?>
								<?php echo $OUTPUT->user_menu(); ?> 
							<?php } ?>	
				</ul>	
             

                        <!-- mobile dropdown div starts here -->
                        <div class="navbar-inverse side-collapse redbg in hidden-lg hidden-md  mob-nav">
                            <nav role="navigation" class="navbar-collapse navbar-center">
                                <a itemprop="url" href="/en" class="hidden-lg hidden-md tab-logo ">
                                    <img src="<?php echo $OUTPUT->pix_url('ABC_LOGO','theme'); ?>" class="" alt="logo" title="">
                                </a>
                                <div class="mrt-30 lbl_fst">
                                    <ul class="nav navbar-nav center-list">
                                        <li><a href="https://www.adityabirlacapital.com/protecting"  target="_blank" class="text-uppercase active">Protecting</a></li>
                                        <li><a href="https://www.adityabirlacapital.com/investing" target="_blank" class="text-uppercase">Investing</a></li>
                                        <li><a href="https://www.adityabirlacapital.com/financing" target="_blank" class="text-uppercase">Financing</a></li>
                                        <li><a href="https://www.adityabirlacapital.com/advising" target="_blank" class="text-uppercase">Advising</a></li>
                                    </ul>
                                </div>
                              <?php if($PAGE->pagetype=='login-index'){ }else if(!isloggedin()){
                                  
                              ?>
                                <div class="navbar-right">
                                    <ul class="nav navbar-nav center-list">
                                        <li class="right-log">
                                            <a href="<?php echo $CFG->httpswwwroot ?>/login/" class="text-uppercase user-log">
                                                <span class="">login</span>
                                                <span class="icon-icon-login abcicons "></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                              <?php }  ?>
                                <div class="header-top">
                                    <ul class="nav nav-service navbar-nav nav-pills">
                                        <li>
                                            <a href="https://adityabirlacapital.com/corporate" target="_blank" class="">Corporates</a>
                                        </li>
                                        <li>
                                            <a href="https://www.adityabirlacapital.com/advisors" target="_blank" class="">Advisors</a>
                                        </li>
                                
                                        <li>
                                            <a href="https://www.adityabirlacapital.com/customer-services" target="_blank" class="">Customer Service</a>
                                        </li>
                                         <li>
                                            <a href="https://www.adityabirlacapital.com/about-us/careers" target="_blank" class="">Career</a>
                                        </li>
                                          <li>
                                            <a href="https://www.adityabirlacapital.com/About-Us?_ga=2.23695480.339100320.1557747752-1004736259.1557300437" target="_blank" title="About Us" class="">
                                                About Us
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                          
                                <p class="secondary  mob-cop">
                                    © 2017, Aditya Bilra Capital Inc.
                                    All Rights Reserved.
                                </p>
                            </nav>
                        </div>
                        <!-- mobile dropdown div ends here -->

                    </div>
                </nav>
            </header>

            <!-- subnavigation second level starts here -->
            <div class="subnav-custom">
                <nav class="navbar navbar-default custom-navbar-subnav  investing affix-top" id="desk-menu">
                    <div class="div_subnav">


                        <!-- visible mobile starts main dropdown -->
                        <div class="mob_nav  hidden-md hidden-lg">
                            <ul class="nav navbar-nav custom-sub-nav mob_drp">
                                <li class="nav-item scnd_center hidden-md lob_collapse">
                                    <a href="#" class="dropdown-toggle ico-drop no-pad text-uppercase" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="Mutualdiv">MUTUAL FUNDS</span></a>
                                </li>
                            </ul>
                            <header class="lob_expand no-display">
                                <div class="inline-f w-100 mob_sub_g">
                                    <p class="click_heading text-bold"><span class="Mutualdiv">MUTUAL FUNDS</span></p>
                                    <span class="abcicons icon-icon-close icon-clo"></span>
                                </div>
                            </header>
                        </div>
                        <!-- visible mobile ends main dropdown -->
                        <div class="collapse navbar-collapse no-pad " id="bs-example-navbar-collapse">
                            <ul class="nav navbar-nav custom-sub-nav  ">
                                <li class="nav-item hidden-sm hidden-xs desk_fst_child">
                                    <a href="#" class="no-pad text-uppercase"><span class="Mutualdiv">MUTUAL FUNDS</span></a>
                                </li>
                                <!-- list in lg dropdown starts-->
                               
                                <!--- list in lg dropdown ends -->

                                   
                               <!-- <li class="nav-item scnd_center">
                                    <a href="#" class="dropdown-toggle ico-drop no-pad " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Education</a>
                                    <ul class="dropdown-menu drop-menu-nav">
                                        <li class="normal_drp"><a href="#" class="">What is Mutual Fund</a></li>
                                        <li class="normal_drp"><a href="#" class="">Why Mutual Fund</a></li>
                                        <li class="normal_drp"><a href="#" class="">Plan your Finance</a></li>
                                        <li class="normal_drp"><a href="#" class="">Investing Right </a></li>
                                      
                                    </ul>
                                </li>
                               

                               <li class="nav-item scnd_center">
                                    <a href="#" class="dropdown-toggle ico-drop no-pad " data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Sections</a>
                                    <ul class="dropdown-menu drop-menu-nav">
                                        <li class="normal_drp"><a href="#" class="">Watch</a></li>
                                        <li class="normal_drp"><a href="#" class="">Read</a></li>
                                        <li class="normal_drp"><a href="#" class="">Listen</a></li>
                                        <li class="normal_drp"><a href="#" class="">Learning Modules</a></li>
                                        <li class="normal_drp"><a href="#" class="">Investor Hangouts</a></li>
                                    </ul>
                                </li>


                                <li class="nav-item scnd_center">
                                    <a href="#" class="dropdown-toggle ico-drop no-pad abc_mb_2nd" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tools & Calculators</a>
                                    <ul class="dropdown-menu drop-menu-nav">
                                        <li class="normal_drp"><a href="#" class="">SIP Calculator</a></li>
                                        <li class="normal_drp"><a href="#" class="">Goal Planning</a></li>
                                        <li class="normal_drp"><a href="#" class="">Retirement</a></li>
                                        <li class="normal_drp"><a href="#" class="">Smart Selfie</a></li>
                                        
                                    </ul>
                                </li>
                                <li class="nav-item scnd_center">
                                    <a href="#" class="dropdown-toggle ico-drop no-pad abc_mb_2nd" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Others</a>
                                    <ul class="dropdown-menu drop-menu-nav">
                                        <li class="normal_drp"><a href="#" class="">Insights</a></li>
                                        <li class="normal_drp"><a href="#" class="">Social Page</a></li>
                                        <li class="normal_drp"><a href="#" class="">A-Z videos </a></li>
                                    </ul>
                                </li>
                                 <li class="nav-item scnd_center"><a href="#" class="no-pad ">Events</a></li>-->
                               <?php  echo $OUTPUT->custom_menu(); ?>   

                            </ul>
                            <ul class="nav navbar-nav navbar-right  custom-sub-nav ">
                             <!--    <li class="scnd_center">
                               
                                    <a href="#" class="no-pad"> 
                                        <i class="fa fa-calendar"></i>
                                        </a>
                                </li> -->
                                <li class="scnd_center">
                                   <a href="#" class="no-pad"> 
                                        
                                        <i class="fa fa-search"></i></a>
                                </li>

                            </ul>
                        </div>
                    </div>

                </nav>
            </div>
                <div class="comp_name b-green"><p class="labelText">Aditya Birla Sun Life Asset Management Company LTD.</p></div>


     
        </div>


    
<!-- black footer section start -->
 
<div class="row"></div>
    <!--<div class=" branding container-fluid">     
        <div class="row" >
            <h1 class="logo col-md-5 col-sm-12 col-xs-12">
                <a href="<?php echo $CFG->httpswwwroot ?>"><img id="logo" src="<?php echo $logourl ?>" alt="<?php echo $SITE->shortname; ?>" /></a>
            </h1><!--//logo--
            
            <div class="info-container col-md-7 col-sm-12 col-xs-12">
                
                <div class="logininfo-container">
                    <?php //echo $PAGE->headingmenu ?>
                    
                    <?php if (!isloggedin()) { ?>
                    <a class="btn btn-cta btn-cta-secondary" href="<?php echo $CFG->httpswwwroot ?>/login/" ><?php echo get_string('login') ?></a>
                    <?php } ?>                    
                    
                    <?php if (isloggedin()) { ?>
                        <?php echo $OUTPUT->user_menu(); ?> 
                    <?php } ?>
                    
                </div><!--//logininfo-container--
                
                <?php /* Moodle 3.1 global search feature - solr needs to be configured */
                if (method_exists($OUTPUT, 'search_box')) {
                  echo $OUTPUT->search_box(); // global search, for Moodle 3.1+
                }
                ?>
            
                <?php if($hasheaderwidget) {?>
                <div class="header-widget hidden-xs hidden-sm">
                    <?php echo $headerwidget ?>
                </div><!--//header-widget--
                <?php }?>
            </div>
        </div><!--//row--           
    </div><!--//branding-->
</div>