/*! sliderevealMob - v1.1.2 - 2016-05-16
* https://github.com/nnattawat/sliderevealMob
* Copyright (c) 2016 Nattawat Nonsung; Licensed MIT */
/*! sliderevealMob - v1.1.1 - 2016-03-04
* https://github.com/nnattawat/sliderevealMob
* Copyright (c) 2016 Nattawat Nonsung; Licensed MIT */
(function ($) {
  // Private attributes and method
  var getPadding = function($el, side) {
    var padding = $el.css('padding-' + side);
    return padding ? +padding.substring(0, padding.length - 2) : 0;
  };

  var sidePosition = function($el) {
    var paddingLeft = getPadding($el, 'top');
    var paddingRight = getPadding($el, 'bottom');
    return ($el.width() + paddingLeft + paddingRight) + "px";
  };

  var SlideRevealMob = function($el, options) {
    // Define default setting
    var setting = {
      width: 250,
      push: true,
      position: "top",
      speed: 300, //ms
      trigger: undefined,
      autoEscape: true,
      show: function(){},
      shown: function(){},
      hidden: function(){},
      hide: function(){},
      //top: 0,
      overlay: false,
      "zIndex": 1049,
      overlayColor: 'rgba(0,0,0,0.5)'
    };

    // Attributes
    this.setting = $.extend(setting, options);
    this.element = $el;

    this.init();
  };

  // Public methods
  $.extend(SlideRevealMob.prototype, {
    init: function() {
      var self = this;
      var setting = this.setting;
      var $el = this.element;

      var transition = "all ease " + setting.speed + "ms";
      $el.css({
        position: "fixed",
        width: setting.width,
        transition: transition,
        height: "232px",
        top: "0"
      })
      .css(setting.position, "-233px");// + sidePosition($el));

      if (setting.overlay) {
        $el.css('z-index', setting.zIndex);

        self.overlayElement = $("<div class='slide-reveal-overlay'></div>")
        .hide()
        .css({
          position: 'fixed',
          top: 65,
          bottom: 0,
          height: '232px',
          width: '100%',
          'z-index': setting.zIndex - 1,
          'background-color': setting.overlayColor,
        }).click(function() {
          self.hide();
        });

        $(".branch-locator-mobile").prepend(self.overlayElement);
      }

      // Add close stage
      $el.data("slide-reveal", false);

      if (setting.push){
        $(".branch-locator-mobile").css({
          position: "relative",
          "overflow-x": "hidden",
          transition: transition,
          top: "0px"
        });
      }

      // Attach trigger using click event
      if (setting.trigger && setting.trigger.length > 0) {
        setting.trigger.on('click.slideRevealMob', function() {
          if (!$el.data("slide-reveal")) { // Show
            self.show();
          } else { // Hide
            self.hide();
          }
        });
      }

      // Bind hide event to ESC
      if (setting.autoEscape) {
        $(document).on('keydown.slideRevealMob', function(e) {
          if ($('input:focus, textarea:focus').length === 0) {
            if (e.keyCode === 27 && $el.data("slide-reveal")) { // ESC
              self.hide();
            }
          }
        });
      }
    },

    show: function(triggerEvents) {
      var setting = this.setting;
      var $el = this.element;
      var $overlayElement = this.overlayElement;

      // trigger show() method
      if (triggerEvents === undefined || triggerEvents) { setting.show($el); }

      // show overlay
      if (setting.overlay) {
        $overlayElement.show();
      }

      // slide the panel
      $el.css(setting.position, "65px");

      $el.css('top',"65px")
      if (setting.push) {
        if (setting.position === "top") {
          $(".branch-locator-mobile").css("top", sidePosition($el));
        } else {
          $(".branch-locator-mobile").css("top", "-" + sidePosition($el));
        }
      }
      $el.data("slide-reveal", true);

      // trigger shown() method
      if (triggerEvents === undefined || triggerEvents) {
        setTimeout(function() {
          setting.shown($el);
        }, setting.speed);
      }
    },

    hide: function(triggerEvents) {
      var setting = this.setting;
      var $el = this.element;
      var $overlayElement = this.overlayElement;

      // trigger hide() method
      if (triggerEvents === undefined || triggerEvents) { setting.hide($el); }

      // hide the panel
      if (setting.push) {
        $(".branch-locator-mobile").css("top", "0px");
      }
      $el.css(setting.position, "-" + sidePosition($el));
      $el.data("slide-reveal", false);

      // trigger hidden() method
      if (triggerEvents === undefined || triggerEvents) {
        setTimeout(function(){
          // hide overlay
          if (setting.overlay) {
            $overlayElement.hide();
          }

          setting.hidden($el);
        }, setting.speed);
      }
    },

    toggle: function(triggerEvents) {
      var $el = this.element;
      if ($el.data('slide-reveal')) {
        this.hide(triggerEvents);
      } else {
        this.show(triggerEvents);
      }
    },

    remove: function() {
      this.element.removeData('slide-reveal-model');
      if (this.setting.trigger && this.setting.trigger.length > 0) {
        this.setting.trigger.off('.slideRevealMob');
      }
      if (this.overlayElement && this.overlayElement.length > 0) {
        this.overlayElement.remove();
      }
    }

  });

  // jQuery collection methods
  $.fn.slideRevealMob = function (options, triggerEvents) {
    if (options !== undefined && typeof(options) === "string") {
      this.each(function() {
        var slideRevealMob = $(this).data('slide-reveal-model');

        if (options === "show") {
          slideRevealMob.show(triggerEvents);
        } else if (options === "hide") {
          slideRevealMob.hide(triggerEvents);
        } else if (options === 'toggle') {
          slideRevealMob.toggle(triggerEvents);
        }
      });
    } else {
      this.each(function() {
        if ($(this).data('slide-reveal-model')) {
          $(this).data('slide-reveal-model').remove();
        }
        $(this).data('slide-reveal-model', new SlideRevealMob($(this), options));
      });
    }

    return this;
  };

}(jQuery));
