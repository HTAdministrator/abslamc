
function fnMastHeadFunctions() {

    var sideslider = $('[data-toggle=collapse-side]');
    var sel = sideslider.attr('data-target');
    var sel2 = sideslider.attr('data-target-2');
    sideslider.click(function (event) {
        fnShowOverlay(true);
        $(sel).toggleClass('in');
        $(sel2).toggleClass('out');
    });


    function fnShowOverlay(isEnabled) {
        var width = $(window).width();
        var IsTab = (width >= 601 && width <= 800);
        var AllSmallDevices = (width <= 800);
        //$(".menu-icon").toggleClass("open");
        if (isEnabled ) {
            if ($(".bodywrapper").hasClass("over_scroll")) {
                $(".bodywrapper").removeClass("over_scroll");
            } else {
                $(".bodywrapper").addClass("over_scroll");
            }
            //$(".cust-log-ico").toggleClass("no-display");
            if (IsTab) {
                // $('.logo').toggleClass("no-display");
                $('.overlay').toggleClass("display_blk");
                setTimeout(function () {
                    $(".overlay").toggleClass("over_fade");
                }, 1);
                IsOverlayDisplay = $('.overlay').hasClass("display_blk");
            }
            

        }

        if (!AllSmallDevices) {
            $(".bodywrapper").removeClass("over_scroll");
            $('.overlay').removeClass("display_blk");
            $('.logo').removeClass("no-display");
        }


        setTimeout(function () {
             width = $(window).width();
             IsTab = (width >= 601 && width <= 800);
             AllSmallDevices = (width <= 800);

             var isData = !($(".side-collapse").hasClass("in"));

             if (IsTab && !isEnabled) {
                 if (isData) {
                     $('.logo').addClass("no-display");
                     $('.overlay').addClass("display_blk");
                     $(".bodywrapper").addClass("over_scroll");
                 }
             }

        }, 100)
        

        if (!isEnabled && $("#bs-example-navbar-collapse").hasClass("in")) {
            $(".icon-icon-close").click();
        }
       

    }
    //menu mobile open close

    $(".menu-icon").click(function () {
        var width = $(window).width();
        $(".menu-icon").toggleClass("open");
        $(".navbar-left .cust-log-ico-mob").toggleClass("no-display");
        $(".custom-nav").toggleClass("position-fixed-nav");
        $(".divicon ").toggleClass("no-display");

        //$("body").toggleClass("over_scroll");
        $(".cust-log-ico").toggleClass("no-display");
        if (width >= 601 && width <= 800) {
            $('.logo').toggleClass("no-display");
            //    $('.overlay').toggleClass("display_blk");
        }
        
    });



    $(".mob_nav a").click(function () {
        $("#bs-example-navbar-collapse").toggleClass("in");
        $(".subnav-custom").toggleClass("click_mob");

       
        $(".lob_collapse").addClass("no-display");
        $(".lob_expand").toggleClass("block-display");
        $(".click_mob .custom-navbar-subnav").addClass("ht_100");
        SetFixedPosition(this, "nav");
        fnShowOverlay(true);

        setTimeout(function(){

             $(".subnav-custom").toggleClass("over_fade");
        },20);
    });

    $(".icon-icon-close").click(function () {

        $("#bs-example-navbar-collapse").removeClass("in");
        $(".subnav-custom").removeClass("click_mob");
        $(".subnav-custom").removeClass("over_fade");
        $(".lob_expand").removeClass("block-display");
        $(".lob_collapse").removeClass("no-display");
        $(".subnav-custom .navbar-default").removeClass("ht_100");
        SetFixedPosition(this, "nav");
        // var dt = parseInt($('.overlay').css("z-index"));
        // if (dt != 50) {
        //     $('.overlay').css("z-index", "50")
        // } else {
        //     $('.overlay').css("z-index", "80")
        // }
        fnShowOverlay(true);
    });


    $(".nav-item .abc_mb_2nd").click(function () {
        var parentNav = $(this).closest("nav");

        if (parentNav.length <= 0) {
            return;
        }
        var width = $(window).width();
        if (width >= 320 && width <= 800) {

            if ($(parentNav).hasClass("ht_100")) {
                $(parentNav).removeClass("ht_100");
            } else {
                $(parentNav).addClass("ht_100");
            }

            if (parentNav.height() <= $(window).height()) {
                $(parentNav).addClass("ht_100");
            }
        }
    });

    function SetFixedPosition(obj, destion) {
        var parentNav = $(obj).closest(destion);

        if (parentNav.length <= 0) {
            return;
        }

        if ($(parentNav).hasClass("menu_fix_top")) {
            $(parentNav).removeClass("menu_fix_top");
        } else {
            $(parentNav).addClass("menu_fix_top");
        }

        if ($(parentNav).hasClass("submenu_fix")) {
            $(parentNav).removeClass("submenu_fix");
        } else {
            $(parentNav).addClass("submenu_fix");
        }
    }

    function fnSetMastLayout(){

                                
        var width = $(window).width();

        /*    if(width >= 801){
                $('.mast_head').removeClass('container-fluid').addClass('container');
            }
           

            else if(width >= 320 && width <= 800){
                $('.mast_head').removeClass('container').addClass('container-fluid');
            }*/
    }

    fnSetMastLayout();

    $(window).resize(function () {
        fnShowOverlay(false);
        fnSetMastLayout();
    });

    
    $(".overlay").click(function () {
        fnCloseOpenMenus();
    });

    $(window).on("orientationchange", function () {
        fnCloseOpenMenus();
    });

    function fnCloseOpenMenus() {
                                                                  
                            
                                  
            

        var width = $(window).width();
        var AllSmallDevices = (width <= 800);
                                             
            

        if (AllSmallDevices) {
            var isMenuOpen = !($(".side-collapse").hasClass("in"));
            if (isMenuOpen) {
                sideslider.click();
            }

            isMenuOpen = $("#bs-example-navbar-collapse").hasClass("in");
            if (isMenuOpen) {
                $(".icon-icon-close").click();
            }
        }

        $(".bodywrapper").removeClass("over_scroll");
        $('.overlay').removeClass("display_blk");
        $('.logo').removeClass("no-display");
    }
}                                
                           
       

     
$(document).ready(function() {

fnMastHeadFunctions();
 $(window).resize(function(){
           var  content = $(".dashboard").find(".accordion");
            var width = $(window).width();

             if(width >= 320 && width <= 600){
          
              $(".panel-collapse").removeClass("in"); 
          $(".accordion-toggle").addClass("collapsed");
           
            }
            else if(width >= 601){
          
              $(".panel-collapse").addClass("in"); 
           
            }



            })

.resize();


$( ".dropbtn" ).click(function(e) {
    $(".dash_drp_sub").toggleClass("show");
     e.stopPropagation(); // This is the preferred method.
    return false;        // This should not be used unless you do not want
                         // any click events registering inside the div
  
});

$(document).click(function() {
    $(".dash_drp_sub").removeClass("show");
    
});

$('.dash_drp_sub li:last').prev().addClass('divider');
$('.dash_drp_sub li:last').addClass('last_chld');


$( ".btn-contact a" ).addClass("no_wrap");


});


$(document).ready(function(){
 
 
    $('.section-max-height').each(function(){  
      
   
      var highestBox = 0;
      
    
      $('.thumbnail-box', this).each(function(){
        
      
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }
      
      });  
            
 
      $('.thumbnail-box',this).css('height',highestBox);
                    
    }); 

});   





(function($){
  $(window).on("load",function(){

    /*$(".content-1").mCustomScrollbar({
      theme:"minimal"
    });*/
     sectionmaxheight();
      sectionheight();
  });
})(jQuery);

$(document).ready(function() {
 // $('select').niceSelect();
});
 

 function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
    if($('#defaultOpen').is(":visible"))
        document.getElementById("defaultOpen").click();

    $(function () {
     /*   $('#datetimepicker1').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            todayHighlight: true
        });*/
    });

 /*   $(function () {
        $('#datetimepickertime').datetimepicker({
            format: 'DD-MM-YYYY LT'
        });
       
    });*/
function initMap() {
  // The location of Uluru
  var uluru = {lat: -25.344, lng: 131.036};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 4, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}

  let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
        $('#image-gallery-image')
          .attr('src', $sel.data('image'));
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

$(function() {

  
/* bia tab starts here */
    
    $(".tabcontent").hide();
    $(".tabcontent:first").show();
    $(".bialeft li:first").addClass("active earlier");

  /* if in tab mode */
    $(".bialeft li").click(function() {
        var _this = $(this);
      $(".tabcontent").hide();
      var activeTab = $(this).attr("rel"); 
      
      $("#"+activeTab).fadeIn();        
        
      $(".bialeft li").removeClass("active");
      $(this).addClass("active");
      
      $(this).addClass("earlier");
       $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("earlier");

      $(".tab_drawer_heading").removeClass("d_active");
      $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active earlier");
      
      sectionmaxheight()
      
    });
    /* if in drawer mode */
    $(".tab_drawer_heading:first").addClass("d_active earlier");

    $(".tab_drawer_heading").click(function() {
      var _this = $(this);
          
      
      $(".tabcontent").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
      
      $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
       
      $(this).addClass("earlier");
      $(".bialeft li[rel^='"+d_activeTab+"']").addClass("earlier");
      
      $(".bialeft li").removeClass("active");
      $(".bialeft li[rel^='"+d_activeTab+"']").addClass("active earlier");
       setTimeout(function(){
                        var offsetTop = $(_this).offset().top; 
                        
                        $("html, body").animate({ scrollTop: offsetTop });
                    },500);
       sectionmaxheight()
     
    });
    
    
    
    
    /* /bia tabs ends here */
    sectionmaxheight();
  });

$(window).resize(function () {
    if (this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function () {
        $(this).trigger('resizeEnd');
    }, 600);
});


$(window).resize(function(){
  
    sectionmaxheight()
});

    /* Education tabs starts here */

    $('.educationtab li').click(function(){
    
    var getindex = $(this).index();
    $(this).addClass('active').siblings().removeClass('active');
    $('.tabcontainer').find('.tabcontent').eq(getindex).show().siblings().hide();
      sectionmaxheight();
    }).eq(0).click();

    /* Education tabs ends here */



function sectionmaxheight() {

$('.section-max-height').each(function(){  
      
   
      var highestBox = 0;
      
    
      $('.thumbnail-box', this).each(function(){

         $(this).height('auto');
      
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }
      
      });  
            
 
      $('.thumbnail-box',this).css('height',highestBox);
                    
    }); 
}


function sectionheight() {

$('.sections').each(function(){  
      
   
      var highestBox = 0;
      
    
      $('.thumbnail-box', this).each(function(){

         $(this).height('auto');
      
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }
      
      });  
            
 
      $('.thumbnail-box',this).css('height',highestBox);
                    
    }); 
}

function sectionheight() {

$('.sections').each(function(){  
      
   
      var highestBox = 0;
      
    
      $('.flex-box', this).each(function(){

         $(this).height('auto');
      
        if($(this).outerHeight() > highestBox) {
          highestBox = $(this).outerHeight(); 
        }
      
      });  
            
 
      $('.flex-box',this).css('height',highestBox);
                    
    }); 
}


/* function readlistingtab(evt, readtabname) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(readtabname).style.display = "block";
  evt.currentTarget.className += " active";
  sectionmaxheight();
}*/

 function listingtab(evt, watchtabname) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(watchtabname).style.display = "block";
  evt.currentTarget.className += " active";
  sectionmaxheight();
}



// start jquery for single carousel item
$('.carousel-inner').each(function() {

if ($(this).children('div').length === 1) $(this).siblings('.carousel-indicators, .left , .right').hide();

});

// End  jquery for single carousel item


// Start Jquery of insights page
var $table = $('table.scroll'),
    $bodyCells = $table.find('tbody tr:first').children(),
    colWidth;

// Adjust the width of thead cells when window resizes
$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).width();
    }).get();
    
    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize(); 
// End Jquery of insights page




// Start Jquery of social page
 if($(window).width() <= 801) {
  /*$('.owl-carousel').owlCarousel({
   stagePadding: 50,
    loop:false,
    margin:8,
    nav:true,
  responsive:{
        601:{
            items:1,
            stagePadding: 8
        },
    300:{
            items:1,
            stagePadding: 8
        },

    }
  })*/
}
 if($(window).width() <= 801) {
/*  $('.owl-carousel1').owlCarousel({
   stagePadding: 50,
    loop:false,
    margin:8,
    nav:true,
  responsive:{
        601:{
            items:3,
            stagePadding: 8
        },
    300:{
            items:2,
            stagePadding: 8
        },

    }
  })*/
}
// End Jquery of social page


// Start Jquery of calculator Landing page
$(document).ready(function(){
  $("#View-all").click(function(){
    $("#hidden-box-div").show();
     $("#View-all").hide();
     $(".collapse-div").show();
    $("#box-div").addClass("add-css");
    $("#hidden-box-div").css("margin-top", "-1px");
    childboxdivheight();
  });
  $("#Collapse-all").click(function(){
    $("#hidden-box-div").hide();
     $("#View-all").show();
     $(".collapse-div").hide();
      $("#box-div").removeClass("add-css");
     $("#hidden-box-div").css("margin-top", "0px");
  });

});

if($(window).width() >= 801) {
function boxdivheight() { 

$('.calculator-types').each(function(){  
      
   
      var highestBox = 0;
      
    
      $('.box-div', this).each(function(){

         $(this).height('auto');
      
        if($(this).outerHeight() > highestBox) {
          highestBox = $(this).outerHeight(); 
        }
      
      });  
            
 
      $('.box-div',this).css('height',highestBox);
                    
    }); 
}

function childboxdivheight() { 

$('.hidden-box-div').each(function(){  
      
   
      var highestBox = 0;
      
    
      $('.child-box-div', this).each(function(){

         $(this).height('auto');
      
        if($(this).outerHeight() > highestBox) {
          highestBox = $(this).outerHeight(); 
        }
      
      });  
            
 
      $('.child-box-div',this).css('height',highestBox);
                    
    }); 
}
boxdivheight();
childboxdivheight();
}
// End Jquery of calculator Landing page


// Start Jquery of podcast Landing page
function podcastmaxheight() {

$('.podcast').each(function(){  
      
   
      var highestBox = 0;
      
    
      $('.podcast-box', this).each(function(){

         $(this).height('auto');
      
        if($(this).height() > highestBox) {
          highestBox = $(this).height(); 
        }
      
      });  
            
 
      $('.podcast-box',this).css('height',highestBox);
                    
    }); 
}
podcastmaxheight();
// End Jquery of podcast Landing page