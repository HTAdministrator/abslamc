<?php
/**
 * @author     Based on code originally written by Julian Ridden, G J Barnard, Mary Evans, Bas Brands, Stuart Lamour and David Scotson.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
 class theme_birlasunlife_core_renderer extends theme_bootstrapbase_core_renderer {
 	
 	/*
     * This renders a notification message.
     * Uses bootstrap compatible html.
     */
    public function notification($message, $classes = 'notifyproblem') {
        $message = clean_text($message);
        $type = '';

        if ($classes == 'notifyproblem') {
            $type = 'alert alert-error';
        }
        if ($classes == 'notifysuccess') {
            $type = 'alert alert-success';
        }
        if ($classes == 'notifymessage') {
            $type = 'alert alert-info';
        }
        if ($classes == 'redirectmessage') {
            $type = 'alert alert-block alert-info';
        }
        return "<div class=\"$type\">$message</div>";
    } 
    
		
    protected function render_custom_menu(custom_menu $menu) {
    	/*
    	* This code replaces adds the current enrolled
    	* courses to the custommenu.
    	*/
    global $USER;
    	$hasdisplaymycourses = (empty($this->page->theme->settings->displaymycourses)) ? false : $this->page->theme->settings->displaymycourses;
        if (isloggedin() && !isguestuser() && $hasdisplaymycourses) {
        	$mycoursetitle = $this->page->theme->settings->mycoursetitle;
            if ($mycoursetitle == 'module') {
				$branchtitle = get_string('mymodules', 'theme_birlasunlife');
			} else if ($mycoursetitle == 'unit') {
				$branchtitle = get_string('myunits', 'theme_birlasunlife');
			} else if ($mycoursetitle == 'class') {
				$branchtitle = get_string('myclasses', 'theme_birlasunlife');
			} else {
				$branchtitle = get_string('mycourses', 'theme_birlasunlife');
			}
			$branchlabel = '<i class="fa fa-briefcase"></i>'.$branchtitle;
            $branchurl   = new moodle_url('/my/index.php');
            $branchsort  = 10000;
 
            $branch = $menu->add($branchlabel, $branchurl, $branchtitle, $branchsort);
 			if ($courses = enrol_get_my_courses(NULL, 'fullname ASC')) {
 				foreach ($courses as $course) {
 					if ($course->visible){
 						$branch->add(format_string($course->fullname), new moodle_url('/course/view.php?id='.$course->id), format_string($course->shortname));
 					}
 				}
 			} else {
                $noenrolments = get_string('noenrolments', 'theme_birlasunlife');
 				$branch->add('<em>'.$noenrolments.'</em>', new moodle_url('/'), $noenrolments);
 			}
            
        }
        
        /*
    	* This code replaces adds the My Dashboard
    	* functionality to the custommenu.
    	*/
        $hasdisplaymydashboard = (empty($this->page->theme->settings->displaymydashboard)) ? false : $this->page->theme->settings->displaymydashboard;
        if (isloggedin() && !isguestuser() && $hasdisplaymydashboard) {
            $branchlabel = '<i class="fa fa-dashboard"></i>'.get_string('mydashboard', 'theme_birlasunlife');
            $branchurl   = new moodle_url('/my/index.php');
            $branchtitle = get_string('mydashboard', 'theme_birlasunlife');
            $branchsort  = 10000;
 
            $branch = $menu->add($branchlabel, $branchurl, $branchtitle, $branchsort);
            $branch->add('<em><i class="fa fa-home"></i>'.get_string('myhome').'</em>',new moodle_url('/my/index.php'),get_string('myhome'));
 			$branch->add('<em><i class="fa fa-user"></i>'.get_string('profile').'</em>',new moodle_url('user/edit.php?id='.$USER->id),get_string('profile'));
 			$branch->add('<em><i class="fa fa-calendar"></i>'.get_string('pluginname', 'block_calendar_month').'</em>',new moodle_url('/calendar/view.php'),get_string('pluginname', 'block_calendar_month'));
 			$branch->add('<em><i class="fa fa-envelope"></i>'.get_string('pluginname', 'block_messages').'</em>',new moodle_url('/message/index.php'),get_string('pluginname', 'block_messages'));
 			$branch->add('<em><i class="fa fa-certificate"></i>'.get_string('badges').'</em>',new moodle_url('/badges/mybadges.php'),get_string('badges'));
 			$branch->add('<em><i class="fa fa-file"></i>'.get_string('privatefiles', 'block_private_files').'</em>',new moodle_url('/user/files.php'),get_string('privatefiles', 'block_private_files'));
 			$branch->add('<em><i class="fa fa-file-text"></i>'.get_string('grade').'</em>',new moodle_url('/grade/report/overview/index.php'),get_string('grade')); 
 			$branch->add('<em><i class="fa fa-cog"></i>'.get_string('preferences').'</em>',new moodle_url('/user/preferences.php'),get_string('preferences')); 
 			$branch->add('<em><i class="fa fa-sign-out"></i>'.get_string('logout').'</em>',new moodle_url('/login/logout.php'),get_string('logout'));    
        }
         
        return parent::render_custom_menu($menu);
    }
	
	public function inspector_ajax($term) {
        global $CFG, $USER;

        $data = array();

        if(is_siteadmin()){
            $courses = get_courses();
        }else{
        $courses = enrol_get_my_courses();
        }
        $site = get_site();

        if (array_key_exists($site->id, $courses)) {
            unset($courses[$site->id]);
        }

        foreach ($courses as $c) {
            if (isset($USER->lastcourseaccess[$c->id])) {
                $courses[$c->id]->lastaccess = $USER->lastcourseaccess[$c->id];
            } else {
                $courses[$c->id]->lastaccess = 0;
            }
        }

        // Get remote courses.
        $remotecourses = array();
        if (is_enabled_auth('mnet')) {
            $remotecourses = get_my_remotecourses();
        }
        // Remote courses will have -ve remoteid as key, so it can be differentiated from normal courses.
        foreach ($remotecourses as $id => $val) {
            $remoteid = $val->remoteid * -1;
            $val->id = $remoteid;
            $courses[$remoteid] = $val;
        }

        foreach ($courses as $course) {
            $modinfo = get_fast_modinfo($course);
            $courseformat = course_get_format($course->id);
            $course = $courseformat->get_course();
            $courseformatsettings = $courseformat->get_format_options();
            $sesskey = sesskey();

            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if (!$thissection->uservisible) {
                    continue;
                }
                if (is_object($thissection)) {
                    $thissection = $modinfo->get_section_info($thissection->section);
                } else {
                    $thissection = $modinfo->get_section_info($thissection);
                }
                if ((string) $thissection->name !== '') {
                    $sectionname = format_string($thissection->name, true,
                        array('context' => context_course::instance($course->id)));
                } else {
                    $sectionname = $courseformat->get_section_name($thissection->section);
                }
                if ($thissection->section <= $course->numsections) {
                    // Do not link 'orphaned' sections.
                    $courseurl = new moodle_url('/course/view.php');
                    $courseurl->param('id', $course->id);
                    $courseurl->param('sesskey', $sesskey);
                    if ((!empty($courseformatsettings['coursedisplay'])) &&
                        ($courseformatsettings['coursedisplay'] == COURSE_DISPLAY_MULTIPAGE)) {
                        $courseurl->param('section', $thissection->section);
                        $coursehref = $courseurl->out(false);
                    } else {
                        $coursehref = $courseurl->out(false).'#section-'.$thissection->section;
                    }
                    $label = $course->fullname.' - '.$sectionname;
                    if (stristr($label, $term)) {
                        $data[] = array('id' => $coursehref, 'label' => $label, 'value' => $label);
                    }
                }
                if (!empty($modinfo->sections[$thissection->section])) {
                    foreach ($modinfo->sections[$thissection->section] as $modnumber) {
                        $mod = $modinfo->cms[$modnumber];
                        if (!empty($mod->url)) {
                            $instancename = $mod->get_formatted_name();
                            $label = $course->fullname.' - '.$sectionname.' - '.$instancename;
                            if (stristr($label, $term)) {
                                $data[] = array('id' => $mod->url->out(false), 'label' => $label, 'value' => $label);
                            }
                        }
                    }
                }
            }
        }
       if(empty($data)){
         $data[0]=array('id'=>$CFG->wwwroot.'/my/','label'=>'Course not available','value'=>'Course not available');
        }
        //echo "<pre>";print_r($data); die;
        return $data;
    }
	
public function get_list_categories(){
    global $DB;
    $sql="select * from {course_categories} where visible=1 and id not in (9)";
    $result=$DB->get_records_sql($sql);
    return $result;
}


public function get_section_name($courseid)
{
   global $DB;
   $sql="SELECT DISTINCT(cs.id),cs.name FROM {course_modules} cm, {modules} md, {course_sections} cs, mdl_url m
         WHERE cm.course =?  
         AND cm.instance = m.id AND
         cs.id = cm.section AND 
         md.name = 'url' AND
         md.id = cm.module";
    $result=$DB->get_records_sql($sql,array($courseid));
    return $result;
}

public function get_homePage_courses($category){
    global $CFG,$DB,$OUTPUT;
     //This courses modified for kitaboo courses demo puropses after demo please enable below comment 
    if($category==9999){
        $category='';
    }else{
     $category=$category;   
    }
     $courses = get_courses($category); 
    //$course=$DB->get_records_sql('SELECT c.* FROM {course} c where id != ?', array(1)); 
    unset($courses[1]);
    foreach($courses as $key => $coursevalue)
    {
       if($coursevalue->fullname == 'Animations' || $coursevalue->fullname =='Videos')
       {
           unset ($courses[$coursevalue->id]);
       }
    }
    
    $coursedetailsarray = array();
    foreach ($courses as $key => $coursevalue) {
    $coursedetailsarray[$key]["id"] = $coursevalue->id;
    $coursedetailsarray[$key]["coursename"] = $coursevalue->fullname;    
    $coursedetailsarray[$key]["summary"] = $coursevalue->summary;
    $coursecontext = context_course::instance($coursevalue->id);
    $isfile = $DB->get_records_sql("Select * from {files} where contextid = ? and filename != ?", array($coursecontext->id, "."));
    if ( $isfile ) {
        foreach ($isfile as $key1 => $isfilevalue) {
            $courseimage = $CFG->wwwroot . "/pluginfile.php/" . $isfilevalue->contextid .
            "/" . $isfilevalue->component . "/" . $isfilevalue->filearea . "/" . $isfilevalue->filename;
        }
    }
    if ( !empty( $courseimage ) ) {
        $coursedetailsarray[$key]["courseimage"] = $courseimage;
    } else {
        $coursedetailsarray[$key]["courseimage"] = $this->pix_url('no-image1','theme');
    }
    $courseimage = '';
    }
  return $coursedetailsarray;  
}

public function get_course_id_session_lang($lang)
{
    global $DB;
    $sql= "SELECT id FROM {course_categories} WHERE idnumber like '".$lang."%'";
    $res=$DB->get_record_sql($sql,array());
    return $res->id;
}
}
        




    

