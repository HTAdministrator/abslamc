jQuery(document).ready(function($) {
    if($("#categories").length){
    $("#categories").change(function(){
    var url = window.location.href.split('?')[0];
    var id=$(this).val();
    location.href =url+"?catid="+id+'#categories';
    return false;
    });
    }
    /* ======= Twitter Bootstrap hover dropdown ======= */    
  
    // apply dropdownHover for non-mobile devices
    $('li.dropdown>[data-toggle="dropdown"]').dropdownHover();
    
    /* ======= jQuery FitVids - Responsive Video ======= */
    /* Ref: https://github.com/davatron5000/FitVids.js/blob/master/README.md */
    
    $(".video-container").fitVids();   
    
    /* Nested Sub-Menus mobile fix */
    $('li.dropdown-submenu > a').on('click', function(e) {
        var current=$(this).next();
		current.toggle();
		e.stopPropagation(); 
		e.preventDefault(); 
		if (current.is(':visible')) {
    		$(this).closest('li.dropdown-submenu').siblings().find('ul.dropdown-menu').hide();
		}
    }); 
    
    /*======== Enable Bootstrap Tooltip ======== */    
    $('[data-toggle="tooltip"]').tooltip();
    
     /*======== Enable Bootstrap Popover ======== */     
    $('[data-toggle="popover"]').popover();
    

});


