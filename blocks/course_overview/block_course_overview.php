<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course overview block
 *
 * @package    block_course_overview
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->dirroot . '/blocks/course_overview/locallib.php');

/**
 * Course overview block
 *
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_course_overview extends block_base {

    /**
     * If this is passed as mynumber then showallcourses, irrespective of limit by user.
     */
    const SHOW_ALL_COURSES = -2;

    /**
     * Block initialization
     */
    public function init() {
        $this->title = get_string('themelearingmodules', 'theme_birlasunlife');
    }

    /**
     * Return contents of course_overview block
     *
     * @return stdClass contents of block
     */
    public function get_content() {
        global $USER, $OUTPUT, $CFG, $DB,$_SESSION;
        require_once($CFG->dirroot . '/user/profile/lib.php');
        require_once($CFG->dirroot . '/mod/scorm/locallib.php');
        if ($this->content !== NULL) {
            return $this->content;
        }

        $config = get_config('block_course_overview');

        $this->content = new stdClass();
        $this->content->text = '';
        $this->content->footer = '';

        $content = array();

        /* $updatemynumber = optional_param('mynumber', -1, PARAM_INT);
          if ($updatemynumber >= 0) {
          block_course_overview_update_mynumber($updatemynumber);
          }

          profile_load_custom_fields($USER);

          $showallcourses = ($updatemynumber === self::SHOW_ALL_COURSES);
          list($sortedcourses, $sitecourses, $totalcourses) = block_course_overview_get_sorted_courses($showallcourses);
          $overviews = block_course_overview_get_overviews($sitecourses);

          $renderer = $this->page->get_renderer('block_course_overview');
          if (!empty($config->showwelcomearea)) {
          require_once($CFG->dirroot.'/message/lib.php');
          $msgcount = message_count_unread_messages();
          $this->content->text = $renderer->welcome_area($msgcount);
          }

          // Number of sites to display.
          if ($this->page->user_is_editing() && empty($config->forcedefaultmaxcourses)) {
          $this->content->text .= $renderer->editing_bar_head($totalcourses);
          }

          if (empty($sortedcourses)) {
          $this->content->text .= get_string('nocourses','my');
          } else {
          // For each course, build category cache.
          $this->content->text .= $renderer->course_overview($sortedcourses, $overviews);
          $this->content->text .= $renderer->hidden_courses($totalcourses - count($sortedcourses));
          }

         */
        //echo $catid;
        $catid = optional_param('catid', '', PARAM_INT);
        $categories = $OUTPUT->get_list_categories();
        if ($catid != "") {
            $catid = $catid;
        } else {
            if ($_SESSION[SESSION]->lang != "") {
                $catid = $OUTPUT->get_course_id_session_lang($_SESSION[SESSION]->lang);
            } else {
                $catid = 1;
            }
        }
        $courses = $OUTPUT->get_homePage_courses($catid);

        $this->content->text.='
			<section class="section_wrapper">';
                    if(is_siteadmin()){
					$this->content->text.='	<div class="containeradmin">';
					}else{
					$this->content->text.='	<div class="container">';
					}
					
        $this->content->text.='<div class="row">
                         <div class="sec_header_wrap shw2">
						<div class="sec_head_bod"></div>
						<div class="sec_head sh2" id="sec_head3">
							<img src="' . $CFG->wwwroot . '/theme/birlasunlife/pix/elearning_icon.png" class="sec_head_icon"/>
							' . get_string('themelearingmodules', 'theme_birlasunlife') .
                '</div>
					</div><!--sec_header_wrap-->
					<div class="elearning_wrap">';
        if (count($courses)) {
            $counse_count = 0;
            foreach ($courses as $k => $v) {
                $result2 = $DB->get_field("course_modules", "instance", array("course" => $v['id']));
                $course = $DB->get_record('course', array('id' => $v['id']), '*', MUST_EXIST);
                $scorm = $DB->get_record('scorm', array('id' => $result2), '*', MUST_EXIST);
                if (!$cm = get_coursemodule_from_instance("scorm", $scorm->id, $course->id, true)) {
                    print_error('invalidcoursemodule');
                }

                if (empty($organization)) {
                    $organization = $scorm->launch;
                }
                $orgidentifier = '';
                $sco = scorm_get_sco($organization, SCO_ONLY);
                if ($sco->organization == '' && $sco->launch == '') {
                    $orgidentifier = $sco->identifier;
                } else {
                    $orgidentifier = $sco->organization;
                }
                $arr_cname = explode(":", $v['coursename']);
               // $course_name = substr($arr_cname[1], 0, 30) . '...';
                
                if(!empty(trim($arr_cname[1]))) {
                    $course_name = $arr_cname[1];
                    $course_name = (strlen($course_name) > 40) ? mb_substr($course_name,0,30, 'UTF-8').'...' : $course_name;
                } else {
                    $course_name = '&nbsp;';
                }
                
                $summary = $v['summary'];

                if (strlen($summary) > 100) {
                    $summarystr = mb_substr($summary, 0, 97, 'UTF-8') . '...';
                } else {
                    $summarystr = $summary;
                }
                $buttonname = get_string('themelaunch', 'theme_birlasunlife').' >';
                $string_form = "<form id='scormviewform_" . $cm->id . "' method='post' action='" . $CFG->wwwroot . "/mod/scorm/player.php'>
                                            <input type='hidden' name='mode' value='normal'>
                                            <input type='hidden' name='scoid' value='" . $scorm->launch . "'>
                                            <input type='hidden' name='cm' value='" . $cm->id . "'>
                                            <input type='hidden' name='currentorg' value='" . $orgidentifier . "'>
                                            <input type='submit' value='" . $buttonname . "'  class='module_btn'></form>";
                $m_name = get_string('module', 'theme_birlasunlife') .' '. ($counse_count + 1);
                $this->content->text.='<div class="col-md-4 slideInUp animated" id="module_' . ($counse_count + 1) . '">
                    <div class="elearning_head">
                            ' . $m_name .
                        '</div>
                    <div class="clear"></div>
                    <div class="elearning_sub_head">' .
                        $course_name .
                        ' </div>
                    <div class="module_wrapper">
                            <img src="' . $v['courseimage'] . '"/>
                            <div class="module_body">
                                    <div class="module_text">' .
                            $summarystr .
                            '</div>
                                    <div class="module_btn_wrap">
                                            <a href="#">
                                                    <div>' .
                                                    $string_form .
                                                    '</div>
                                            </a>
                                    </div>
                            </div>
                    </div>
            </div>';
                $counse_count++;
            }
        } else {
            $this->content->text.="<div class='col-sm-12 no-course-available'>" . get_string('nomodules', 'theme_birlasunlife') . "</div>";
        }
        $this->content->text.='</div><!--elearning_wrap-->
				</div><!--row-->
			</div><!--container-->
		</section><!--section_wrapper-->';
        
        return $this->content;
    }

    /**
     * Allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * Locations where block can be displayed
     *
     * @return array
     */
    public function applicable_formats() {
        return array('my' => true);
    }

    /**
     * Sets block header to be hidden or visible
     *
     * @return bool if true then header will be visible.
     */
    public function hide_header() {
        // Hide header if welcome area is show.
//        $config->showwelcomearea =1;
//        $config = get_config('block_course_overview');
//        return !empty($config->showwelcomearea);
        return true;
    }

}
