<?php
//require_once('../config.php');
require_once($CFG->libdir . '/enrollib.php');

function local_enrol_user($id) {
	
    global $CFG, $DB;
    try {
        $proceed = true;

		if (!enrol_is_enabled('manual')) {
            $proceed = false;
        }

        if (!$enrol = enrol_get_plugin('otp')) {
            $proceed = false;
		}
        if ($proceed) {
			echo "HERE";die();
            // Check user exist or not
            if ($user = $DB->get_record('user', array('id' => $id), '*', MUST_EXIST)) {
                $courses = get_courses();

                if ($courses) {
                    foreach ($courses as $course) {
                        $courseproceed = true;
                        if (!$instances = $DB->get_records('enrol', array('courseid' => $course->id), 'sortorder,id ASC')) {
                            $courseproceed = false;
                        }
                        if ($courseproceed) {
                            $params = array($id, $course->id);

                           echo $sql = "SELECT count(*) FROM {$CFG->prefix}user_enrolments 
									WHERE userid = ? AND enrolid 
									IN(SELECT id FROM {$CFG->prefix}enrol WHERE courseid = ? AND enrol = 'manual' ) AND status = 0";
								
                            // Check user enroll or not in course
                            $total = $DB->count_records_sql($sql, $params);

                            if ($total <= 0) {
                                // Need to proceed if no error found
                                // Enrolment of user into course
                                $instance = reset($instances);
                                $enrol->enrol_user($instance, $id, 5, time(), 0);
                            }
                        }
                    }
                }
            } else {
                print_error('usernotexist');
            }
        }
    } catch (Exception $e) {
        redirect("$CFG->wwwroot/");
    }

    return true;
}

