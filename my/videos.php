<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../config.php');
require_once($CFG->dirroot . '/my/lib.php');

// TODO Add sesskey check to edit

$edit   = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off
$reset  = optional_param('reset', null, PARAM_BOOL);

//***************************************************************//
// Line commented for Birla LMS by Hurix Digital
//require_login();
//***************************************************************//


$strmymoodle = get_string('myhome');
$header = "$SITE->shortname: ".get_string('myhome')." (".get_string('mypage', 'admin').")";
// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/videos.php', $params);
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_title($header);
$PAGE->set_heading($header);
echo $OUTPUT->header();
$string.='';

$string .='<section class="section_wrapper">';
	if(is_siteadmin()){
					$string .='	<div class="containeradmin">';
					}else{
					$string .='	<div class="container">';
					}
			$string .='
				<div class="row">	
					<div class="sec_header_wrap">
						<div class="sec_head_bod"></div>
						<div class="sec_head fadeInDown animated" id="sec_head1">
							<img src="'.$CFG->wwwroot.'/theme/birlasunlife/pix/introducing_mutual_fund_icon.png" class="sec_head_icon"/>
							'.get_string('video','theme_birlasunlife').
						'</div>
					</div><!--sec_header_wrap-->';
$string .= '	<div class="vid_wrapper">';
$videos=get_coursemodules_in_course('url',21);
foreach ($videos as $k => $v) {
    $desc = $DB->get_record('url', array('id' => $v->instance));
    
       $string .= '<div class="col-md-6">
                        <!--<a href="'.$CFG->wwwroot.'/mod/'.$v->modname .'/view.php?id='.$v->id.'">-->
                            <a href="'.$CFG->wwwroot.'/my/video_view.php?id='.$v->id.'">
                        <div class="vid_wrap">'
                                .$desc->intro.
                                '<!--<div class="vid_name">'
                                     .$desc->headertext.      
                                  '</div> -->
                                <div class="vid_wrap_body">
                                        <div class="vid_wrap_head">'
                                         .$desc->headertext.      
                                        '</div>
                                        <div class="vid_wrap_text">'
                                         .$desc->bodytext.       
                                        '</div>
                                </div><!--vid_wrap_body-->
                        </div>
                        </a>
                </div><!--col-md-6-->';
}

$string .= '</div> <!--vid_wrapper_wrap-->';
$string .= '	</div><!--row-->
			</div><!--container-->
		</section><!--section_wrapper-->';





/* 
$string.='<section class="animation-section video-section after-logged-in-section">
	<h1 class="h1-heading"><span>'.get_string('video','theme_birlasunlife').'</span></h1>
	<div class="container-fluid">
		
<div class="row slick-videos-slider">';
$videos=get_coursemodules_in_course('url',21);
foreach($videos as $k=>$v){
     $desc=$DB->get_record('url',array('id'=>$v->instance),'intro');
if($desc){
$string.='<a href="'.$CFG->wwwroot.'/mod/'.$v->modname.'/view.php?id='.$v->id.'">';   
$string.='<div class="col-sm-4">';
$string.=$desc->intro;
$string.='</div>';
$string.='</a>';
 }  
 }
foreach ($videos as $k => $v) {
             $desc = $DB->get_record('url', array('id' => $v->instance));
             $string.= '<div class="col-sm-4"><div class="animation-blocks">';
             $string.= '<a href="'.$CFG->wwwroot.'/mod/'.$v->modname .'/view.php?id='.$v->id.'">';
             $string.= '<div class="animation-image-block">';
              if ($desc->intro) 
                { $string.= $desc->intro; }
                
             $string.='</div><div class="text-overlay-container"><div class="animation-heading-block">';
             $string.= $desc->headertext;
             $string.= '</div><div class="animation-details-block">';
             $string.= $desc->bodytext;  
             $string.= '</div></div></a></div></div>';
            }
$string.='</div>

</div>
</section>';*/
echo $string;
?>

<script>
$(document).ready(function(){
	$(".vid_wrap").hover(function(){
				$(this).find(".vid_wrap_body").css("top", "-1px");
				}, function(){
				$(this).find(".vid_wrap_body").css("top", "315px");
			});
			
			
			$(window).scroll(function(){
				// This is then function used to detect if the element is scrolled into view
				function elementScrolled(elem)
				{
					var docViewTop = $(window).scrollTop();
                                
					var docViewBottom = docViewTop + $(window).height();
					var elemTop = $(elem).offset().top;
					return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
				}
				 
				// This is where we use the function to detect if ".box2" is scrolled into view, and when it is add the class ".animated" to the <p> child element
				if(elementScrolled('#sec_head1')) {
					var els = $('#sec_head1'),
						i = 0,
						f = function () {
							$(els).addClass('fadeInDown');
							$(els).addClass('animated');
							//if(i < els.length) setTimeout(f, 400);
							
						};
					f();
				}		
				
				
			});
});
</script>

<?php
echo $OUTPUT->footer();
?>

