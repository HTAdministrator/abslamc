<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once(dirname(__FILE__) . '/../config.php');
require_once($CFG->dirroot . '/my/lib.php');
global $SESSION;
redirect_if_major_upgrade_required();

// TODO Add sesskey check to edit

$edit = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off
//$reset = optional_param('reset', null, PARAM_BOOL);
$id  = optional_param('id', 0, PARAM_INT);        // Course module ID
$lang1= optional_param('lang','', PARAM_RAW);

if(!isset($_SESSION["SESSION"]->lang) || empty($_SESSION["SESSION"]->lang))
{
    $_SESSION["SESSION"]->lang=$lang1;
}

$_SESSION["SESSION"]->lang;
$strmymoodle = get_string('myhome');
$header = "$SITE->shortname: " . get_string('myhome') . " (" . get_string('mypage', 'admin') . ")";
// Start setting up the page
$params=array('id'=>$id, 'lang'=>$_SESSION["SESSION"]->lang);

$PAGE->set_context($context);
$PAGE->set_url('/my/animation_view.php', $params);
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_title($header);
$PAGE->set_heading($header);
echo $OUTPUT->header();

$str_output = '';

$str_output .= '<section class="section_wrapper">';
			if(is_siteadmin()){
					$str_output .='	<div class="containeradmin">';
					}else{
					$str_output .='	<div class="container">';
					}
				$str_output .= '<div class="row">';
$str_output .= '<div class="sec_header_wrap">
						<div class="sec_head_bod"></div>
						<div class="sec_head fadeInDown animated">
							<img src="'.$CFG->wwwroot.'/theme/birlasunlife/pix/about_mutual_fund_icon.png" class="sec_head_icon"/>
							'.get_string('animation','theme_birlasunlife').'
						</div>
					</div><!--sec_header_wrap-->';
$str_output .= '<div class="about_mutual_fund_video_wrap">						
		<div id="myCarousel" class="carousel slide" data-ride="carousel">';
$str_output .='<!-- Wrapper for slides -->
	<div class="carousel-inner">';

$animations = get_coursemodules_in_course('url', 22);
$cnt = 1;
foreach ($animations as $k => $v) {
    $stractive ="";
    $firstid=$v->id;
    //if($cnt==1){$stractive = "active";}
     $desc_animation = $DB->get_record('url', array('id' => $v->instance));
    $str_output .='<div class="item '.$stractive.'" id ="item_'.$v->id.'">
                        <div class="slider_item_head">
                                <img src="'.$CFG->wwwroot.'/theme/birlasunlife/pix/birla_images/vid_sm.png"/>
                                <span class="slider_item_head_text">'.$desc_animation->headertext.'</span>
                        </div>
                        <video controls="true" width="100%" height="432" preload="metadata" title="JaanotohMaano Time Value of Money" id="video_'.$v->id.'">
                                <source src="'.$desc_animation->externalurl.'" type="video/mp4">
                        </video>
                        <div class="slider_item_body">
                               '.$desc_animation->bodytext.'
                        </div>
                  </div>';
    $cnt++;
}
$str_output .='</div>';
$str_output .='<!-- Left and right controls -->
                <a class="left carousel-control cc_left" href="#myCarousel" data-slide="prev" data-interval="true">
                  <img src="'.$CFG->wwwroot.'/theme/birlasunlife/pix/birla_images/slide_left_aro.png"/>
                </a>
                <a class="right carousel-control cc_right" href="#myCarousel" data-slide="next" data-interval="true">
                  <img src="'.$CFG->wwwroot.'/theme/birlasunlife/pix/birla_images/slide_right_aro.png"/>
                </a>';
$str_output .='</div>'
        . '</div><!--about_mutual_fund_video_wrap-->';

$str_output .='</div><!--row-->
			</div><!--container-->
		</section><!--section_wrapper-->';
echo $str_output;
echo $OUTPUT->footer();
?>
<script>

		$(document).ready(function(){
                    
                    var pathname = window.location.pathname;
                  
        		$(".ip_btn").click(function(){
				$(".f_error").show();
			});
                        $('#myCarousel').carousel({
                            interval: false,
                        }).on('slide.bs.carousel', function () {
                            var currele =$(this).find('.active').find('video').attr("id");
                             document.getElementById(currele).pause();
                         });
                        
                    
			$(".error_close").click(function(){
				$(".f_error").hide();
			});
			
			$(".investDmm").click(function(e){
				e.preventDefault();
				$(this).parent(".investWrapper").find("ul").toggle();
			});
			$(".brandMenu").click(function(e){
				e.preventDefault();
				$(".topNavContent").toggle();
			});
			
			$(".arrow").click(function(e){
				e.preventDefault();
                                $(this).parents("li").find(".subMenuWrapper").toggle();
			});
			
			$(".navbar-toggle").click(function(){
				$("#mainsidebar").css("right","0px");
			});
			$(".menu-close").click(function(){
				$("#mainsidebar").css("right","-500px");
			});
			var tech = getUrlParameter('id');
                         if($('#item_'+tech).length==1 ){
                            $('#item_'+tech).addClass('active');
                        }else {
                            $('#item_'+<?php echo $firstid;?>).addClass('active');
                        }
		});
		

   var getUrlParameter = function getUrlParameter(sParam) {
    
                var sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    var key = decodeURIComponent(sParameterName[0]);
                    var value = decodeURIComponent(sParameterName[1]);
                    
                    if (key === sParam) {
                        return value === undefined ? true : value;
                    }
                }
    };



	</script>
