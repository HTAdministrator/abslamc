<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * My Moodle -- a user's personal dashboard
 *
 * - each user can currently have their own page (cloned from system and then customised)
 * - only the user can see their own dashboard
 * - users can add any blocks they want
 * - the administrators can define a default site dashboard for users who have
 *   not created their own dashboard
 *
 * This script implements the user's view of the dashboard, and allows editing
 * of the dashboard.
 *
 * @package    moodlecore
 * @subpackage my
 * @copyright  2010 Remote-Learner.net
 * @author     Hubert Chathi <hubert@remote-learner.net>
 * @author     Olav Jordan <olav.jordan@remote-learner.net>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__) . '/../config.php');
require_once($CFG->dirroot . '/my/lib.php');

// TODO Add sesskey check to edit

$edit   = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off
$reset  = optional_param('reset', null, PARAM_BOOL);

require_login();


$strmymoodle = get_string('myhome');
$header = "$SITE->shortname: ".get_string('myhome')." (".get_string('mypage', 'admin').")";
// Start setting up the page
$params = array();
$PAGE->set_context($context);
$PAGE->set_url('/my/videos.php', $params);
$PAGE->set_pagelayout('mydashboard');
$PAGE->set_title($header);
$PAGE->set_heading($header);
echo $OUTPUT->header();
$string.='';
$string.='<section class="animation-section video-section after-logged-in-section">
	<h1 class="h1-heading"><span>'.get_string('video','theme_birlasunlife').'</span></h1>
	<div class="container-fluid">
		
<div class="row slick-videos-slider">';
$videos=get_coursemodules_in_course('url',21);
/* foreach($videos as $k=>$v){
     $desc=$DB->get_record('url',array('id'=>$v->instance),'intro');
if($desc){
$string.='<a href="'.$CFG->wwwroot.'/mod/'.$v->modname.'/view.php?id='.$v->id.'">';   
$string.='<div class="col-sm-4">';
$string.=$desc->intro;
$string.='</div>';
$string.='</a>';
 }  
 }*/
foreach ($videos as $k => $v) {
             $desc = $DB->get_record('url', array('id' => $v->instance));
             $string.= '<div class="col-sm-4"><div class="animation-blocks">';
             $string.= '<a href="'.$CFG->wwwroot.'/mod/'.$v->modname .'/view.php?id='.$v->id.'">';
             $string.= '<div class="animation-image-block">';
              if ($desc->intro) 
                { $string.= $desc->intro; }
                
             $string.='</div></a><div class="animation-heading-block"><br>';
             $string.= $desc->headertext;
             $string.= '</div><div class="animation-details-block">';
             $string.= $desc->bodytext;  
             $string.= '</div></div></div>';
            }
$string.='</div>

</div>
</section>';
echo $string;
?>

<script>
$(document).ready(function(){
	$('.slick-videos-slider').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1,
    	dots: false,	  
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,

			  }
			},
			{
			  breakpoint: 600,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		]	
	});

});
</script>

<?php
echo $OUTPUT->footer();
?>

