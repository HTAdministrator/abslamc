<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../../config.php');
//require_once('lib.php');

global $DB;

$username = $_POST['user'];
$OTP = rand(1000, 9999);
$user = $DB->get_record('user', array('username' => $username));

if ($user) {
    //if user is not siteadmin and auhentication is otp
    if (!is_siteadmin($user) && $user->auth == 'otp') {
        $userOTP = new stdClass();
        $userOTP->fullname = $user->firstname . " " . $user->lastname;
        $userOTP->username = $username;
        $userOTP->otp = $OTP;
        $userOTP->email = $user->email;
        
        if ($user_otp = $DB->get_record('user_otp', array('username' => $username))) {
            $user_otp->otp = $OTP;
            $timemodified = time();
            $user_otp->timemodified = $timemodified;
            $DB->update_record('user_otp', $user_otp);
        } else {
            $timecreated = time();
            $userOTP->timecreated = $timecreated;
            $userOTP->timemodified = $timecreated;
            $DB->insert_record('user_otp', $userOTP, false);
        }
        if(validate_mobile($username))
        {
            sendSMS($username,$OTP);
            echo "OTP Send ";
        }
        else
        {
         echo "Invalid Mobile Number";
        }
        // echo "OTP Sent";
    }
    else{
         echo "Invalid User";
    }
}
else{
    echo "Invalid User";
}

