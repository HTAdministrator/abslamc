<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../../config.php');
//require_once('lib.php');

global $DB;

$username = $_POST['user'];
$OTP = rand(1000, 9999);
$user = $DB->get_record('user', array('username' => $username));

if ($user) {
    //if user is not siteadmin and auhentication is otp
    if (!is_siteadmin($user) && $user->auth == 'otp') {
        $userOTP = new stdClass();
        $userOTP->fullname = $user->firstname . " " . $user->lastname;
        $userOTP->username = $username;
        $userOTP->otp = $OTP;
        $userOTP->email = $user->email;
        
        if ($user_otp = $DB->get_record('user_otp', array('username' => $username))) {
            $user_otp->otp = $OTP;
            $timemodified = time();
            $user_otp->timemodified = $timemodified;
            $DB->update_record('user_otp', $user_otp);
        } else {
            $timecreated = time();
            $userOTP->timecreated = $timecreated;
            $userOTP->timemodified = $timecreated;
            $DB->insert_record('user_otp', $userOTP, false);
        }
        if(validate_mobile($username))
        {
            sendSMS($username,$OTP);
            echo "OTP Send ";
        }
        else
        {
         echo "Invalid Mobile Number";
        }
        // echo "OTP Sent";
    }
    else{
         echo "Invalid User";
    }
}
else{
    echo "Invalid User";
}

function sendSMS($phone,$otp)
{
    global $CFG;
    require_once($CFG->libdir . '/authlib.php');
    $config=get_config('auth/otp');
    $mestext=str_replace('{a}',$otp, $config->otpMessageTxt);
    $curl = curl_init();
    curl_setopt ($curl, CURLOPT_URL, "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi");
    curl_setopt ($curl, CURLOPT_GET, 1);
    curl_setopt ($curl, CURLOPT_POSTFIELDS, "feedid=".$config->otpfeedId."&username=".$config->otpuserId."&password=".$config->otpPassword."&To=".$phone."&Text=".$mestext."&senderid=".$config->otpsenderId);
    curl_setopt ($curl, CURLOPT_COOKIESESSION, 1);
    curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt ($curl, CURLOPT_MAXREDIRS, 20);
    curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
    curl_setopt ($curl, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt ($curl, CURLOPT_REFERER, "http://bulkpush.mytoday.com");
    $text = curl_exec($curl);
    curl_close($curl);
    
}

