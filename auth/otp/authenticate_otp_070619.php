<?php
require_once( '../../config.php' );
global $CFG, $USER, $DB, $OUTPUT;
$PAGE->set_pagelayout('standard');


$PAGE->set_title($strtitle);
$PAGE->set_heading($strtitle);
echo $OUTPUT->header();
$username = optional_param('user', '', PARAM_RAW);
$confirm = optional_param('notconfirm', 0, PARAM_INT);
//DEFINITIONS
require_once($CFG->libdir . '/formslib.php');

class simplehtml_form extends moodleform {

    function definition() {
        global $CFG;
        $loginurl = new moodle_url('/login/index.php');
        $mform = & $this->_form; // Don't forget the underscore! 
        $mform->addElement('html', '<div class="form_wrapper">');
        if(isset($_GET['error'])){
            $mform->addElement('html', '<div class="loginheading"><h2>Error:' .$_GET['error']. '</h2></div>'); 
        }
        $mform->addElement('html', '<div class="loginheading"><h2>' . get_string('signupaccount') . '</h2></div>');
        //$mform->addElement('header', 'createuserandpass', 'Enter your one time password', '');
        $mform->addElement('password', 'otp', get_string('otp'), 'placeholder="' . get_string('otp') . '"');
        //$mform->addElement('password', 'otp', 'OTP'); // Add elements to your form
        $mform->setType('password', PARAM_NOTAGS);                   //Set type of element       
        //$mform->addElement('html', '<label onclick="generateOTP()" class="otptxt"><a>Re Send OTP</a></label>');
        $mform->addElement('hidden', 'username', $username);
        $mform->addElement('hidden', 'user', $username); // Add elements to your form
        //$mform->setType('username', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('username', $username); // Default value added
        $mform->addElement('html', '<br/>');
        // Buttons
        $this->add_action_buttons($cancel = false, get_string('startsignup'));
        

        $mform->addElement('html', '<div class="create_account_wrap">');
        $mform->addElement('html', get_string('alreadyaccnt') . ' <a href="' . $loginurl . '">' . get_string("login") . '</a>');
        $mform->addElement('html', '</div>');
        $mform->addElement('html', '</div>');
    }

    function validation($data, $files) {
        return array();
    }

}

$mform = new simplehtml_form();
$data = new stdClass();   // or data loaded from the database, or an array, as appropriate
$data->username = $username; // Set up all the data you want to pass into the form
$mform->set_data($data);

if ($mform->is_cancelled()) {
    echo '<h1>Cancelled</h1>';
    echo 'Handle form cancel operation, if cancel button is present on form';
} else if ($fromform = $mform->get_data()) {
    $otp = $fromform->otp;
    $user = $DB->get_record('user', array('username' => $fromform->username));
	
    if (validate_internal_user_OTP($user, $otp)) {
        $user->confirmed = 1;
        $DB->update_record('user', $user);
        complete_user_login($user);
        sleep(5);
        $url = $CFG->wwwroot . "/auth/otp/authsuccess.php";
       
        redirect($url);
        exit;
    } else {
        echo '<h1>Invalid OTP</h1>';
        $params = array();
        $params = array('user' => $fromform->username,'error'=>'Invalid OTP Enter');
        $page_url = new moodle_url("/auth/otp/authenticate_otp.php", $params);
        redirect($page_url);
        exit;
        //$mform->display();
    }
} else {
    if ($confirm == 1) {
        echo "<div class='h4'>Your account is not confirmed. Please confirm your account by regenerating otp and validating it.</div>";
    }
    $mform->display();
}
?>


<script>
    function generateOTP() {
        var username = '<?php echo $username; ?>';

        if (username != "") {
            //window.location.href = "../login/generateOTP.php?user="+encodeURIComponent(username);
            $.ajax({
                type: "POST",
                url: "generateOTP.php",
                data: {
                    user: username

                },
                success: function (result) {
                    alert(result);
                },
                error: function (result) {
                    alert('error');
                }
            });
        }
        else {
            alert("Please enter username");
        }
    }


</script>