<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Authentication Plugin: otp Authentication
 *
 * @author Martin Dougiamas
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package auth_otp
 */
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/authlib.php');

/**
 * otp authentication plugin.
 */
class auth_plugin_otp extends auth_plugin_base {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->authtype = 'otp';
        $this->config = get_config('auth/otp');
    }

    /**
     * Old syntax of class constructor. Deprecated in PHP7.
     *
     * @deprecated since Moodle 3.1
     */
    public function auth_plugin_otp() {
        debugging('Use of class name as constructor is deprecated', DEBUG_DEVELOPER);
        self::__construct();
    }

    /**
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Authentication success or failure.
     */
    function user_login($username, $password) {
        global $CFG, $DB;
        if ($user = $DB->get_record('user', array('username' => $username, 'mnethostid' => $CFG->mnet_localhost_id))) {
//            return validate_internal_user_password($user, $password);
            //return validate_internal_user_OTP($user, $password);
            return true;
        } else {
            if (validate_mobile($username)) {
                redirect($CFG->wwwroot . "/login/signup.php?username=$username");
            }
            return false;
        }
    }

//    /**
//     * Compare otp against password entered to determine if it is valid.
//     * Code added by Hurix Digital to validate user OTP
//     * @param type $user
//     * @param type $otp
//     * @return boolean
//     */
//    function validate_internal_user_OTP($user, $otp) {
//        global $CFG, $DB;
//        $validated = false;
//        $user_otp = $DB->get_record('user_otp', array('username' => $user->username));
//
//        if ($user_otp->otp === $otp) {
//           
//            
//            $currenttime = time();
//            $otptime = $user_otp->timemodified;
//            $mins = ( $currenttime - $otptime) / 60;
//            //time validation for OTP
//           
//            if($mins >30){
//                 $validated = false;
//            }else{
//            $validated = true;
//            }
//        }
//
//        return $validated;
//    }

    /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object  (with system magic quotes)
     * @param  string  $newpassword Plaintext password (with system magic quotes)
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword) {
        $user = get_complete_user_data('id', $user->id);
        // This will also update the stored hash to the latest algorithm
        // if the existing hash is using an out-of-date algorithm (or the
        // legacy md5 algorithm).
        return update_internal_user_password($user, $newpassword);
    }

    function can_signup() {
        return true;
    }

    /**
     * Sign up a new user ready for confirmation.
     * Password is passed in plaintext.
     *
     * @param object $user new user object
     * @param boolean $notify print notice with link and terminate
     */
    function user_signup($user, $notify = true) {
        global $CFG, $DB;
        require_once($CFG->dirroot . '/user/profile/lib.php');
        require_once($CFG->dirroot . '/user/lib.php');

        $plainpassword = $user->password;
        $user->password = hash_internal_user_password($user->password);
        $user->confirmed = 0;
        if (empty($user->calendartype)) {
            $user->calendartype = $CFG->calendartype;
        }


        $user->id = user_create_user($user, false, false);

        user_add_password_history($user->id, $plainpassword);

        // Save any custom profile field information.
        profile_save_data($user);

        // Trigger event.
        \core\event\user_created::create_from_userid($user->id)->trigger();

//        if (!send_confirmation_email($user)) {
//            //print_error('auth_otpnootp', 'auth_otp');
//        }
//
//        if ($notify) {
//            global $CFG, $PAGE, $OUTPUT;
//            $otpconfirm = get_string('otpconfirm');
//            $PAGE->navbar->add($otpconfirm);
//            $PAGE->set_title($otpconfirm);
//            $PAGE->set_heading($PAGE->course->fullname);
//            echo $OUTPUT->header();
//            notice(get_string('otpconfirmsent', '', $user->otp), "$CFG->wwwroot/index.php");
//        } else {
//            return true;
//        }
        $this->generateOTP($user); //generate OTP and saved in custom table
        $params = Array("user" => $user->username);
        $url_otp = $CFG->wwwroot . "/auth/otp/authenticate_otp.php";
        $url = new moodle_url($url_otp, $params);
        redirect($url);
    }

    function generateOTP($user) {
        global $DB;
        $OTP = rand(1000, 9999);
        if (!is_siteadmin($user) && $user->auth == 'otp') {
            $userOTP = new stdClass();
            $userOTP->fullname = $user->firstname . " " . $user->lastname;
            $userOTP->username = $user->username;
            $userOTP->otp = $OTP;
            $userOTP->email = $user->email;

            if ($user_otp = $DB->get_record('user_otp', array('username' => $user->username))) {
                $user_otp->otp = $OTP;
                $timemodified = time();
                $user_otp->timemodified = $timemodified;
                $DB->update_record('user_otp', $user_otp);
            } else {
                $timecreated = time();
                $userOTP->timecreated = $timecreated;
                $userOTP->timemodified = $timecreated;
                $DB->insert_record('user_otp', $userOTP, false);
            }
            if (validate_mobile($user->username)) {
                sendSMS($user->username, $OTP);
            }
            return true;
        } else {
            return false;
        }
    }

    function prevent_local_passwords() {
        return false;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return true;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return true;
    }

    /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return null; // use default internal method
    }

    /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password() {
        return true;
    }

    /**
     * Returns true if plugin can be manually set.
     *
     * @return bool
     */
    function can_be_manually_set() {
        return true;
    }

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
        include "config.html";
    }

    /**
     * Processes and stores configuration data for this authentication plugin.
     */
    function process_config($config) {
        // set to defaults if undefined
        if (!isset($config->recaptcha)) {
            $config->recaptcha = false;
        }
        // save settings
        set_config('recaptcha', $config->recaptcha, 'auth/otp');
        set_config('otpuserId', $config->otpuserId, 'auth/otp');
        set_config('otpPassword', $config->otpPassword, 'auth/otp');
        set_config('otpMessageTxt', $config->otpMessageTxt, 'auth/otp');
        set_config('otpfeedId', $config->otpfeedId, 'auth/otp');
        set_config('otpsenderId', $config->otpsenderId, 'auth/otp');
        set_config('otpExpirytime', $config->otpExpirytime, 'auth/otp');
        set_config('otpsms', $config->otpsms, 'auth/otp');
        return true;
    }

    /**
     * Returns whether or not the captcha element is enabled.
     * @return bool
     */
    function is_captcha_enabled() {
        return get_config("auth/{$this->authtype}", 'recaptcha');
    }

}
