<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_otp', language 'en'.
 *
 * @package   auth_otp
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['auth_otpdescription'] = '<p>otp-based self-registration enables a user to create their own account via a \'Create new account\' button on the login page. The user then receives an otp containing a secure link to a page where they can confirm their account. Future logins just check the username and password against the stored values in the Moodle database.</p><p>Note: In addition to enabling the plugin, otp-based self-registration must also be selected from the self registration drop-down menu on the \'Manage authentication\' page.</p>';
$string['auth_otpnootp'] = 'Tried to send you an otp but failed!';
$string['auth_otprecaptcha'] = 'Adds a visual/audio confirmation form element to the sign-up page for otp self-registering users. This protects your site against spammers and contributes to a worthwhile cause. See http://www.google.com/recaptcha for more details.';
$string['auth_otprecaptcha_key'] = 'Enable reCAPTCHA element';
$string['auth_otpsettings'] = 'Settings';
$string['pluginname'] = 'otp-based self-registration';
$string['auth_otpuserid']='OTP UserId';
$string['auth_otpuserpass']='OTP Password';
$string['auth_otpusermsg']='OTP Message';

$string['auth_otpuseridtxt']='This field will used to save the authenticated UserId used for the system';
$string['auth_otpuserpasstxt']='This field will used to save the authenticated Password used for the system';
$string['auth_otpusermsgtxt']='This field will used to save message text used to send SMS to user.{a} will be used for the OTP number.';

$string['auth_otpusermsgtxt']='{a} is OTP number used for login on Birla LMS site.';

$string['auth_otpfeedid']='Feed ID';
$string['auth_otpsenderid']='Sender Id';
$string['auth_otptimelimit']='Expiry Limit';
$string['auth_otptimelimittxt']='This is time limit set (in mins) for the OTP to expire';

$string['auth_otpfeedidtxt']='';
$string['auth_otpsenderidtxt']='';


$string['auth_otpsmsactive']='OTP Active';
$string['auth_otpsmsactivetxt']='Set to send SMS or Not';